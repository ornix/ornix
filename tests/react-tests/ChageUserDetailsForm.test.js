/* eslint-env jest */
/* eslint-disable react/jsx-filename-extension, react/jsx-key */

import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import MediaQuery from 'react-responsive';

import ChangeUserDetailsForm from '../../src/views/dumb/changeUserDetailsForm';
import ChangePasswordForm from '../../src/views/dumb/changePasswordForm';

describe('ChangeUserDetailsForm', () => {
  test('Renders the form elements', () => {
    const wrapper = shallow(
      <ChangeUserDetailsForm
        user={{}}
        onInputChange={jest.fn()}
        formErrors={{}}
        onBlur={jest.fn()}
      />,
    );

    expect(wrapper.find(MediaQuery)).to.have.length(2);
    expect(wrapper.find(TextField)).to.have.length(10);
    expect(wrapper.find(DatePicker)).to.have.length(2);
    expect(wrapper.find(RaisedButton)).to.have.length(2);
  });
});

describe('ChangePasswordForm', () => {
  test('Renders the form elements', () => {
    const wrapper = shallow(
      <ChangePasswordForm
        onInputChange={jest.fn()}
        formErrors={{}}
      />,
    );

    expect(wrapper.find(MediaQuery)).to.have.length(2);
    expect(wrapper.find(TextField)).to.have.length(3);
    expect(wrapper.find(RaisedButton)).to.have.length(2);
  });
});
