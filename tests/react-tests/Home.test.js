/* eslint-env jest */
/* eslint-disable react/jsx-filename-extension, react/jsx-key */

import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import FlatButton from 'material-ui/FlatButton';
import MediaQuery from 'react-responsive';

import Home from '../../src/views/dumb/home';
import HomeView from '../../src/views/smart/HomeView';
import Spinner from '../../src/views/dumb/spinner';

describe('Home', () => {
  test('Renders the buttons REGISTER and LOGIN', () => {
    const wrapper = shallow(<Home />);

    expect(wrapper.find(MediaQuery)).to.have.length(2);
    expect(wrapper.find(FlatButton)).to.have.length(4);

    expect(wrapper.containsAnyMatchingElements([
      <FlatButton>REGISTER</FlatButton>,
      <FlatButton>LOGIN</FlatButton>,
    ])).to.equal(true);
  });
});

describe('HomeView', () => {
  test('Renders the spinner on initial load', () => {
    const wrapper = mount(<HomeView history={{}} />);
    expect(wrapper.find(Spinner)).to.have.length(1);
  });

  test('Renders <Home /> if there is no user', () => {
    const wrapper = mount(<HomeView history={{}} />);
    Object.defineProperty(wrapper.instance(), 'hasLoadedImages', {
      get: () => true,
    });
    wrapper.update();
    wrapper.setState({ isLoading: false });
    expect(wrapper.find(Home)).to.have.length(1);
  });

  // Note: Unable to test due to setState() issues with Enzyme
  // test('Renders the <FarmListContainer /> as the home if a user is logged in', () => {
  //   const wrapper = mount(
  //     <MuiThemeProvider muiTheme={customTheme}>
  //       <HomeView history={{}} />
  //     </MuiThemeProvider>,
  //   );
  //   wrapper.setState({ user: { _id: 'mock' } });
  //   wrapper.setState({ isLoading: false });
  //   expect(wrapper.find(FarmListContainer)).to.have.length(1);
  // });
});
