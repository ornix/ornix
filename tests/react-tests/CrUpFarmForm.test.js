/* eslint-env jest */
/* eslint-disable  react/jsx-filename-extension, react/jsx-key */

import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import RaisedButton from 'material-ui/RaisedButton';
import CrUpFarmForm from '../../src/views/dumb/crUpFarmForm';

describe('CrUpFarmForm', () => {
  test('Renders 2 buttons', () => {
    const wrapper = shallow(
      <CrUpFarmForm
        farm={{}}
        onInputChange={jest.fn()}
        onMapLoad={jest.fn()}
        marker={{}}
        onMapClick={jest.fn()}
        revealNotification={{}}
        message={{}}
        handleRequestClose={jest.fn()}
        handleCancelAction={jest.fn()}
        center={{}}
        onPlotFarm={jest.fn()}
        onOverlayComplete={jest.fn()}
        boundary={{}}
        willCreate={{}}
        bounds={{}}
        onSearchBoxMounted={jest.fn()}
      />,
    );
    expect(wrapper.find(RaisedButton)).to.have.length(4);
  });
});
