/* eslint-env jest */
/* eslint-disable react/jsx-filename-extension, react/jsx-key */

import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { DropDownMenu, DatePicker, TextField, RaisedButton, GridTile } from 'material-ui';
import Upload from 'material-ui-upload/Upload';
import MediaQuery from 'react-responsive';
import BigCalendar from 'react-big-calendar';

import ActivityCalendar from '../../src/views/dumb/activityCalendar';
import ActivityForm from '../../src/views/dumb/activityForm';
import ImageGallery from '../../src/views/dumb/imageGallery';
// import ImagePreview from '../../src/views/dumb/imagePreview';
// import ProfitChart from '../../src/views/dumb/profitChart';

// import ActivityContainer from '../../src/views/smart/ActivityContainer';

describe('Activity Calendar', () => {
  test('Renders the Calendar', () => {
    const wrapper = shallow(
      <ActivityCalendar
        events={[]}
        onSelectEvent={jest.fn}
        onNavigate={jest.fn}
      />);
    expect(wrapper.find(BigCalendar)).to.have.length(2);
  });

  test('Renders the Activity Form', () => {
    const wrapper = shallow(
      <ActivityForm
        onInputChange={jest.fn}
        formErrors={{}}
        selectedActivity={''}
        onFileLoad={jest.fn}
        onDropDownChange={jest.fn}
      />);
    expect(wrapper.find(MediaQuery)).to.have.length(2);
    expect(wrapper.find(DropDownMenu)).to.have.length(2);
    expect(wrapper.find(DatePicker)).to.have.length(2);
    expect(wrapper.find(TextField)).to.have.length(4);
    expect(wrapper.find(RaisedButton)).to.have.length(2);
    expect(wrapper.find(Upload)).to.have.length(2);
  });

  test('Renders the Image Viewer uploader', () => {
    const wrapper = shallow(<ImageGallery uriList={[]} remove={jest.fn} removeAll={jest.fn} />);
    expect(wrapper.find(GridTile)).to.have.length(2);
    const contentWrapper = shallow(<ImageGallery uriList={['item1', 'item2']} remove={jest.fn} removeAll={jest.fn} />);
    expect(contentWrapper.find(GridTile)).to.have.length(4);
  });

  // TODO: Implementation
  // test('Renders the ActivityContainer components correctly', () => {
  //   const wrapper = mount(
  //      <ActivityContainer match={{ params: { id: 'MOCK', type: 'MOCK' } }} history={{}} />
  //   );
  //   console.log(wrapper.debug());
  //   expect(wrapper.find(MediaQuery)).to.have.length(2);
  //   expect(wrapper.find(ActivityForm)).to.have.length(1);
  //   expect(wrapper.find(ImagePreview)).to.have.length(1);
  //   expect(wrapper.find(ProfitChart)).to.have.length(1);
  //   expect(wrapper.find(ActivityCalendar)).to.have.length(1);
  // });
});
