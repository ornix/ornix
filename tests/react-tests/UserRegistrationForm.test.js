/* eslint-env jest */
/* eslint-disable react/jsx-filename-extension, react/jsx-key */

import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';

import UserRegistrationForm from '../../src/views/dumb/userRegistrationForm';

describe('UserRegistrationForm', () => {
  test('Renders the form elements', () => {
    const wrapper = shallow(
      <UserRegistrationForm
        formErrors={{}}
        onInputChange={jest.fn()}
        onBlur={jest.fn()}
      />,
    );

    expect(wrapper.find(TextField)).to.have.length(16);
    expect(wrapper.find(DatePicker)).to.have.length(2);
    expect(wrapper.find(FlatButton)).to.have.length(2);
  });
});
