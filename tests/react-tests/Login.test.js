/* eslint-env jest */
/* eslint-disable react/jsx-filename-extension */

import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import { TextField } from 'material-ui';
import PasswordField from 'material-ui-password-field';
import MediaQuery from 'react-responsive';

import LoginView from '../../src/views/smart/LoginView';
import Login from '../../src/views/dumb/login';
import Spinner from '../../src/views/dumb/spinner';

describe('<Login />', () => {
  test('Renders 2 <TextField /> and 2 <PasswordField /> components', () => {
    const wrapper = shallow(<Login errorMessage={''} submitHandler={jest.fn()} inputHandler={jest.fn()} />);
    expect(wrapper.find(MediaQuery)).to.have.length(2);
    expect(wrapper.find(TextField)).to.have.length(2);
    expect(wrapper.find(PasswordField)).to.have.length(2);
  });
});

describe('<LoginView />', () => {
  test('Renders Login components correctly', () => {
    const wrapper = mount(
      <LoginView history={{}} />,
    );
    expect(wrapper.find(Login)).to.have.length(0);
    expect(wrapper.find(Spinner)).to.have.length(1);
    Object.defineProperty(wrapper.instance(), 'hasLoadedImages', {
      get: () => true,
    });
    wrapper.update();
    wrapper.setState({ hasLoaded: true });
    expect(wrapper.find(Spinner)).to.have.length(0);
    expect(wrapper.find(Login)).to.have.length(1);
    expect(wrapper.find(MediaQuery)).to.have.length(2);
  });
});
