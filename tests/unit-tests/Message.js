import { expect } from 'chai';
import faker from 'faker';
import mongodb from 'mongodb';

import Message from '../../src/models/Message';

const ObjectID = mongodb.ObjectID;

describe('Message', () => {
  const message1 = new Message({
    userId: new ObjectID().toString(),
    farmId: new ObjectID().toString(),
    content: faker.lorem.sentences(3),
    createdAt: new Date(),
  });

  const message2 = new Message({
    userId: new ObjectID().toString(),
    farmId: new ObjectID().toString(),
  });

  describe('#isValid', () => {
    it('Checks if model conforms to schema', () => {
      expect(message1.isValid()).to.equal(true);
    });

    it('Returns false for valid document of a message', () => {
      expect(message2.isValid()).to.equal(false);
    });
  });
});
