import { expect } from 'chai';
import WeatherMap from '../../src/models/WeatherMap';

describe('WeatherMap', () => {
  const iloiloArea = new WeatherMap('122.5655547', '10.7213377');

  describe('#getCurrentWeather', () => {
    it('Retrieves the current weather', async () => {
      const currentWeather = await iloiloArea.getCurrentWeather();

      expect(currentWeather).to.be.an.instanceOf(Object);
      expect(currentWeather.coord.lon).to.equal(122.57);
      expect(currentWeather.coord.lat).to.equal(10.72);
      expect(currentWeather.name).to.equal('Iloilo');
      expect(currentWeather).to.haveOwnProperty('weather');
      expect(currentWeather).to.haveOwnProperty('main');
      expect(currentWeather).to.haveOwnProperty('wind');
      expect(currentWeather).to.haveOwnProperty('dt');
    });
  });

  describe('#getWeatherForecast', () => {
    it('Retrieves daily weather forecasts', async () => {
      const weatherForecast = await iloiloArea.getWeatherForecast();
      const keys = Object.keys(weatherForecast);
      const set = new Set(keys);
      expect(keys.length).to.equal(set.size);
    });
  });
});
