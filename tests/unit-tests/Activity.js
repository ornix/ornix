import { expect } from 'chai';
import faker from 'faker';

import Activity from '../../src/models/Activity';

describe('Activity', () => {
  const activity1 = new Activity({
    type: 'water',
    brand: faker.lorem.word(),
    remarks: faker.lorem.words(),
    date: faker.date.past(),
    images: [faker.image.nature()],
  });

  const activity2 = new Activity({
    type: 'harvest',
    brand: faker.lorem.word(),
    remarks: faker.lorem.words(),
    date: faker.date.past(),
    images: [faker.image.nature()],
  });

  describe('#isValid', () => {
    it('Checks if model conforms to schema', () => {
      expect(activity1.isValid()).to.equal(true);
    });

    it('Returns false for valid document of an activity', () => {
      expect(activity2.isValid()).to.equal(false);
    });
  });
});
