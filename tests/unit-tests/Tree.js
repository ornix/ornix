import { expect } from 'chai';
import mongodb from 'mongodb';
import faker from 'faker';

import Tree from '../../src/models/Tree';

const ObjectID = mongodb.ObjectID;

describe('Tree', () => {
  const tree = new Tree({
    type: faker.random.word(),
    description: faker.random.words(5),
    coordinates: { lat: faker.finance.amount(-90, 90, 4), lng: faker.finance.amount(-180, 180, 4) },
    farmId: new ObjectID().toString(),
    icon: faker.image.nature(),
  });

  describe('#isValid', () => {
    it('Checks if model conforms to schema', () => {
      expect(tree.isValid()).to.equal(true);
    });
  });

  describe('#markerData()', () => {
    it('Gets a preprocessed marker data object', () => {
      const markerData = tree.markerData;
      expect(markerData).to.have.all.keys('lastActivity', '_id', 'position', 'defaultAnimation', 'key', 'showInfo', 'type', 'description', 'icon');
      expect(markerData.position).to.have.all.keys('lat', 'lng');
    });
  });
});
