import { expect } from 'chai';

import Statistics from '../../src/models/Statistics';

describe('Statistics', () => {
  const sampleDate = new Date('Mar 11 2018');
  const datum1 = {
    profit: 100,
    amountOfHarvest: 200,
    date: sampleDate,
  };

  const datum2 = {
    profit: 150,
    amountOfHarvest: 250,
    date: sampleDate,
  };

  const sampleData = [datum1, datum2];
  const statistics = new Statistics(sampleData);

  describe('#calculate()', () => {
    const month = sampleDate.getMonth();
    it('Calculates the total profit for the month', () => {
      expect(statistics.statistics[month].profit).to.be.equal(250);
    });
    it('Calculates the total amount of harvest for the month', () => {
      expect(statistics.statistics[month].amountOfHarvest).to.be.equal(450);
    });
  });

  describe('#getProfitArray()', () => {
    it('Returns the array of profit', () => {
      expect(statistics.getProfitArray()).to.be.an('array');
      expect(statistics.getProfitArray()[2]).to.be.equal(250);
    });
  });

  describe('#getAmountOfHarvestArray()', () => {
    it('Returns the array of harvest', () => {
      expect(statistics.getProfitArray()).to.be.an('array');
      expect(statistics.getAmountOfHarvestArray()[2]).to.be.equal(450);
    });
  });

  describe('#preSeed()', () => {
    it('Generates seeds for statistics', () => {
      expect(statistics.statistics).to.have.include.all.keys('0', '11');
      // expect(statistics.statistics).to.have.property('1', { amountOfHarvest: 0, profit: 0 });
    });
  });
});
