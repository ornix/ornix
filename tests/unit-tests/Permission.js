import { expect } from 'chai';
import Permission from '../../src/models/Permission';

describe('Permission', () => {
  const permission = new Permission();

  describe('#canExecute()', () => {
    it('Verifies if an admin is authorized to do a write action', () => {
      const isAuthorized = permission.canExecute('admin', 'write');
      expect(isAuthorized).to.be.true;
    });
  });
});
