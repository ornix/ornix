import faker from 'faker';
import chai, { expect } from 'chai';
import spy from 'chai-spies';

import Sms from '../../src/models/Sms';

chai.use(spy);

describe('Sms', () => {
  const recepient = `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`;
  const content = faker.lorem.sentences();
  const name = faker.name.firstName();
  const code = faker.random.number(4);

  const sms = new Sms(recepient, content);
  const smsSpy = chai.spy(sms.sendSms);
  sms.sendSms = smsSpy;

  describe('#sendVerificationSms()', () => {
    const smsContent = `Hello ${name}! Your Harvestree verification code is: ${code}`;

    it('Sends the verification code to the user', () => {
      sms.sendVerificationSms(name, code);
      expect(smsSpy).to.have.been.called();
      expect(sms.content).to.be.equal(smsContent);
    });
  });
});
