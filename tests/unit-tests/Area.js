import { expect } from 'chai';
import mongodb from 'mongodb';
import faker from 'faker';

import Area from '../../src/models/Area';

const ObjectID = mongodb.ObjectID;

describe('Area', () => {
  const circleArea = new Area({
    type: faker.random.word(),
    shape: 'circle',
    radius: faker.random.number(),
    center: { lat: faker.finance.amount(-90, 90, 4), lng: faker.finance.amount(-180, 180, 4) },
    description: faker.lorem.sentence(),
    farmId: new ObjectID().toString(),
    activityIds: [new ObjectID().toString()],
  });

  const polygonArea = new Area({
    type: faker.random.word(),
    shape: 'polygon',
    radius: faker.random.number(),
    center: { lat: faker.finance.amount(-90, 90, 4), lng: faker.finance.amount(-180, 180, 4) },
    description: faker.lorem.sentence(),
    farmId: new ObjectID().toString(),
    activityIds: [new ObjectID().toString()],
  });

  describe('#isValid', () => {
    it('Checks if model conforms to schema', () => {
      expect(circleArea.isValid()).to.equal(true);
    });

    it('Returns false for valid document of an area', () => {
      expect(polygonArea.isValid()).to.equal(false);
    });
  });
});
