import { expect } from 'chai';
import faker from 'faker';

import Farm from '../../src/models/Farm';
import User from '../../src/models/User';

import app from '../../src/app';

describe('Farm', () => {
  describe('#isValid()', () => {
    const farmOwner = new User({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      username: faker.internet.userName(),
      dateOfBirth: faker.date.past(),
      password: '123456',
      confirmPassword: '123456',
      contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
      isVerified: true,
    });

    const randomEmployee1 = new User({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      username: faker.internet.userName(),
      dateOfBirth: faker.date.past(),
      password: '123456',
      confirmPassword: '123456',
      contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
      isVerified: true,
    });

    const randomEmployee2 = new User({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      username: faker.internet.userName(),
      dateOfBirth: faker.date.past(),
      password: '123456',
      confirmPassword: '123456',
      contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
      isVerified: true,
    });

    it('Returns true for valid document of a farm', async () => {
      const owner1 = await app.service('api/users').create(farmOwner);
      const employee1 = await app.service('api/users').create(randomEmployee1);
      const employee2 = await app.service('api/users').create(randomEmployee2);

      const farm = new Farm({
        name: 'Farm 1',
        description: 'Description 1',
        ownerId: owner1._id.toString(),
        center: { lat: 123, lng: 456 },
        employeeIds: [employee1._id.toString(), employee2._id.toString()],
      });

      expect(farm.isValid()).to.be.true;
    });
  });

  describe('#isValid()', () => {
    const farm2 = new Farm({
      name: `${faker.random.word()} tree`,
      center: { lat: faker.random.number(), lng: faker.random.number() },
    });

    it('Returns false for valid document of a farm', () => {
      expect(farm2.isValid()).to.be.false;
    });
  });

  describe('Farm service', () => {
    const service = app.service('/api/farms');

    const farmOwner = new User({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      username: faker.internet.userName(),
      dateOfBirth: faker.date.past(),
      password: '123456',
      confirmPassword: '123456',
      isVerified: true,
      contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
    });

    const randomEmployee1 = new User({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      username: faker.internet.userName(),
      dateOfBirth: faker.date.past(),
      password: '123456',
      confirmPassword: '123456',
      isVerified: true,
      contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
    });

    const randomEmployee2 = new User({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      username: faker.internet.userName(),
      dateOfBirth: faker.date.past(),
      password: '123456',
      confirmPassword: '123456',
      isVerified: true,
      contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
    });

    it('Creates a new farm', async () => {
      const owner1 = await app.service('api/users').create(farmOwner);
      const employee1 = await app.service('api/users').create(randomEmployee1);
      const employee2 = await app.service('api/users').create(randomEmployee2);

      const farm = new Farm({
        name: faker.random.word(),
        description: faker.random.words(5),
        ownerId: owner1._id.toString(),
        center: { lat: faker.finance.amount(-90, 90, 4), lng: faker.finance.amount(-180, 180, 4) },
        employeeIds: [employee1._id.toString(), employee2._id.toString()],
      });

      const result = await service.create(farm);

      expect(result).to.be.an.instanceOf(Object);
      expect(result).to.have.property('name', farm.name);
      expect(result).to.have.property('description', farm.description);
      expect(result).to.have.deep.property('ownerId', owner1._id.toString());
      expect(result).to.have.deep.property('center',
        {
          lat: parseFloat(farm.center.lat),
          lng: parseFloat(farm.center.lng),
        },
      );
      expect(result).to.have.deep.property('employeeIds', [employee1._id.toString(), employee2._id.toString()]);
      expect(result).to.have.property('createdAt');
      expect(result).to.have.property('_id');
    });
  });
});
