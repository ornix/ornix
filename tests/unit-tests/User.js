import { expect } from 'chai';
import faker from 'faker';
import User from '../../src/models/User';
import Farm from '../../src/models/Farm';
import app from '../../src/app';

describe('User', () => {
  describe('#isValid()', () => {
    const farm1 = new Farm({
      name: faker.random.word(),
      description: faker.random.words(5),
      center: { lat: faker.finance.amount(-90, 90, 4), lng: faker.finance.amount(-180, 180, 4) },
    });

    const farm2 = new Farm({
      name: faker.random.word(),
      description: faker.random.words(5),
      center: { lat: faker.finance.amount(-90, 90, 4), lng: faker.finance.amount(-180, 180, 4) },
    });

    it('Returns true for a valid document', async () => {
      const farmX = await app.service('api/farms').create(farm1);
      const farmY = await app.service('api/farms').create(farm2);

      const user = new User({
        firstName: faker.name.firstName(),
        middleName: faker.name.lastName(),
        lastName: faker.name.lastName(),
        address: `${faker.address.streetAddress()}, ${faker.address.city()}, ${faker.address.state()}, ${faker.address.country()}`,
        dateOfBirth: new Date(faker.date.past()),
        username: faker.internet.userName(),
        password: '123456',
        confirmPassword: '123456',
        contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
        farms: [farmX._id.toString(), farmY._id.toString()],
        isVerified: true,
      });

      expect(user.isValid()).to.be.true;
    });
  });

  describe('#isValid()', () => {
    const user = new User({
      middleName: 'Sambo',
      lastName: 'Arevalo',
      address: 'Villena St., Cadiz City, Neg. Occ.',
      dateOfBirth: new Date('March 01 1996'),
    });

    it('Returns false for an invalid document', () => {
      expect(user.isValid()).to.be.false;
    });
  });
});

describe('User service', () => {
  const service = app.service('/api/users');

  describe('#create()', () => {
    const user = new User({
      firstName: faker.name.firstName(),
      middleName: faker.name.lastName(),
      lastName: faker.name.lastName(),
      address: `${faker.address.streetAddress()}, ${faker.address.city()}, ${faker.address.state()}, ${faker.address.country()}`,
      dateOfBirth: new Date(faker.date.past()),
      username: faker.internet.userName(),
      contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
      password: '123456',
      confirmPassword: '123456',
    });

    it('Creates a new user', async () => {
      const result = await service.create(user);
      result.dateOfBirth = result.dateOfBirth.toUTCString();
      expect(result).to.be.an.instanceOf(Object);
      expect(result).to.have.property('firstName', user.firstName);
      expect(result).to.have.property('middleName', user.middleName);
      expect(result).to.have.property('lastName', user.lastName);
      expect(result).to.have.property('address', user.address);
      expect(result).to.have.property('dateOfBirth', new Date(result.dateOfBirth).toUTCString());
      expect(result).to.have.property('username', user.username);
      expect(result).to.have.property('password');
      expect(result).to.not.have.property('confirmPassword');
      expect(result).to.have.property('createdAt');
      expect(result).to.have.property('_id');
      expect(result).to.have.property('isVerified');
      expect(result.isVerified).to.be.true;
    });

    it('Disallows account creation of duplicate username', async () => {
      try {
        await service.create(user);
      } catch (error) {
        expect(error.message).to.equal('Username already exists.');
      }
    });
  });
});
