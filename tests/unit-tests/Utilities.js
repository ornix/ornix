import { expect } from 'chai';
import utilities from '../../src/utilities';

describe('Utilities', () => {
  describe('#capitalize', () => {
    it('Returns a string with capitalized first letters at every space', () => {
      const sentence = 'lorem ipsum dolor sit amet.';
      const capitalizedSentence = utilities.capitalize(sentence);
      expect(capitalizedSentence).to.equal('Lorem Ipsum Dolor Sit Amet.');
    });
  });
});
