import { expect } from 'chai';
import faker from 'faker';
import mongodb from 'mongodb';

import partialValidation from '../../src/hooks/partialValidation';
import User from '../../src/models/User';
import Farm from '../../src/models/Farm';

const ObjectID = mongodb.ObjectID;

describe('partialValidation', () => {
  const partialUser = new User({
    username: faker.internet.userName(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    middleName: faker.name.lastName(),
    address: faker.address.streetAddress(),
  });
  const partialFarm = new Farm({
    name: faker.company.companyName(),
    description: faker.company.catchPhraseDescriptor(),
    employeeIds: [
      new ObjectID().toString(),
      new ObjectID().toString(),
      new ObjectID().toString(),
      new ObjectID().toString(),
    ],
    ownerId: new ObjectID().toString(),
  });
  it('Validates a fraction of the properties of user', async () => {
    const hookData = await partialValidation(User.schema)({ data: partialUser });
    expect(hookData).to.haveOwnProperty('data');
    expect(hookData.data).to.deep.equal(partialUser);
  });
  it('Validates a fraction of the properties of farm', async () => {
    const hookData = await partialValidation(Farm.schema)({ data: partialFarm });
    expect(hookData).to.haveOwnProperty('data');
    expect(hookData.data).to.deep.equal(partialFarm);
  });
});
