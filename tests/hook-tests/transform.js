import { expect } from 'chai';
import faker from 'faker';
import transform from '../../src/hooks/transform';

import Farm from '../../src/models/Farm';

describe('transform', () => {
  const farm1 = {
    name: faker.company.companyName(),
    description: faker.company.catchPhraseDescriptor(),
    center: { lat: 123, lng: 456 },
  };
  const farm2 = {
    name: faker.company.companyName(),
    description: faker.company.catchPhraseDescriptor(),
    center: { lat: 123, lng: 456 },
  };
  const farms = [farm1, farm2];

  describe('array', () => {
    it('Transforms an array of objects into an array of farms', async () => {
      const transformedFarms = await transform(Farm)({ type: 'after', result: farms });
      const isAllFarmModels = transformedFarms.result.every(farm => farm.constructor.name === 'Farm');
      expect(isAllFarmModels).to.be.true;
    });
  });
  describe('object', () => {
    it('Transforms an object if there is no \'isPaginated\' property on the object.', async () => {
      const transformedFarm = await transform(Farm)({ type: 'after', result: farm1 });
      const isTransformed = transformedFarm.result.constructor.name === 'Farm';
      expect(isTransformed).to.be.true;
    });
  });
  describe('paginated result', () => {
    it('Transforms the data property if the object has an \'isPaginated\' property.', async () => {
      const paginatedResult = { isPaginated: true, data: farms };
      const transformedData = await transform(Farm)({ type: 'after', result: paginatedResult });
      const isDataTransformed = transformedData.result.data.every(farm => farm.constructor.name === 'Farm');
      expect(isDataTransformed).to.be.true;
    });
  });
});
