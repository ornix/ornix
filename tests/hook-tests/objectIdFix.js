import mongodb from 'mongodb';
import { expect } from 'chai';

import objectIdFix from '../../src/hooks/objectIdFix';

const ObjectID = mongodb.ObjectID;

describe('ObjectID Fix Hook', () => {
  describe('#objectIdFix(hook)', () => {
    it('Transforms an _id string on a hook query to an ObjectID instance', () => {
      const hook = {
        params: {
          query: {
            _id: {
              $in: [new ObjectID().toString()],
            },
            details: '...other details...',
          },
        },
      };

      const hookData = objectIdFix(hook);
      expect(hookData.params.query._id.$in[0]).to.be.an.instanceOf(ObjectID);
    });
  });
});
