import _ from 'underscore';
import Joi from 'joi';

function partialValidation(schema) { // for patch methods
  return function execute(hook) {
    const { data } = hook;
    const { doc } = data;
    const keys = Object.keys(doc);
    const filteredSchema = _(schema).pick(...keys);
    const validation =
      Joi.validate(doc, filteredSchema, { convert: true, abortEarly: false, stripUnknown: true });
    if (validation.errors) {
      throw new Error(validation.errors);
    }
    if (validation.error) {
      throw new Error(validation.error);
    }
    return hook;
  };
}

export default partialValidation;
