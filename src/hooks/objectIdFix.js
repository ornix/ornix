import { ObjectID } from 'mongodb';

/**
 * This function converts the _id found in hook.params.query
 * during object population.
 *
 * Without this function, the query will not work because
 * mongodb needs the 'ids' to be of type ObjectID.
 */
function objectIdFix(hook) {
  const _id = hook.params.query._id;

  if (_id && _id.$in) {
    _id.$in = _id.$in.map(current => new ObjectID(current));
  }

  return hook;
}

export default objectIdFix;
