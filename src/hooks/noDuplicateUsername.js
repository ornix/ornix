/**
 * This hook checks the database if the username that is about
 * to be registered is already taken.
 *
 * Throws an error if the username exists.
 */
function noDuplicateUsername() {
  return async function execute(hook) {
    const usersService = hook.app.service('/api/users');
    const result = await usersService.find({ query: { username: hook.data.username } });
    if (result.total !== 0) {
      throw new Error('Username already exists.');
    }
    return hook;
  };
}

module.exports = noDuplicateUsername;
