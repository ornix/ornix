/* eslint-disable no-param-reassign */
// We have to make our own hook for this
// since feathers' 'restrictToOwner' doesn't support
// an array field for user
// the thing is, tree holds the farmId
// then, the user holds an array of farmIds
// so it gets a bit messy
import errors from 'feathers-errors';
import modifyQuery from './modifyQuery';
import updateUser from './updateUser';

const defaults = {
  farmField: 'farmId',
  roles: ['owner', 'manager', 'employee'],
};

function restrict(options = {}) {
  return async function exec(hook) {
    await updateUser()(hook);

    const { params, type, method, id, path, data } = hook;
    const { provider, user } = params;
    const allowedMethods = ['get', 'find', 'create', 'update', 'patch', 'remove'];

    options = Object.assign({}, defaults, options);
    const { farmField, roles } = options;

    if (!provider) {
      return hook;
    }

    if (type !== 'before') {
      throw new Error('This hook should only be used as a \'before\' hook.');
    }

    if (!allowedMethods.find(allowedMethod => allowedMethod === method)) {
      throw new Error('Hook should only be used in \'get\', \'create\', \'patch\', \'find\', \'update\', or \'remove\'.');
    }

    if (!user) {
      throw new errors.NotAuthenticated('User is missing, you must be logged in.');
    }

    if (!id) {
      if (method === 'create') {
        const farmId = data[farmField];
        const farmIds = hook.params.user.getRelatedFarms(...roles);

        if (!farmId) {
          throw new Error(`Farm field ${farmField} doesn't exist.`);
        }

        if (!farmIds.includes(farmId)) {
          throw new Error(`User is not allowed to use '${method}' in '${path}'.`);
        }
        return hook;
      }
      // modify the query if there's no id
      return modifyQuery(options)(hook);
    }


    // to avoid infinite loop,
    // make it an internal call
    const customParams = Object.assign({}, hook.params, { provider: undefined });

    const farmIds = user.getRelatedFarms(...roles);
    const result = await hook.service.get(id, customParams);
    const authorized = farmIds.includes(result[farmField].toString());

    if (!authorized) {
      throw new errors.Forbidden(`User is not allowed to use '${method}' in '${path}'.`);
    }
    return hook;
  };
}

export default restrict;
