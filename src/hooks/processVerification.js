/* eslint-disable no-param-reassign */

import SMS from '../models/Sms';

async function sendCode(hook) {
  if (!hook.params.provider) {
    const sms = new SMS(hook.result.contactNumber);
    await sms.sendVerificationSms(
      hook.result.firstName, hook.result.verifyShortToken
      || hook.result.resetShortToken,
    );
  }
  return hook;
}

async function renewCode(hook) {
  if (hook.method !== 'patch' && hook.type !== 'after') {
    throw new Error('Call error: Must only be used on AFTER:PATCH operation');
  }

  if ((!hook.data.isVerified && hook.data.verifyShortToken) || hook.data.resetShortToken) {
    await sendCode(hook);
  }

  return hook;
}

function internalVerification(hook) {
  if (hook.method !== 'create' && hook.type !== 'before') {
    throw new Error('Call error: Must only be used on BEFORE:CREATE operation');
  }

  if (process.env.NODE_ENV !== 'production' && process.env.SMS !== 'on') {
    hook.data.isVerified = true;
  }
  delete hook.params.provider;
  return hook;
}

export { sendCode, renewCode, internalVerification };
