/* eslint-disable no-param-reassign */

function mark() {
  return function execute(hook) {
    const couldBeUsed = hook.method === 'find';

    if (!couldBeUsed) {
      throw new Error('Hook must only be used with find');
    }
    const { service } = hook;
    const isPaginated = Object.keys(service.paginate).length > 0;
    hook.result.isPaginated = isPaginated;
    return hook;
  };
}

export default mark;
