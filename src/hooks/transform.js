/*
 * A transform hook for a 'single or an array of' mongo documents.
 *
 * @param {*} Model - the target model class to receive the transformation.
 */

/* eslint-disable no-param-reassign */
function transform(ModelClass) {
  return async function execute(hook) {
    const property = hook.type === 'before' ? 'data' : 'result';

    if (hook[property] instanceof Array) {
      hook[property] = hook[property].map(object => new ModelClass(object));
    } else if (hook[property] instanceof Object) {
      const object = hook[property];
      const { isPaginated } = object;
      const hasPagination = isPaginated;

      if (hasPagination) {
        object.data = object.data.map(obj => new ModelClass(obj));
      } else {
        hook[property] = new ModelClass(hook[property]);
      }
    } else {
      throw new Error('Invalid data type given');
    }
    return hook;
  };
}

export default transform;
