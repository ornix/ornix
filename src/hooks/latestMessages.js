/* eslint-disable no-param-reassign */

export default function (hook) {
  hook.params.query.$sort = {
    createdAt: -1,
  };
  return hook;
}
