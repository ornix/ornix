// we need to modify the query so that we could limit
// the docs the user could access

/* eslint-disable no-param-reassign */
import { ObjectID } from 'mongodb';

const defaults = {
  farmField: 'farmId',
  roles: ['owner'],
};

function modify(options = {}) {
  return function exec(hook) {
    const { type, method, params } = hook;
    const { user } = params;
    const disallowedMethods = ['create'];

    if (type !== 'before') {
      throw new Error('Modifyquery hook should only be used as a before hook.');
    }

    if (disallowedMethods.find(disallowedMethod => method === disallowedMethod)) {
      throw new Error('Modifyquery hook isn\'t allowed to be used with create.');
    }

    options = Object.assign({}, defaults, options);
    const { farmField, roles } = options;
    const farmIds = user.getRelatedFarms(...roles);

    const newQuery = {};
    newQuery[farmField] = { $in: farmIds };
    if (newQuery._id && newQuery._id.$in) { // gawd ds s atrocious
      newQuery._id.$in = newQuery._id.$in.map(id => new ObjectID(id));
    }

    const actualQueryKeys = Object.keys(hook.params.query).filter(query => !query.startsWith('$'));
    const topLevelQueryKeys = Object.keys(hook.params.query).filter(query => query.startsWith('$'));

    const actualQueries = {};
    actualQueryKeys.forEach((key) => {
      actualQueries[key] = hook.params.query[key];
    });

    const query = { $and: [actualQueries, newQuery] };
    topLevelQueryKeys.forEach((key) => {
      query[key] = hook.params.query[key];
    });

    hook.params.query = query;

    return hook;
  };
}

export default modify;
