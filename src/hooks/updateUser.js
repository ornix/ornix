function update() {
  return async function exec(hook) {
    if (!hook.params.provider) {
      return hook;
    }

    const { user } = hook.params;
    if (user) {
      const service = hook.app.service('api/users');
      hook.params.user = await service.get(user._id); // eslint-disable-line
    }
    return hook;
  };
}

export default update;
