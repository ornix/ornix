function isAction(...args) {
  return function execute(hook) {
    return args.includes(hook.data.action);
  };
}

export default isAction;
