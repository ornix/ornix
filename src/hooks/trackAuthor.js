// tracking is the field where the current user's
// user id will be attached. for example, in activities
// say, trackingField will be 'authorId', then at the
// end of the hook, trackingField would then have an extra
// field which is 'authorId' that has the current user's id
// as it's value.
function track(trackingField) {
  return function exec(hook) {
    if (!hook.params.provider) {
      return hook;
    }

    const { data, type, params } = hook;
    const { user } = params;
    const allowedMethods = ['create', 'update', 'patch'];
    const isAllowed = allowedMethods.some(method => method === hook.method);

    if (type !== 'before') {
      throw new Error('TrackAuthor could only be used as a before hook.');
    }
    if (!isAllowed) {
      throw new Error('TrackAuthor is only allowed to be used on \'create\', \'update\', or \'patch\'.');
    }

    data[trackingField] = user._id;
    return hook;
  };
}

export default track;
