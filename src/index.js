import logger from 'winston';
import app from './app';

const port = process.env.PORT || app.get('port');
const server = app.listen(port);

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason),
);

server.on('listening', () =>
  logger.info(`Harvestree is live on http://${app.get('host')}:${port}`),
);
