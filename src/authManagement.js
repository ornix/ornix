import { iff } from 'feathers-hooks-common';
import authentication from 'feathers-authentication';
import authManagement from 'feathers-authentication-management';
import isAction from './hooks/isAction';

function manage() {
  const app = this;
  const options = {
    service: 'api/users',
    identifyUserProps: ['username'],
  };

  app.configure(authManagement(options));

  app.service('authManagement').hooks({
    before: {
      create: [
        iff(isAction('passwordChange', 'identityChange'), authentication.hooks.authenticate('jwt')),
      ],
    },
  });
}

export default manage;
