/* eslint-disable no-param-reassign */
import _ from 'underscore';

function autobind(target, key, descriptor) {
  const fn = descriptor.value;

  if (typeof fn !== 'function') {
    throw new Error(`Decorator accepts only methods not: ${typeof fn}`);
  }

  function boundFunction() {
    let boundfn = fn.bind(this);

    // for inheritance
    if (target.constructor !== this.constructor) {
      boundfn = fn.bind(target);
    }
    return boundfn;
  }

  descriptor.get = boundFunction;

  // can only use either one of the ff:
  // writable and value
  // get and set
  // we're not using set so I'll set aside the set property
  return _(descriptor).omit('value', 'writable');
}

export default autobind;
