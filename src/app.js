import path from 'path';
import favicon from 'serve-favicon';
import compress from 'compression';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';

import feathers from 'feathers';
import configuration from 'feathers-configuration';
import hooks from 'feathers-hooks';
import rest from 'feathers-rest';
import socketio from 'feathers-socketio';

import handler from 'feathers-errors/handler';
import notFound from 'feathers-errors/not-found';

import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';

import authentication from './authentication';
import authManagement from './authManagement';

import mongodb from './mongodb';

const app = feathers();

// Load app configuration
app.configure(configuration());
// Enable CORS, security, compression, favicon and body parsing
app.use(cors());
app.use(helmet());
app.use(compress());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));


// Host the public folder
app.use('/', feathers.static(app.get('public')));

// Host the html file
app.use('*', feathers.static(path.join(app.get('public'), 'index.html')));

// Set up Plugins and providers
app.configure(hooks());
app.configure(mongodb);
app.configure(rest());
app.configure(socketio());

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
app.configure(authManagement);
// Set up our services (see `services/index.js`)
app.configure(services);
// Configure a middleware for 404s and the error handler
app.use(notFound());
app.use(handler());


app.hooks(appHooks);

export default app;
