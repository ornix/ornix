/* eslint-env jest */

let feathers = jest.genMockFromModule('feathers');
/**
 * Generates a mock object of Feathers for jest
 * so that the test will not actually call the
 * feathers instance during run.
 *
 * Either way, the test will crash if the Feathers module
 * is not mocked.
 *
 */
feathers = () => ({
  configure() {
    return this;
  },
  on() {
    return this;
  },
});

module.exports = feathers;
