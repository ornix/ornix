import feathers from 'feathers';
import authClient from 'feathers-authentication-client';
import hooks from 'feathers-hooks';
import setupSocketio from './socketio';
import callService from './callService';
import setupAllServices from './setupAllServices';
import isLoggedIn from './isLoggedIn';

const app = feathers()
   .configure(hooks())
   .configure(setupSocketio())
   .configure(authClient({ storage: window.localStorage }))
   .configure(setupAllServices());

app.on('logout', () => {
  window.location.href = '/login';
});

app.callService = callService;
app.isLoggedIn = isLoggedIn;
window.app = app;

export default app;
