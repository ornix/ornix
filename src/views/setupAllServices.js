import setupService from './setupService';

import activitiesHooks from './hooks/activities.hooks';
import areasHooks from './hooks/areas.hooks';
import farmsHooks from './hooks/farms.hooks';
import treesHooks from './hooks/trees.hooks';
import usersHooks from './hooks/users.hooks';
import uploadHooks from './hooks/upload.hooks';
import messagesHooks from './hooks/messages.hooks';

function setupAllServices() {
  return function setupPlugins() {
    const app = this;

    app
    .configure(setupService('activities', activitiesHooks))
    .configure(setupService('areas', areasHooks))
    .configure(setupService('farms', farmsHooks))
    .configure(setupService('trees', treesHooks))
    .configure(setupService('users', usersHooks))
    .configure(setupService('uploads', uploadHooks))
    .configure(setupService('messages', messagesHooks));
  };
}

export default setupAllServices;
