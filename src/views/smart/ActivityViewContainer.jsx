import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import { Dialog, FlatButton, Snackbar } from 'material-ui';
import MediaQuery from 'react-responsive';

import ActivityView from '../dumb/activityView';
import ImageGallery from '../dumb/imageGallery';
import client from '../client';
import autobind from '../../decorators/autobind';
import ImagePreview from '../dumb/imagePreview';
import ConfirmationModal from '../dumb/confirmationModal';
import Activity from '../../models/Activity';
import User from '../../models/User';
import Spinner from '../dumb/spinner';

const styles = {
  img: {
    width: '100%',
    height: '100%',
  },
};

class ActivityViewContainer extends React.Component {
  constructor(props) {
    super(props);
    this.query = props.location.search;
    this.activityId = props.match.params.id;
    this.state = {
      open: false,
      snackbarMessage: '',
      showSnackbar: false,
      modify: false,
      images: [],
      formErrors: {},
      openConfirmation: false,
      upload: false,
      remove: false,
      removeActvity: false,
      isFullPreview: false,
    };
  }

  async componentWillMount() {
    const stripWord = string => string.replace(/[^a-zA-Z]/, '');
    const message = {
      isCreated: 'Activity added successfully!',
    };
    const hasAMessage = Object.keys(message).includes(stripWord(this.query));
    const outputMessage = message[stripWord(this.query)];
    if (hasAMessage) {
      this.showMessage(outputMessage);
    }

    await this.getActivity();
  }

  @autobind
  onChange(pictures) {
    this.setState({ images: pictures });
  }

  @autobind
  onFileLoad(e) {
    this.setState({
      file: e.target.result,
    });

    const pictures = this.state.images;

    pictures.push(this.state.file);

    this.setState({ images: pictures, remove: false });
    this.onChange(pictures);
  }

  async getActivity() {
    const activityService = client.service('/api/activities');
    const currentActivity = await activityService.get(this.activityId);
    const author = new User(currentActivity.author);

    this.getImageUri(currentActivity.images);
    this.setState({
      activity: currentActivity,
      preservedActivity: currentActivity,
      author,
    });
  }

  @autobind
  getImageUri(images) {
    const imageUris = images.map(image => `/uploads/${image}`);
    this.setState({
      images: imageUris,
    });
  }

  @autobind
  showErrors() {
    const user = new Activity(this.state.activity);
    const validation = user.validate();
    const errors = validation.error;
    const formErrors = {};

    if (errors) {
      errors.details.forEach((error) => {
        formErrors[error.path] = error.message;
      });
    }
    this.setState({ formErrors });
  }

  @autobind
  hasErrors() {
    const { formErrors } = this.state;
    const errors = Object.keys(formErrors);
    return errors.length > 0;
  }

  @autobind
  handleOpen(action) {
    if (this.state.modify) {
      this.setState({ open: false, openConfirmation: true, removeActvity: false });
    } else if (action === 'preview') {
      this.setState({ open: true, openConfirmation: false, removeActvity: false });
    } else {
      this.setState({ open: false, openConfirmation: false, removeActvity: true });
    }
  }

  @autobind
  handleClose() {
    this.setState({
      open: false,
      upload: false,
      modify: false,
      openConfirmation: false,
      removeActvity: false,
      images: [],
      activity: {},
      isFullPreview: false,
    });

    this.setState({
      activity: this.state.preservedActivity,
    });

    this.getImageUri(this.state.activity.images);
  }

  @autobind
  modifyActivity() {
    this.setState({ modify: true });
  }

  @autobind
  handleRemove(image) {
    const pictures = this.state.images;
    this.remove(pictures, image);
  }

  @autobind
  handleRemoveAll() {
    const pictures = this.state.images;
    this.removeAll(pictures);
  }

  @autobind
  async handleRemoveActivity() {
    const activityService = await client.callService('activities');
    activityService.remove(this.props.match.params.id);
    history.back();
  }

  @autobind
  handleInputChange(event, data) {
    const activityStateData = this.state.activity;
    const value = data;
    if (!event) {
      activityStateData.date = value;
    } else {
      const name = event.target.name;
      activityStateData[name] = value;
    }
    this.setState({ activity: activityStateData });
  }

  @autobind
  handleCancelChanges() {
    this.setState({ openConfirmation: true });
  }

  @autobind
  handleCancelDiscardChanges() {
    this.setState({ openConfirmation: false });
  }

  @autobind
  async handleSubmit(event) {
    event.preventDefault();
    const activityService = await client.callService('activities');
    const activityData = this.state.activity;

    if (this.state.remove && !this.state.upload) {
      activityData.images = this.state.images.map((image) => {
        const x = image.split('/');
        return x[2];
      });
    } else if (this.state.upload) {
      const uploadedImage = await this.handleUpload();
      uploadedImage.forEach((image) => {
        activityData.images.push(image.id);
      });
    } else {
      activityData.images = this.state.images.map((image) => {
        const x = image.split('/');
        return x[2];
      });
    }

    delete activityData._id;

    const activity = new Activity(activityData);

    if (activity.isValid()) {
      await activityService.patch(this.props.match.params.id, activity);
    }

    this.handleClose();
  }

  async handleUpload() {
    const promise = [];
    const uploadService = await client.callService('uploads');
    const pictures = this.state.images;

    if (pictures && pictures.length) {
      pictures.filter((picture) => {
        if (picture.startsWith('data')) {
          const uploadeImagePromise = uploadService.create({
            uri: `${picture} `,
          });
          promise.push(uploadeImagePromise);
        }
        return null;
      });
    }
    return Promise.all(promise);
  }

  remove(array, element) {
    const index = array.indexOf(element);
    array.splice(index, 1);
    this.onChange(array);
    this.setState({ remove: true });
  }

  removeAll(array) {
    array.splice(0, array.length);
    this.onChange(array);
    this.setState({ remove: true });
  }

  @autobind
  imageView(src) {
    this.handleOpen('preview');
    this.setState({
      imageSrc: src,
      isFullPreview: true,
    });
  }

  @autobind
  showMessage(message) {
    this.setState({
      showSnackbar: true,
      snackbarMessage: message,
    });
  }

  @autobind
  closeSnackBar() {
    this.setState({
      showSnackbar: false,
    });
  }

  get isLoading() {
    const { activity, author } = this.state;
    const loadingVars = [activity, author];
    return !loadingVars.every(variable => variable);
  }

  render() {
    if (this.isLoading) {
      return <Spinner />;
    }
    const imageGallery = (
      this.state.modify ? (<ImagePreview
        uriList={this.state.images}
        onChange={this.onChange}
        remove={this.handleRemove}
        removeAll={this.handleRemoveAll}
      />) :
      (<ImageGallery
        uriList={this.state.images}
        imageView={(src) => {
          this.imageView(src);
        }}
        isFullPreview={this.state.isFullPreview}
      />)
    );

    const actions = [
      <FlatButton
        key="closeButton"
        label="close"
        primary
        onClick={this.handleClose}
      />,
    ];

    return (
      <div>

        {/* For tablets, laptops, desktops, large monitors */}
        <MediaQuery minWidth={768}>
          <Grid fluid style={{ top: 50, position: 'relative', zIndex: 2, margin: 0, padding: 0 }}>
            <Row center="xs">
              <Col sm={6} xs={10} style={{ marginTop: '40px' }}>
                <ActivityView
                  activity={this.state.activity}
                  imageGallery={imageGallery}
                  modifyActivity={() => { this.modifyActivity(); }}
                  modify={this.state.modify}
                  onInputChange={this.handleInputChange}
                  cancelChanges={this.handleCancelChanges}
                  saveChanges={this.handleSubmit}
                  onFileLoad={this.onFileLoad}
                  formErrors={this.state.formErrors}
                  removeActivity={() => {
                    this.handleOpen('remove');
                  }}
                  author={this.state.author}
                  upload={() => {
                    this.setState({ upload: true });
                  }}
                />
              </Col>
            </Row>
          </Grid>
        </MediaQuery>

        {/* For mobile phones and other devices which has a with of less than 767px */}
        <MediaQuery maxWidth={767}>
          <Grid fluid style={{ margin: 0, padding: 0 }}>
            <Row center="xs">
              <Col xs={12}>
                <ActivityView
                  activity={this.state.activity}
                  imageGallery={imageGallery}
                  modifyActivity={() => { this.modifyActivity(); }}
                  modify={this.state.modify}
                  onInputChange={this.handleInputChange}
                  cancelChanges={this.handleCancelChanges}
                  saveChanges={this.handleSubmit}
                  onFileLoad={this.onFileLoad}
                  formErrors={this.state.formErrors}
                  removeActivity={() => {
                    this.handleOpen('remove');
                  }}
                />
              </Col>
            </Row>
          </Grid>
        </MediaQuery>

        <Snackbar
          open={this.state.showSnackbar}
          message={this.state.snackbarMessage}
          autoHideDuration={5000}
          onRequestClose={this.closeSnackBar}
        />

        <Dialog
          modal={false}
          actions={actions}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <img style={styles.img} src={this.state.imageSrc} alt={this.state.imageSrc} />
        </Dialog>

        <ConfirmationModal
          key="discard changes"
          open={this.state.openConfirmation}
          handleClose={this.handleCancelDiscardChanges}
          dialogContent={[]}
          dialogTitle="Discard Changes?"
          handleDone={this.handleClose}
        />

        <ConfirmationModal
          key="remove activity"
          open={this.state.removeActvity}
          handleClose={this.handleClose}
          dialogContent={[]}
          dialogTitle="Are you sure you want to delete this activity?"
          handleDone={() => {
            this.handleRemoveActivity();
          }}
        />
      </div>
    );
  }

}

ActivityViewContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  location: PropTypes.shape({
    search: PropTypes.string.isRequired,
  }),
};

ActivityViewContainer.defaultProps = {
  location: { search: '' },
};

export default ActivityViewContainer;
