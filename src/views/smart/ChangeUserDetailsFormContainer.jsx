import React from 'react';
import _ from 'underscore';
import Snackbar from 'material-ui/Snackbar';

import User from '../../models/User';
import client from '../client';
import CurrentUser from '../user';
import ChangeUserDetailsForm from '../dumb/changeUserDetailsForm';
import Spinner from '../dumb/spinner';

class ChangeUserDetailsFormContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      hasLoaded: false,
      formErrors: {},
      dirtyFields: [],
      updateSuccess: 0, // 0 - neutral, 1 - success, 2 - fail
    };
    this.fields = [
      'firstName',
      'middleName',
      'lastName',
      'address',
      'contactNumber',
      'dateOfBirth',
    ];
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleOnBlur = this.handleOnBlur.bind(this);
    this.isDirty = this.isDirty.bind(this);
    this.removeFieldError = this.removeFieldError.bind(this);
    this.hasFormErrors = this.hasFormErrors.bind(this);
  }

  async componentDidMount() {
    await this.getCurrentUser();
  }

  async getCurrentUser() {
    const userObject = await CurrentUser();
    const restrictedFields = ['username', 'createdAt'];
    const userProfile = _(userObject).omit(...restrictedFields);
    const user = new User(userProfile);
    const { dateOfBirth } = user;
    user.dateOfBirth = new Date(dateOfBirth);
    this.setState({ user, hasLoaded: true });
  }

  /**
 * Handles visual validation cues for the form.
 */
  handleOnBlur(event) {
    const dirtyFields = this.state.dirtyFields;
    dirtyFields.push(event.target.name);
    this.setState({ dirtyFields });
    this.showFormErrors();
  }

  /*
  * Converts the form into a user object and does a
  * validation check before submitting to the service.
  */
  handleSubmit(event) {
    event.preventDefault();
    if (!this.hasFormErrors()) {
      const user = new User(this.state.user.doc);
      this.update(user);
    } else {
      this.showFormErrors();
    }
  }

  /*
  * Handles state management and visual validation cues at
  * each user input-interaction.
  */
  handleInputChange(event, data) {
    if (data && !event) {
      const { user } = this.state;
      const updatedData = Object.assign(user, { dateOfBirth: data });
      this.setState({
        user: updatedData,
      });
    } else {
      this.removeFieldError(event.target.name);
      const name = event.target.name;
      const value = event.target.value;
      const userStateData = this.state.user;
      userStateData[name] = value;
      this.setState({ user: userStateData });
    }
  }

  /**
 * Checks if a field has been touched.
 */
  isDirty(field) {
    return this.state.dirtyFields.includes(field);
  }

  /**
   * Removes the error visual cue in the field and is paired with
   * the handleInputChange() method.
   */
  removeFieldError(field) {
    const formErrors = this.state.formErrors;
    delete formErrors[field];
    this.setState({ formErrors });
  }

  hasFormErrors() {
    return Object.keys(this.showFormErrors()).length > 0;
  }

  /*
  * Shows visual cues for form errors.
  */
  showFormErrors() {
    const { user } = this.state;
    const userData = new User(user);
    const validation = userData.validate();
    const errors = validation.error;
    const formErrors = {};
    if (errors) {
      errors.details.forEach((error) => {
        if (this.isDirty(error.path)) {
          formErrors[error.path] = error.message;
        }
      });
    }
    this.setState({ formErrors });
    return formErrors;
  }

  /*
  * Calls the user service and creates a new user.
  */
  async update() {
    const { user } = this.state;
    const { _id } = user;
    const service = client.service('api/users');
    try {
      const response = await service.patch(_id, user);

      if (response._id) {
        this.setState({
          updateSuccess: 1,
        });
      } else {
        this.setState({
          updateSuccess: 2,
        });
      }
    } catch (error) {
      this.setState({
        updateSuccess: 2,
      });
    }
  }

  get renderedComponent() {
    if (this.state.hasLoaded) {
      return (
        <div>
          <form onSubmit={this.handleSubmit}>
            <ChangeUserDetailsForm
              user={this.state.user}
              onInputChange={this.handleInputChange}
              formErrors={this.state.formErrors}
              onBlur={this.handleOnBlur}
            />
          </form>
        </div>
      );
    }
    return <Spinner />;
  }

  render() {
    return (
      <div>
        {this.renderedComponent}
        <Snackbar
          open={this.state.updateSuccess !== 0}
          message={
            this.state.updateSuccess === 1 ? 'Update Successful!' : 'Update Failed! Please check the form.'
          }
          autoHideDuration={4000}
          onRequestClose={() => {
            this.setState({
              updateSuccess: 0,
            });
          }}
        />
      </div>
    );
  }
}

export default ChangeUserDetailsFormContainer;
