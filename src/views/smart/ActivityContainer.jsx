import React from 'react';
import { Tabs, Tab, Divider, RaisedButton } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import SwipeableViews from 'react-swipeable-views';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import Calendar from 'material-ui/svg-icons/action/today';
import Statistics from 'material-ui/svg-icons/editor/insert-chart';
import AddActivity from 'material-ui/svg-icons/av/library-add';
import ContentAddCircle from 'material-ui/svg-icons/content/add-circle';
import HighlightOff from 'material-ui/svg-icons/action/highlight-off';

import ActivityForm from '../dumb/activityForm';
import Activity from '../../models/Activity';
import client from '../client';
import ImagePreview from '../dumb/imagePreview';
import ProfitChart from '../dumb/profitChart';
import ActivityCalendar from '../dumb/activityCalendar';

class ActivityFormContainer extends React.Component {
  constructor(props) {
    super(props);
    this.markerId = props.match.params.id;
    this.markerType = props.match.params.type;

    this.state = {
      activity: { type: 'water', date: new Date() },
      formErrors: {},
      notify: false,
      pictures: [],
      remove: false,
      activities: [],
      slideIndex: 0,
      currentYearOnNavigation: new Date().getFullYear(),
      addActivity: false,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.hasErrors = this.hasErrors.bind(this);
    this.create = this.create.bind(this);
    this.handleDropDownChange = this.handleDropDownChange.bind(this);
    this.onFileLoad = this.onFileLoad.bind(this);
    this.onChange = this.onChange.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleRemoveAll = this.handleRemoveAll.bind(this);
    this.navigateToActivityProfile = this.navigateToActivityProfile.bind(this);
    this.addActivity = this.addActivity.bind(this);
    this.handleChangeTab = this.handleChangeTab.bind(this);
    this.onNavigate = this.onNavigate.bind(this);
    this.openAddActivity = this.openAddActivity.bind(this);
    this.closeAddActivity = this.closeAddActivity.bind(this);
  }

  async componentWillMount() {
    await this.retrieveActivities();
  }

  onNavigate(date) {
    this.setState({
      currentYearOnNavigation: new Date(date).getFullYear(),
    });
  }

  onFileLoad(e) {
    this.setState({
      file: e.target.result,
    });

    const pictures = this.state.pictures;

    pictures.push(this.state.file);

    this.setState({ pictures });
    this.onChange(pictures);
  }

  onChange(pictures) {
    this.setState({ pictures });
  }

  handleChangeTab(value) {
    this.setState({
      slideIndex: value,
    });
  }

  handleRemove(image) {
    const pictures = this.state.pictures;
    this.remove(pictures, image);
  }

  handleRemoveAll() {
    const pictures = this.state.pictures;
    this.removeAll(pictures);
  }

  remove(array, element) {
    const index = array.indexOf(element);
    array.splice(index, 1);
    this.onChange(array);
  }

  removeAll(array) {
    array.splice(0, array.length);
    this.onChange(array);
  }

  showErrors() {
    const user = new Activity(this.state.activity);
    const validation = user.validate();
    const errors = validation.error;
    const formErrors = {};

    if (errors) {
      errors.details.forEach((error) => {
        formErrors[error.path] = error.message;
      });
    }
    this.setState({ formErrors });
  }

  hasErrors() {
    const { formErrors } = this.state;
    const errors = Object.keys(formErrors);
    return errors.length > 0;
  }

  async addActivity(activity) {
    const currentMarker = this.state.marker;

    currentMarker.activityIds.push(activity._id);

    try {
      await this.service.patch(currentMarker._id, { activityIds: currentMarker.activityIds });
      const activities = this.state.activityList;
      activities.push(activity);

      this.setState({
        activityList: activities,
        marker: currentMarker,
      });
    } catch (error) {
      this.setState({
        error: {
          error,
          source: 'Add Activity',
        },
      });
    }
  }

  async create(activity) {
    try {
      const newActivity = await this.activityService.create(activity);
      await this.addActivity(newActivity);

      const newActivityCalendarSchema = this.convertToCalendarSchema(newActivity);
      this.setState({
        activities: [...this.state.activities, newActivityCalendarSchema],
        notify: true,
      });
      this.props.history.push({
        pathname: `/activityProfile/${this.markerType}/${newActivity._id}`,
        search: '?isCreated',
      });
      return newActivity;
    } catch (error) {
      this.setState({
        error: {
          error,
          source: 'Create Activity',
        },
      });
    }
    return null;
  }

  async handleSubmit(event) {
    event.preventDefault();
    const activityData = this.state.activity;
    const activity = new Activity(activityData);

    const x = await this.handleUpload();
    const uploadedImages = x.map(image => image.id);

    if (activity.isValid()) {
      activity.images = uploadedImages;
      activity.farmId = this.state.marker.farmId;
      await this.create(activity);
    }

    this.setState({
      pictures: [],
      activity: {
        type: this.state.activity.type,
        date: new Date(),
      },
    });
  }

  async handleUpload() {
    const pictures = this.state.pictures;
    const promise = [];
    const uploadService = await client.callService('uploads');

    if (pictures && pictures.length) {
      pictures.forEach((picture) => {
        const uploadeImagePromise = uploadService.create({
          uri: `${picture} `,
        });
        promise.push(uploadeImagePromise);
      });
    }
    return Promise.all(promise);
  }

  handleInputChange(event, data) {
    const activityStateData = this.state.activity;
    const value = data;
    if (!event) {
      activityStateData.date = value;
    } else {
      const name = event.target.name;
      activityStateData[name] = value;
    }
    this.setState({ activity: activityStateData });
    this.showErrors();
  }

  async retrieveActivities() {
    this.service = await client.callService(`${this.markerType}s`);

    this.activityService = await client.callService('activities');
    let marker;

    try {
      marker = await this.service.get(this.markerId);
    } catch (e) {
      this.props.history.push('/page-not-found');
      return;
    }

    const activities = marker.activities.map(activity => (this.convertToCalendarSchema(activity)));

    this.setState({
      activities,
      marker,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  convertToCalendarSchema(activity) {
    const activityData = activity;
    activityData.id = activity._id;
    activityData.title = activity.type;
    activityData.allDay = true;
    activityData.start = new Date(activityData.date);
    activityData.end = new Date(activityData.date);
    return activityData;
  }

  handleRequestClose() {
    this.setState({
      open: false,
    });
  }

  closeSnackBar() {
    this.setState({
      showSnackbar: false,
    });
  }

  handleDropDownChange(event, index, value) {
    const activity = this.state.activity;
    activity.type = value;
    this.setState({
      activity,
    });
  }

  navigateToActivityProfile(id) {
    this.props.history.push(`/activityProfile/${this.markerType}/${id}`);
  }

  openAddActivity() {
    this.setState({ addActivity: true });
  }

  closeAddActivity() {
    this.setState({ addActivity: false });
  }

  render() {
    return (
      <div>
        {/* For tablets, laptops, desktops, large monitors */}
        <MediaQuery minWidth={768}>
          <Grid fluid>

            { this.state.addActivity ?
              <div className="circular">
                <Row end="sm" style={{ margin: '4vh 0px 2vh 0px' }}>
                  <Col sm={12}>
                    <RaisedButton label="Close" primary onClick={this.closeAddActivity} icon={<HighlightOff />} />
                  </Col>
                </Row>
                <Row center="sm" style={{ margin: '5vh 0 0 0' }}>
                  <Col sm={6} >
                    <form onSubmit={this.handleSubmit}>
                      <ActivityForm
                        onInputChange={this.handleInputChange}
                        formErrors={this.state.formErrors}
                        selectedActivity={this.state.activity.type}
                        onDropDownChange={this.handleDropDownChange}
                        onFileLoad={this.onFileLoad}
                      />
                    </form>
                  </Col>
                  <Col sm={6}>
                    <ImagePreview
                      uriList={this.state.pictures}
                      onChange={this.onChange}
                      remove={this.handleRemove}
                      removeAll={this.handleRemoveAll}
                    />
                  </Col>
                  <Col sm={12} style={{ marginTop: '30px' }}>
                    <Divider />
                  </Col>
                </Row>
              </div>
              :
              <Row end="sm" style={{ margin: '4vh 0 0 0' }}>
                <Col sm={12}>
                  <RaisedButton label="Add Activity" primary onClick={this.openAddActivity} icon={<ContentAddCircle />} />
                </Col>
              </Row>
            }

            <Row style={{ margin: '4vh 0 3vh 0' }}>
              <Col sm={12} style={{ marginBottom: '4vh' }}>
                <ProfitChart
                  activities={this.state.activities}
                  year={this.state.currentYearOnNavigation}
                  redraw
                />
              </Col>
              <Col sm={12}>
                <ActivityCalendar
                  events={this.state.activities}
                  onSelectEvent={this.navigateToActivityProfile}
                  onNavigate={this.onNavigate}
                />
              </Col>
            </Row>
          </Grid>
        </MediaQuery>

        {/* For mobile phones and other devices which has a with of less than 767px */}
        <MediaQuery maxWidth={767}>
          <Grid fluid style={{ margin: 0, padding: 0, overflowY: 'auto' }}>
            <Tabs
              value={this.state.slideIndex}
              onChange={this.handleChangeTab}
              tabItemContainerStyle={{ height: 45, marginTop: -3, backgroundColor: '#43A047' }}
            >
              <Tab label={<AddActivity color={this.state.slideIndex === 0 ? 'white' : '#A5D6A7'} />} value={0} />
              <Tab label={<Statistics color={this.state.slideIndex === 1 ? 'white' : '#A5D6A7'} />} value={1} />
              <Tab label={<Calendar color={this.state.slideIndex === 2 ? 'white' : '#A5D6A7'} />} value={2} />
            </Tabs>

            <SwipeableViews
              index={this.state.slideIndex}
              onChangeIndex={this.handleChangeTab}
            >
              <div style={{ height: '100%', overflowX: 'hidden', overflowY: 'auto' }}>
                <Row center="xs">
                  <Col xs={12} >
                    <form onSubmit={this.handleSubmit}>
                      <ActivityForm
                        onInputChange={this.handleInputChange}
                        formErrors={this.state.formErrors}
                        selectedActivity={this.state.activity.type}
                        onDropDownChange={this.handleDropDownChange}
                        onFileLoad={this.onFileLoad}
                      />
                    </form>
                  </Col>
                </Row>
                <Row>
                  <Col xs={12}>
                    <Divider />
                  </Col>
                </Row>
                <Row center="xs"style={{ marginTop: '0.5vh' }}>
                  <Col xs={12}>
                    <ImagePreview
                      uriList={this.state.pictures}
                      onChange={this.onChange}
                      remove={this.handleRemove}
                      removeAll={this.handleRemoveAll}
                    />
                  </Col>
                </Row>
              </div>

              <div style={{ overflowX: 'hidden', overflowY: 'auto' }} >
                <MediaQuery orientation="portrait">
                  <div style={{ height: '100vh', display: 'grid', backgroundColor: '#F5F5F5' }}>
                    <div style={{ margin: 'auto' }}>
                      <div className="rotate-to-landscape" style={{ margin: 'auto' }} />
                      <div style={{ marginTop: '4vh', fontFamilty: 'Roboto' }}>Rotate your device in landscape</div>
                    </div>
                  </div>
                </MediaQuery>
                <MediaQuery orientation="landscape">
                  <Row center="xs">
                    <Col xs={12}>
                      <ProfitChart
                        activities={this.state.activities}
                        year={this.state.currentYearOnNavigation}
                        redraw
                      />
                    </Col>
                  </Row>
                </MediaQuery>
              </div>

              <div style={{ overflowX: 'hidden', overflowY: 'auto' }} >
                <MediaQuery orientation="portrait">
                  <div style={{ height: '100vh', display: 'grid', backgroundColor: '#F5F5F5' }}>
                    <div style={{ margin: 'auto' }}>
                      <div className="rotate-to-landscape" style={{ margin: 'auto' }} />
                      <div style={{ marginTop: '4vh', fontFamilty: 'Roboto' }}>Rotate your device in landscape</div>
                    </div>
                  </div>
                </MediaQuery>
                <MediaQuery orientation="landscape">
                  <Row center="xs">
                    <Col xs={12}>
                      <ActivityCalendar
                        events={this.state.activities}
                        onSelectEvent={this.navigateToActivityProfile}
                        onNavigate={this.onNavigate}
                      />
                    </Col>
                  </Row>
                </MediaQuery>
              </div>
            </SwipeableViews>
          </Grid>
        </MediaQuery>
        {/* </MediaQuery> */}
      </div>
    );
  }
}

ActivityFormContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,
};

export default ActivityFormContainer;
