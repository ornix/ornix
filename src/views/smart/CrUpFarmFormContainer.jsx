/* global google */

import React from 'react';
import canUseDOM from 'can-use-dom';
import { TextField } from 'material-ui';
import PropTypes from 'prop-types';

import CrUpFarmForm from '../dumb/crUpFarmForm';
import Modal from '../dumb/doneCancelModal';
import Farm from '../../models/Farm';
import client from '../client';
import CurrentUser from '../user';

const geolocation = (
  canUseDOM && navigator.geolocation ?
    navigator.geolocation :
    ({
      getCurrentPosition(success, failure) {
        failure("Your browser doesn't support geolocation.");
      },
    })
);

class CrUpFarmFormContainer extends React.Component {
  constructor(props) {
    super(props);

    this.willCreate = false;
    this.farmService = client.service('api/farms');
    this.currentAction = props.match.params.action;
    this.currentUrl = props.location.pathname;
    this.overlays = [];
    this.state = {
      farm: {
        center: {
          lat: 10.7202,
          lng: 122.5621,
        },
      },
      boundary: [],
      modal: {
        open: false,
        disabled: false,
        dialogTitle: 'Add some details to your farm',
      },
      revealNotification: false,
      markers: [],
    };
    this.isUnmounted = false;
    if (this.currentAction === 'create') {
      this.willCreate = true;
    } else if (this.currentAction === 'update') {
      this.willCreate = false;
      this.currentFarmId = this.props.match.params.id;
    } else {
      props.history.push('/page-not-found');
    }

    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.handleCancelAction = this.handleCancelAction.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleMapLoad = this.handleMapLoad.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);
    this.handlePlotFarm = this.handlePlotFarm.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleOverlayComplete = this.handleOverlayComplete.bind(this);
    this.handleSearchBoxMounted = this.handleSearchBoxMounted.bind(this);
    this.handleBoundsChanged = this.handleBoundsChanged.bind(this);
    this.handlePlacesChanged = this.handlePlacesChanged.bind(this);
  }

  async componentWillMount() {
    await client.authenticate({
      strategy: 'jwt',
      accessToken: window.localStorage['feathers-jwt'],
    });
    await this.getFarmOwner();
    await this.getFarmData();
  }

  componentDidMount() {
    geolocation.getCurrentPosition((position) => {
      if (this.isUnmounted) {
        return;
      }
      if (this.currentAction === 'create') {
        this.setState({
          farm: {
            center: {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            },
          },
        });
      }
    },
     (reason) => {
       if (this.isUnmounted) {
         return;
       }
       this.setState({
         farm: {
           center: {
             lat: 10.7202,
             lng: 122.5621,
           },
         },
       });
       this.setState({
         reason,
       });
     });
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  async getFarmOwner() {
    const user = await CurrentUser();
    this.setState({ owner: user });
  }


  async getFarmData() {
    if (!this.currentFarmId) {
      return null;
    }
    const currentFarm = await this.farmService.get(this.currentFarmId);
    this.setState({ farm: currentFarm, boundary: currentFarm.boundary });
    return currentFarm;
  }

  async update(farm) {
    try {
      await this.farmService.patch(this.currentFarmId, farm);
      this.props.history.push({
        pathname: '/farmList',
        search: '?isUpdated',
      });
    } catch (error) {
      this.props.history.push({
        pathname: '/farmList',
        search: '?isUpdated=false',
      });
      throw new Error(error);
    }
  }

  async create(farmObject) {
    try {
      await this.farmService.create(farmObject);
      this.props.history.push({
        pathname: '/farmList',
        search: '?isCreated',
      });
    } catch (error) {
      this.props.history.push({
        pathname: '/farmList',
        search: '?isCreated=false',
      });
      throw new Error(error);
    }
  }

  handleRequestClose() {
    this.setState({
      revealNotification: false,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const farmStateData = this.state.farm;
    farmStateData.boundary = this.state.boundary;
    if (!this.state.farm.description || !this.state.farm.name) {
      this.setState({
        revealNotification: true,
        message: 'Please fill up the form first',
      });
    } else if (this.currentFarmId === undefined) {
      const farm = new Farm(farmStateData);
      this.setState({ farm: farmStateData });
      this.create(farm);
    } else {
      const farm = new Farm(farmStateData);
      delete farm.ownerId;
      this.update(farm);
    }
  }

  handleCancelAction() {
    this.props.history.push('/farmList');
  }

  handleInputChange(event, data) {
    const name = event.target.name;
    const value = data;
    const farmStateData = this.state.farm;

    farmStateData[name] = value;
    farmStateData.ownerId = this.state.owner._id;
    this.setState({ farm: farmStateData });
  }

  handleMapLoad(map) {
    this._mapComponent = map;
  }

  handleMapClick(event) {
    this.setState({
      farm: {
        center: {
          lat: event.latLng.lat(),
          lng: event.latLng.lng(),
        },
      },
      revealNotification: true,
    });
  }

  handleCloseModal() {
    this.setState({
      modal: {
        ...this.state.modal,
        open: false,
      },
    });
  }

  // opens create farm form modal
  handlePlotFarm() {
    const { boundary } = this.state;
    // checks if polygon have atleast 3 sides
    if ((boundary && boundary.length >= 4 && boundary[0] === boundary[boundary.length - 1]) ||
      (this.state.farm.boundary && this.state.farm.boundary.length >= 4)) {
      this.setState({
        modal: {
          ...this.state.modal,
          open: true,
        },
      });
    } else {
      this.setState({
        revealNotification: true,
        message: 'Please set boundary first',
      });
    }
  }

  // sets the boundary of the farm
  handleOverlayComplete(event) {
    // removes previous polygons
    if (this.overlays.length) {
      this.overlays.forEach((overlay) => {
        overlay.setMap(null);
      });
      this.overlays = [];
    }

    const boundary = [];
    const array = event.overlay.getPath().getArray();
    const latLngBounds = new google.maps.LatLngBounds();
    array.forEach((coordinate, index) => {
      boundary.push({ lat: coordinate.lat(), lng: coordinate.lng() });
      if (index < array.length) {
        latLngBounds.extend(coordinate);
      }
    });
    if (boundary[0] !== boundary[boundary.length - 1]) {
      boundary.push(boundary[0]);
    }
    const center = latLngBounds.getCenter();
    const farm = this.state.farm;
    delete farm.ownerId;
    this.setState({
      boundary,
      farm: {
        ...farm,
        center: {
          lat: center.lat(),
          lng: center.lng(),
        },
      },
    });
    this.overlays.push(event.overlay);
  }

  handleSearchBoxMounted(searchbox) {
    this._searchBox = searchbox;
  }

  handleBoundsChanged() {
    this.setState({
      bounds: this._mapComponent.getBounds(),
      farm: {
        ...this.state.farm,
        center: this._mapComponent.getCenter(),
      },
    });
  }

  handlePlacesChanged() {
    const places = this._searchBox.getPlaces();
    const markers = places.map(place => ({
      position: place.geometry.location,
    }));

    const mapCenter = markers.length > 0 ? markers[0].position : this.state.farm.center;
    const farm = this.state.farm;
    delete farm.boundary;
    delete this.state.boundary;
    this.setState({
      farm: {
        ...this.state.farm,
        center: mapCenter,
      },
      markers,
    });
  }

  render() {
    const modalContent = [
      <TextField
        key="name"
        type="text"
        name="name"
        hintText="Farm Name"
        floatingLabelText="Farm Name"
        fullWidth
        onChange={this.handleInputChange}
        value={this.state.farm.name || ''}
      />,
      <TextField
        key="description"
        type="text"
        name="description"
        hintText="Farm Description"
        floatingLabelText="Farm Description"
        fullWidth
        onChange={this.handleInputChange}
        value={this.state.farm.description || ''}
      />,
    ];
    return (
      <div>
        <div>
          {this.state.farm ?
            <CrUpFarmForm
              farm={this.state.farm}
              onInputChange={this.handleInputChange}
              onMapLoad={this.handleMapLoad}
              marker={this.state.farm.center}
              onMapClick={this.handleMapClick}
              revealNotification={this.state.revealNotification}
              message={this.state.message}
              handleRequestClose={this.handleRequestClose}
              handleCancelAction={this.handleCancelAction}
              center={this.state.farm.center}
              onPlotFarm={this.handlePlotFarm}
              onOverlayComplete={this.handleOverlayComplete}
              boundary={this.state.farm.boundary}
              willCreate={this.willCreate}
              onSearchBoxMounted={this.handleSearchBoxMounted}
              bounds={this.state.bounds}
              onPlacesChanged={this.handlePlacesChanged}
            /> : undefined

          }
          <Modal
            key="normalModal"
            open={this.state.modal.open}
            handleClose={this.handleCloseModal}
            dialogContent={modalContent}
            dialogTitle={this.state.modal.dialogTitle}
            handleDone={this.handleSubmit}
            disabled={false}
          />
        </div>
      </div>
    );
  }
}

CrUpFarmFormContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      action: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,
};

export default CrUpFarmFormContainer;
