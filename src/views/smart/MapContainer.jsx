/* global google */

import React from 'react';
import { Snackbar, Dialog, FlatButton } from 'material-ui';
import MediaQuery from 'react-responsive';
import PropTypes from 'prop-types';

import CustomMap from '../dumb/customMap';
import CreateTreeForm from '../dumb/createTreeForm';
import MarkerIconPicker from '../dumb/markerIconPicker';
import MarkerInformation from '../dumb/markerInformation';
import WeatherForecast from '../dumb/weatherForecast';
import MapToolbox from '../dumb/mapToolbox';
import Tree from '../../models/Tree';
import client from '../client';
import WeatherMap from '../../models/WeatherMap';
import WeatherData from '../dumb/weatherDataContainer';
import ConfirmationModal from '../dumb/confirmationModal';
import Spinner from '../dumb/spinner';
import autobind from '../../decorators/autobind';

class GoogleMapContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      markers: [],
      iconUrl: '/images/assets/pinApple.png',
      showSnackbar: false,
      snackbarMessage: '',
      openModal: false,
      center: undefined,
      formErrors: {},
      drawingMode: null,
      selectedIcon: '',
      areas: [],
      area: {},
      tree: {},
      generalData: {
        type: '',
        description: '',
      },
      isQuickPlotEnabled: false,
      stepIndex: 0,
      isAreaValid: true,
      selectedTool: '',
      openConfirmation: false,
      currentMarker: {},
      isFieldHealthVisible: false,
    };

    this.handleMapLoad = this.handleMapLoad.bind(this);
    this.handleMapClick = this.handleMapClick.bind(this);
    this.handleMapRightClick = this.handleMapRightClick.bind(this);
    this.handleMarkerClose = this.handleMarkerClose.bind(this);
    this.handleMarkerClick = this.handleMarkerClick.bind(this);
    this.handleMarkerSelect = this.handleMarkerSelect.bind(this);
    this.handleChangeMarker = this.handleChangeMarker.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleModalInputChange = this.handleModalInputChange.bind(this);
    this.setFarmCenter = this.setFarm.bind(this);
    this.loadMarkers = this.loadMarkers.bind(this);
    this.loadAreas = this.loadAreas.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleToolSelection = this.handleToolSelection.bind(this);
    this.handleDrawingModeAction = this.handleDrawingModeAction.bind(this);
    this.handleOverlayComplete = this.handleOverlayComplete.bind(this);
    this.handleCancelDrawing = this.handleCancelDrawing.bind(this);
    this.quickPlotActionHandler = this.quickPlotActionHandler.bind(this);
    this.handleStepIndex = this.handleStepIndex.bind(this);
    this.handleRemoveMarker = this.handleRemoveMarker.bind(this);
    this.isInsideFarm = this.isInsideFarm.bind(this);
    this.openConfirmation = this.openConfirmation.bind(this);
    this.closeConfirmation = this.closeConfirmation.bind(this);
    this.showFieldHealth = this.showFieldHealth.bind(this);
    this.errorMessage = 'Something went wrong. Please refresh the page and try again.';
  }

  async componentWillMount() {
    this.treeService = await client.callService('trees');
    this.areaService = await client.callService('areas');
    this.farmService = await client.callService('farms');
    this.treeService.on('created', this.addMarker);
    this.treeService.on('removed', this.removeMarker);
    this.treeService.on('patched', this.updateMarker);

    const farmId = this.props.match.params.id;

    /**
     * Try catch block that checks if the given
     * farmID exists in the database. Halts remaining
     * operations if the farmID does not exist.
     */
    try {
      await this.setFarm(farmId);
    } catch (error) {
      this.setState({
        showSnackbar: true,
        snackbarMessage: this.errorMessage,
      });
      return;
    }

    await this.loadMarkers();
    await this.loadAreas();
    await this.loadWeatherData();
  }

  async setFarm(farmId) {
    try {
      const farm = await this.farmService.get(farmId);
      this.setState({
        currentFarm: farm,
        center: farm.center,
      });
    } catch (error) {
      this.setState({
        showSnackbar: true,
        snackbarMessage: this.errorMessage,
      });
      throw new Error(error.message);
    }
  }

  // Find center of areas
  getAreaCenter(area) {
    let position = {};
    const latLngBounds = new google.maps.LatLngBounds();
    if (area.bounds) {
      const bounds = [];
      bounds.push(new google.maps.LatLng(area.bounds.north, area.bounds.east));
      bounds.push(new google.maps.LatLng(area.bounds.south, area.bounds.east));
      bounds.push(new google.maps.LatLng(area.bounds.south, area.bounds.west));
      bounds.push(new google.maps.LatLng(area.bounds.north, area.bounds.west));

      bounds.forEach((bound) => {
        latLngBounds.extend(bound);
      });
      position = latLngBounds.getCenter();
    } else if (area.path) {
      const paths = [];
      area.path.map((path) => {
        paths.push(new google.maps.LatLng(path.lat, path.lng));
        return paths;
      });
      paths.forEach((path) => {
        latLngBounds.extend(path);
      });
      position = latLngBounds.getCenter();
    } else if (area.center) {
      latLngBounds.extend(new google.maps.LatLng(area.center.lat, area.center.lng));
      position = area.center;
    }

    this.setState({});
    return position;
  }

  @autobind
  addMarker(treeData) {
    const treeModel = new Tree(treeData);
    const markerData = treeModel.markerData;

    this.setState({
      markers: this.state.markers.concat([markerData]),
    });
  }

  @autobind
  removeMarker(treeData) {
    const currentFarm = this.state.currentFarm;
    const treeModel = new Tree(treeData);
    const markerData = treeModel.markerData;

    currentFarm.trees = currentFarm.trees.filter(tree => tree._id !== markerData._id);
    currentFarm.treeIds = currentFarm.treeIds.filter(treeId => treeId !== markerData._id);

    this.setState({
      currentFarm,
    });

    this.loadMarkers();
  }

  @autobind
  updateMarker(treeData) {
    this.removeMarker(treeData);
    this.addMarker(treeData);
  }

  loadMarkers() {
    const nextMarkers = [];
    try {
      /**
       * Tree data is accessed on the 'trees' object
       * array made possible by hook population.
       *
       * No need to query each marker separately.
       */
      const trees = this.state.currentFarm.trees;
      trees.forEach((treeData) => {
        const treeModel = new Tree(treeData);
        const markerData = treeModel.markerData;
        nextMarkers.push(markerData);
      });
      this.setState({
        markers: nextMarkers,
      });
    } catch (error) {
      this.setState({
        showSnackbar: true,
        snackbarMessage: this.errorMessage,
      });
    }
  }

  loadAreas() {
    /**
       * Area data is accessed on the 'areas' object
       * array made possible by hook population.
       *
       * No need to query each area separately.
       */
    try {
      const areas = this.state.currentFarm.areas;
      areas.forEach((area) => {
        if (area.activities) {
          // eslint-disable-next-line no-param-reassign
          area.lastActivity = area.activities[area.activities.length - 1];
        }
      });
      this.setState({
        areas,
      });
    } catch (error) {
      this.setState({
        showSnackbar: true,
        snackbarMessage: this.errorMessage,
      });
    }
  }

  handleMapLoad(map) {
    this.mapComponent = map;
    this.setState({
      showSnackbar: true,
      snackbarMessage: 'Loading map...',
    });
  }

  handleMapClick(event) {
    const coordinates = { lat: event.latLng.lat(), lng: event.latLng.lng() };
    if (!this.state.drawingMode && this.isInsideFarm(coordinates)) {
      const farmId = this.state.currentFarm._id;
      this.setState({
        tree: {
          coordinates,
          createdAt: new Date(),
          updatedAt: new Date(),
          icon: this.state.iconUrl,
          farmId,
          type: this.state.generalData.type,
          description: this.state.generalData.description,
        },
      });

      if (!this.state.generalData.type
        && !this.state.generalData.description) {
        this.handleOpenModal();
      }
      if (this.state.isQuickPlotEnabled) {
        this.handleSubmit();
      } else {
        this.handleOpenModal();
      }
    }
  }

  handleMapRightClick(targetMarker) {
    const nextMarkers = this.state.markers.filter(marker => marker !== targetMarker);
    this.setState({
      nextMarkers,
    });
  }

  handleMarkerClose(target) {
    this.setState({
      markers: this.state.markers.map((marker) => {
        if (marker === target) {
          return {
            ...marker,
            showInfo: false,
          };
        }
        return marker;
      }),
      areas: this.state.areas.map((area) => {
        if (area === target) {
          return {
            ...area,
            showInfo: false,
          };
        }
        return area;
      }),
    });
  }

  handleMarkerClick(target) {
    this.setState({
      markers: this.state.markers.map((marker) => {
        if (marker === target) {
          return {
            ...target,
            showInfo: true,
            selected: true,
          };
        }
        marker.selected = false; // eslint-disable-line no-param-reassign
        marker.showInfo = false; // eslint-disable-line no-param-reassign
        return marker;
      }),
      areas: this.state.areas.map((area) => {
        if (area === target) {
          return {
            ...target,
            showInfo: true,
            selected: true,
          };
        }
        area.selected = false;  // eslint-disable-line no-param-reassign
        area.showInfo = false;  // eslint-disable-line no-param-reassign
        return area;
      }),
    });
  }

  handleMarkerSelect(selectedMarker) {
    this.setState({
      markers: this.state.markers.map((marker) => {
        if (marker === selectedMarker) {
          this.setState({
            center: marker.position,
          });
          return {
            ...marker,
            showInfo: true,
            selected: true,
          };
        }
        marker.selected = false; // eslint-disable-line no-param-reassign
        marker.showInfo = false; // eslint-disable-line no-param-reassign
        return marker;
      }),
      areas: this.state.areas.map((area) => {
        if (area === selectedMarker) {
          const areaCenter = this.getAreaCenter(area);
          const position = area.shape !== 'circle' ? { lat: areaCenter.lat(), lng: areaCenter.lng() } : area.center;
          // eslint-disable-next-line no-param-reassign
          area.position = area.position ? area.position : position;
          this.setState({
            center: area.position,
          });
          return {
            ...area,
            showInfo: true,
            selected: true,
          };
        }
        area.selected = false; // eslint-disable-line no-param-reassign
        area.showInfo = false; // eslint-disable-line no-param-reassign
        return area;
      }),
    });
  }

  async create(data) {
    const currentFarm = this.state.currentFarm;
    Object.assign(data, { farmId: currentFarm._id.toString() });

    try {
      if (this.state.drawingMode) {
        const area = await this.areaService.create(data);
        currentFarm.areaIds.push(area._id);
        currentFarm.areas.push(area);
      } else {
        const tree = await this.treeService.create(data);
        currentFarm.treeIds.push(tree._id);
        currentFarm.trees.push(tree);
      }
      const { areaIds, treeIds } = currentFarm;
      const newFarm = await this.farmService.patch(currentFarm._id, { areaIds, treeIds });
      this.setState({
        currentFarm: newFarm,
        showSnackbar: true,
        snackbarMessage: '✔ Marker Added!',
      });
      this.loadAreas();
      this.loadMarkers();
    } catch (error) {
      this.setState({
        showSnackbar: true,
        snackbarMessage: this.errorMessage,
      });
    }
  }

  handleChangeMarker(assetPath) {
    const iconUrl = `/images/assets/${assetPath}`;
    this.setState({
      iconUrl,
      selectedIcon: assetPath,
    });
  }

  handleToolSelection(assetPath) {
    if (this.state.selectedTool === assetPath) {
      this.setState({ selectedTool: '' });
    } else {
      this.setState({ selectedTool: assetPath });
    }

    if (assetPath === 'fieldHealth.svg') {
      this.showFieldHealth();
    }
  }

  handleRequestClose() {
    this.setState({
      showSnackbar: false,
    });
  }

  handleOpenModal() {
    this.setState({
      openModal: true,
      formErrors: {},
    });
  }

  handleCloseModal() {
    this.setState({
      openModal: false,
    });
  }

  async handleSubmit() {
    if (this.state.drawingMode) {
      const area = this.state.area;
      area.type = this.state.generalData.type;
      area.description = this.state.generalData.description;
      if (this.state.isAreaValid) {
        await this.create(area);
        await this.loadAreas();
      } else {
        this.setState({
          showSnackbar: true,
          snackbarMessage: 'The area or part of the area is outside the farm',
        });
      }
      this.handleCloseModal();
    }
    if (!this.state.drawingMode) {
      const tree = this.state.tree;
      tree.type = this.state.generalData.type;
      tree.description = this.state.generalData.description;
      this.create(tree);
      this.loadMarkers();
      this.handleCloseModal();
    }
    this.showErrors();
  }

  /**
   * Weather Data rendering
   *
   * Load weather data from Open Weather Map API
   */
  async loadWeatherData() {
    const weatherMap = new WeatherMap(this.state.center.lng, this.state.center.lat);

    const currentWeather = await weatherMap.getCurrentWeather();
    const forecastWeather = await weatherMap.getWeatherForecast();

    this.setState({
      currentWeatherMapData: currentWeather,
      forecastWeatherMapData: forecastWeather,
    });
  }

  handleStepIndex(index) {
    this.setState({
      stepIndex: index,
    });
  }

  showErrors() {
    const formErrors = {};
    if (!this.state.drawingMode) {
      if (!this.state.generalData.type) {
        formErrors.type = 'Tree type is required';
      }

      if (!this.state.generalData.description) {
        formErrors.description = 'Tree description is required';
      }
    }

    this.setState({
      formErrors,
    });
  }

  // Store modal data to state
  handleModalInputChange(event, data) {
    const name = event.target.name;
    const value = data;
    const generalData = this.state.generalData;

    generalData[name] = value;
    this.setState({
      generalData,
    });
  }

  handleDrawingModeAction(event, value) {
    let mode;
    if (!value) {
      mode = null;
    } else {
      mode = value;
    }
    this.setState({ drawingMode: mode });
  }

  handlePolygonComplete(overlay) {
    const area = this.state.area;
    const path = [];
    let isValid = true;
    overlay.getPath().getArray().forEach((coordinate) => {
      path.push({ lat: coordinate.lat(), lng: coordinate.lng() });
    });

    path.map((coordinate) => {
      isValid = this.isInsideFarm(coordinate) && isValid;
      return coordinate;
    });

    if (isValid) {
      this.setState({
        area: {
          ...area,
          path,
        },
        isAreaValid: isValid,
      });
    } else {
      this.setState({ isAreaValid: isValid });
    }
  }

  handleCircleComplete(overlay) {
    const area = this.state.area;
    const center = overlay.getCenter();
    const radius = overlay.getRadius();

    this.setState({
      area: {
        ...area,
        center: {
          lat: center.lat(),
          lng: center.lng(),
        },
        radius,
      },
    });
  }

  handleRectangleComplete(overlay) {
    const area = this.state.area;
    const bounds = {
      north: overlay.getBounds().getNorthEast().lat(),
      east: overlay.getBounds().getNorthEast().lng(),
      south: overlay.getBounds().getSouthWest().lat(),
      west: overlay.getBounds().getSouthWest().lng(),
    };

    const isValid = this.isInsideFarm({ lat: bounds.north, lng: bounds.east }) &&
      this.isInsideFarm({ lat: bounds.south, lng: bounds.east }) &&
      this.isInsideFarm({ lat: bounds.south, lng: bounds.west }) &&
      this.isInsideFarm({ lat: bounds.north, lng: bounds.west });
    if (isValid) {
      this.setState({
        area: {
          ...area,
          bounds,
        },
        isAreaValid: isValid,
      });
    } else {
      this.setState({ isAreaValid: isValid });
    }
  }

  handleOverlayComplete(event) {
    const area = this.state.area;
    const farmId = this.state.currentFarm._id;

    this.setState({
      area: {
        ...area,
        shape: event.type,
        createdAt: new Date(),
        updatedAt: new Date(),
        farmId,
        type: this.state.generalData.type,
        description: this.state.generalData.description,
      },
    });

    switch (event.type) {
      case 'circle':
        this.handleCircleComplete(event.overlay);
        break;
      case 'rectangle':
        this.handleRectangleComplete(event.overlay);
        break;
      case 'polygon':
        this.handlePolygonComplete(event.overlay);
        break;
      default:
        break;
    }

    event.overlay.setMap(null);

    this.handleOpenModal();
  }

  navigateToActivities(id, type) {
    this.props.history.push(`/activity/${type}/${id}`);
  }

  handleCancelDrawing() {
    this.setState({ drawingMode: null });
    this.setState({ drawingMode: true });
  }

  quickPlotActionHandler(event, isInputChecked) {
    this.setState({
      isQuickPlotEnabled: isInputChecked,
    });

    if (isInputChecked === true) {
      document.getElementById('quickPlotForm').style.display = 'initial';
    } else {
      document.getElementById('quickPlotForm').style.display = 'none';
    }
  }

  async handleRemoveMarker() {
    const currentFarm = this.state.currentFarm;
    const marker = this.state.currentMarker;

    if (marker.shape) {
      await this.areaService.remove(marker._id);
      currentFarm.areas = currentFarm.areas.filter(area => area._id !== marker._id);
      currentFarm.areaIds = currentFarm.areaIds.filter(areaId => areaId !== marker._id);
      this.setState({ currentFarm });
      this.loadAreas();
      this.closeConfirmation();
    } else {
      await this.treeService.remove(marker._id);
      currentFarm.trees = currentFarm.trees.filter(tree => tree._id !== marker._id);
      currentFarm.treeIds = currentFarm.treeIds.filter(treeId => treeId !== marker._id);
      this.setState({ currentFarm });
      this.loadMarkers();
      this.closeConfirmation();
    }
  }

  isInsideFarm(location) {
    if (this.state.currentFarm.boundary) {
      const paths = this.state.currentFarm.boundary.map(point =>
        new google.maps.LatLng(point.lat, point.lng));
      const farm = new google.maps.Polygon({ paths });
      return google.maps.geometry.poly.containsLocation(
        new google.maps.LatLng(location.lat, location.lng),
        farm,
      );
    }
    return false;
  }

  closeConfirmation() {
    this.setState({
      openConfirmation: false,
    });

    this.loadMarkers();
    this.loadAreas();
  }

  openConfirmation(marker) {
    this.setState({
      openConfirmation: true,
      currentMarker: marker,
    });
  }

  showFieldHealth() {
    const visible = this.state.isFieldHealthVisible;
    const map = this.mapComponent;
      /**
       * This is the only way I can modify the encapsulated
       * map instance used by the google-maps-react component
       * so that I can add custom overlay maps.
       *
       * const map = new google.maps.Map(node)
      */
    const mapX = map.context.__SECRET_MAP_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;

    if (!visible) {
      // Define custom WMS tiled layer
      const SHLayer = new google.maps.ImageMapType({
        getTileUrl(coord, zoom) {
          const proj = map.getProjection();
          const zfactor = 2 ** zoom; // same as Math.pow(2, zoom);
          // get Long Lat coordinates
          const top = proj.fromPointToLatLng(
          new google.maps.Point((coord.x * 512) / zfactor, (coord.y * 512) / zfactor));
          const bot = proj.fromPointToLatLng(
          new google.maps.Point(((coord.x + 1) * 512) / zfactor, ((coord.y + 1) * 512) / zfactor));

          // create the Bounding box string
          const bbox = `${top.lng()},${
          bot.lat()},${
          bot.lng()},${
          top.lat()}`;

          // base WMS URL
          let url = 'https://services.sentinel-hub.com/ogc/wms/06560e15-b06c-47da-825b-924cc483e5d5';
          url += '?REQUEST=GetMap';   // WMS operation
          url += '&SERVICE=WMS';      // WMS service
          url += '&VERSION=1.1.1';    // WMS version
          url += '&LAYERS=NDVI';      // WMS layers
          url += '&FORMAT=image/jpg'; // WMS format
          url += '&SRS=EPSG:4326';    // set WGS84
          url += `&BBOX=${bbox}`;     // set bounding box
          url += '&WIDTH=512';        // tile size in google
          url += '&HEIGHT=512';

          return url;                 // return URL for the tile
        },

        tileSize: new google.maps.Size(512, 512),
      });

      mapX.overlayMapTypes.push(SHLayer);
    } else {
      mapX.overlayMapTypes.clear();
    }
    this.setState({
      isFieldHealthVisible: !visible,
    });
  }

  render() {
    const { stepIndex } = this.state;
    const assets = [
      'marker_red.png',
      'marker_pink.png',
      'marker_orange.png',
      'marker_yellow.png',
      'marker_lime.png',
      'marker_light_blue.png',
      'marker_purple.png',
      'marker_teal.png',
      'marker_indigo.png',
      'pinApple.png',
      'pinBanana.png',
      'pinChico.png',
      'pinCoconut.png',
      'pinCustardApple.png',
      'pinJackfruit.png',
      'pinLanzones.png',
      'pinMango.png',
      'pinPapaya.png',
      'pinRambutan.png',
    ];

    const toolAssets = [
      'mapMarker.svg',
      'activityList.svg',
      'weather.svg',
      'fieldHealth.svg',
    ];

    const actions = [
      <FlatButton
        key="cancel"
        label="Cancel"
        primary
        onClick={this.handleCloseModal}
      />,
      <FlatButton
        key="submit"
        label="Submit"
        primary
        keyboardFocused
        onClick={this.handleSubmit}
      />,
    ];

    let weatherData;

    if (this.state.currentWeatherMapData) {
      weatherData = (<WeatherData
        currentWeatherMapData={this.state.currentWeatherMapData}
        forecastWeatherMapData={this.state.forecastWeatherMapData}
        stepIndex={(index) => {
          this.handleStepIndex(index);
        }}
        renderStepActions={(step) => {
          this.renderStepActions(step);
        }}
        currentStep={stepIndex}
      />);
    } else {
      weatherData = undefined;
    }

    const toolIconName = this.state.selectedTool.substring(0, this.state.selectedTool.indexOf('.'));

    return (
      <div>
        {/* For tablets, laptops, desktops, large minitors */}
        <MediaQuery minWidth={768}>
          <div style={{ height: '100%' }}>
            {this.state.currentFarm ?
              <CustomMap
                containerElement={
                  <div style={{
                    position: 'absolute',
                    height: window.innerHeight - 64,
                    top: 64,
                    left: 0,
                    right: 0,
                    bottom: 0,
                  }}
                  />
                }
                mapElement={
                  <div style={{
                    height: '100%',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                  }}
                  />
                }
                onMapLoad={this.handleMapLoad}
                onMapClick={this.handleMapClick}
                markers={this.state.markers}
                center={this.state.center}
                onMarkerClose={this.handleMarkerClose}
                onMarkerClick={this.handleMarkerClick}
                drawingMode={this.state.drawingMode}
                areas={this.state.areas}
                onOverlayComplete={this.handleOverlayComplete}
                onMapRightClick={this.handleCancelDrawing}
                removeMarker={this.openConfirmation}
                boundary={this.state.currentFarm.boundary}
              /> : <Spinner />
            }

            <WeatherForecast
              weatherData={weatherData}
              selectedTool={toolIconName}
            />

            <MarkerInformation
              markers={this.state.markers}
              areas={this.state.areas}
              onIconClick={this.handleMarkerSelect}
              activityButton={(markerId, type) => { this.navigateToActivities(markerId, type); }}
              selectedTool={toolIconName}
            />
            <MarkerIconPicker
              assets={assets}
              onIconClick={this.handleChangeMarker}
              onInputChange={this.handleModalInputChange}
              formErrors={this.state.formErrors}
              values={this.state.generalData}
              handleDrawingModeAction={this.handleDrawingModeAction}
              handleQuickPlotAction={this.quickPlotActionHandler}
              selectedIcon={this.state.selectedIcon}
              selectedTool={toolIconName}
            />

            <MapToolbox
              assets={toolAssets}
              onIconClick={this.handleToolSelection}
              selectedTool={this.state.selectedTool}
            />

            <Snackbar
              open={this.state.showSnackbar}
              message={this.state.snackbarMessage}
              autoHideDuration={2000}
              onRequestClose={this.handleRequestClose}
            />
            <Dialog
              paperClassName="circular"
              open={this.state.openModal}
              actions={actions}
              modal={false}
              onRequestClose={this.handleCloseModal}
            >
              <CreateTreeForm
                onInputChange={this.handleModalInputChange}
                formErrors={this.state.formErrors}
                values={this.state.generalData}
              />
            </Dialog>

            <ConfirmationModal
              key="removeMarkerModal"
              open={this.state.openConfirmation}
              handleClose={this.closeConfirmation}
              dialogContent={[]}
              dialogTitle={'Are you sure to remove this marker?'}
              handleDone={this.handleRemoveMarker}
            />

          </div>
        </MediaQuery>

        {/* For mobile phones and other devices which has a with of less than 767px */}
        <MediaQuery maxWidth={767}>
          <MediaQuery orientation="portrait">
            <div style={{ height: '100vh', display: 'grid', backgroundColor: '#F5F5F5' }}>
              <div style={{ margin: 'auto' }}>
                <div className="rotate-to-landscape" style={{ margin: 'auto' }} />
                <div style={{ marginTop: '4vh', fontFamilty: 'Roboto' }}>Rotate your device in landscape</div>
              </div>
            </div>
          </MediaQuery>
          <MediaQuery orientation="landscape">
            <div style={{ height: '100%', overflowX: 'hidden' }}>
              {this.state.currentFarm ?
                <CustomMap
                  containerElement={
                    <div style={{
                      position: 'absolute',
                      height: window.innerHeight,
                      top: 0,
                      left: 0,
                      right: 0,
                      bottom: 0,
                    }}
                    />
                  }
                  mapElement={
                    <div style={{
                      height: '100%',
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      right: 0,
                      bottom: 0,
                    }}
                    />
                  }
                  onMapLoad={this.handleMapLoad}
                  onMapClick={this.handleMapClick}
                  markers={this.state.markers}
                  center={this.state.center}
                  onMarkerClose={this.handleMarkerClose}
                  onMarkerClick={this.handleMarkerClick}
                  drawingMode={this.state.drawingMode}
                  areas={this.state.areas}
                  onOverlayComplete={this.handleOverlayComplete}
                  onMapRightClick={this.handleCancelDrawing}
                  removeMarker={this.openConfirmation}
                  boundary={this.state.currentFarm.boundary}
                /> : <Spinner />
              }

              <WeatherForecast
                weatherData={weatherData}
                selectedTool={toolIconName}
              />

              <MarkerInformation
                markers={this.state.markers}
                areas={this.state.areas}
                onIconClick={this.handleMarkerSelect}
                activityButton={(markerId, type) => { this.navigateToActivities(markerId, type); }}
                selectedTool={toolIconName}
              />
              <MarkerIconPicker
                assets={assets}
                onIconClick={this.handleChangeMarker}
                onInputChange={this.handleModalInputChange}
                formErrors={this.state.formErrors}
                values={this.state.generalData}
                handleDrawingModeAction={this.handleDrawingModeAction}
                handleQuickPlotAction={this.quickPlotActionHandler}
                selectedIcon={this.state.selectedIcon}
                selectedTool={toolIconName}
              />

              <MapToolbox
                assets={toolAssets}
                onIconClick={this.handleToolSelection}
                selectedTool={this.state.selectedTool}
              />

              <Snackbar
                open={this.state.showSnackbar}
                message={this.state.snackbarMessage}
                autoHideDuration={2000}
                onRequestClose={this.handleRequestClose}
              />
              <Dialog
                paperClassName="circular"
                open={this.state.openModal}
                actions={actions}
                modal={false}
                onRequestClose={this.handleCloseModal}
              >
                <CreateTreeForm
                  onInputChange={this.handleModalInputChange}
                  formErrors={this.state.formErrors}
                  values={this.state.generalData}
                />
              </Dialog>
              <ConfirmationModal
                key="removeMarkerModal"
                open={this.state.openConfirmation}
                handleClose={this.closeConfirmation}
                dialogContent={[]}
                dialogTitle={'Are you sure to remove this marker?'}
                handleDone={this.handleRemoveMarker}
              />
            </div>
          </MediaQuery>
        </MediaQuery>

      </div>
    );
  }
}

GoogleMapContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,
};

export default GoogleMapContainer;
