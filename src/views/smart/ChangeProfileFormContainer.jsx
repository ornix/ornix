import React from 'react';
import { Card, CardHeader, CardText } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import { Grid, Row, Col } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';

import ChangeUserDetailsFormContainer from '../smart/ChangeUserDetailsFormContainer';
import ChangePasswordFormContainer from '../smart/ChangePasswordFormContainer';

class ChangeProfileFormContainer extends React.Component {

  render() {
    return (
      <div>
        {/* For tablets, laptops, desktops, large monitors */}
        <MediaQuery minWidth={768}>
          <Grid fluid style={{ top: 50, position: 'relative', zIndex: 2, margin: 0, padding: 0 }}>
            <Row center="sm">
              <Col sm={12} style={{ marginTop: '40px' }}>
                <ChangeUserDetailsFormContainer />
              </Col>
              <Col sm={12} style={{ margin: '6vh 0 3vh 0' }}>
                <ChangePasswordFormContainer />
              </Col>
            </Row>
          </Grid>
        </MediaQuery>

        {/* For mobile phones and other devices which has a with of less than 767px*/}
        <MediaQuery maxWidth={767}>
          <Grid fluid style={{ margin: 0, padding: 0 }}>
            <Card zDepth={0}>
              <CardHeader
                title="Edit Profile"
                actAsExpander
                showExpandableButton
              />
              <CardText expandable style={{ margin: 0, padding: 0 }}>
                <ChangeUserDetailsFormContainer />
              </CardText>
            </Card>
            <Divider />
            <Card zDepth={0}>
              <CardHeader
                title="Change Password"
                actAsExpander
                showExpandableButton
              />
              <CardText expandable style={{ margin: 0, padding: 0 }}>
                <ChangePasswordFormContainer />
              </CardText>
            </Card>
            <Divider />
          </Grid>
        </MediaQuery>
      </div>
    );
  }
}

export default ChangeProfileFormContainer;
