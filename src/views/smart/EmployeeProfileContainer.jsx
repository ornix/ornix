import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';

import EmployeeProfile from '../dumb/employeeProfile';
import client from '../client';

class EmployeeProfileContainer extends React.Component {
  constructor(props) {
    super(props);
    this.employeeId = props.match.params.employeeId;

    this.state = {
      employee: {},
    };
  }

  componentDidMount() {
    this.getEmployee();
  }

  async getEmployee() {
    const usersService = client.service('/api/users');
    const currentEmployee = await usersService.get(this.employeeId);
    this.setState({
      employee: currentEmployee,
    });
  }

  render() {
    return (
      <div>
        {/* For tablets, laptops, desktops, large monitors */}
        <MediaQuery minWidth={768}>
          <Grid fluid style={{ top: 50, padding: 0, position: 'relative', zIndex: 2 }}>
            <Row center="sm">
              <Col sm={12} style={{ marginTop: '40px' }}>
                <EmployeeProfile
                  employee={this.state.employee}
                />
              </Col>
            </Row>
          </Grid>
        </MediaQuery>

        {/* For mobile phones and other devices which has a with of less than 767px*/}
        <MediaQuery maxWidth={767}>
          <Grid fluid style={{ margin: 0, padding: 0 }}>
            <Row center="xs">
              <Col xs={12}>
                <EmployeeProfile
                  employee={this.state.employee}
                />
              </Col>
            </Row>
          </Grid>
        </MediaQuery>
      </div>
    );
  }
}

EmployeeProfileContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      employeeId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default EmployeeProfileContainer;
