import React from 'react';
import LinearProgress from 'material-ui/LinearProgress';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';

import UserRegistrationForm from '../dumb/userRegistrationForm';
import User from '../../models/User';
import client from '../client';
import CurrentUser from '../user';
import autobind from '../../decorators/autobind';

class UserRegistrationFormContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formErrors: {},
      user: {},
      dirtyFields: [],
      isRegistered: false,
      open: false,
      hasLoaded: false,
    };
  }

  async componentWillMount() {
    const currentUser = await CurrentUser();
    if (currentUser) {
      this.props.history.push('/');
    }
    this.setState({
      hasLoaded: true,
    });
  }

  @autobind
  async handleUsernameBlur(event) {
    try {
      await client.service('authManagement').create({
        action: 'checkUnique',
        value: {
          username: event.target.value,
        },
        meta: { noErrMsg: true },
      });
      this.setState({
        usernameError: '',
      });
    } catch (error) {
      this.setState({
        usernameError: 'Username already taken, please choose another one.',
      });
    }

    this.showFormErrors();
  }

  /*
  * Converts the form into a user object and does a
  * validation check before submitting to the service.
  */
  @autobind
  handleSubmit(event) {
    event.preventDefault();
    if (!this.hasFormErrors()) {
      const user = new User(this.state.user);
      this.register(user);
    } else {
      this.showFormErrors();
    }
  }

  /*
  * Handles user state management from registration form fields
  */
  @autobind
  handleInputChange(event, data) {
    if (data && !event) {
      const { user } = this.state;
      const userData = Object.assign(user, { dateOfBirth: data });
      this.setState({
        user: userData,
      });
      this.showFormErrors();
    } else {
      this.removeFieldError(event.target.name);
      const name = event.target.name;
      const value = event.target.value;
      const userStateData = this.state.user;
      userStateData[name] = value;
      this.setState({ user: userStateData });
    }
  }

  /**
   * Handles visual validation cues for the form.
   */
  @autobind
  handleOnBlur(event) {
    const dirtyFields = this.state.dirtyFields;
    dirtyFields.push(event.target.name);
    this.setState({ dirtyFields });
    this.showFormErrors();
  }

  /**
   * Checks if a field has been touched.
   */
  @autobind
  isDirty(field) {
    return this.state.dirtyFields.includes(field);
  }

  /**
   * Removes the error visual cue in the field and is paired with
   * the handleInputChange() method.
   */
  @autobind
  removeFieldError(field) {
    const formErrors = this.state.formErrors;
    delete formErrors[field];
    this.setState({ formErrors });
  }

  @autobind
  hasFormErrors() {
    return Object.keys(this.showFormErrors()).length > 0;
  }

  /*
  * Shows visual cues for form errors.
  */
  @autobind
  showFormErrors() {
    const user = new User(this.state.user);
    const validation = user.validate();
    const errors = validation.error;
    const formErrors = {};

    if (this.state.usernameError) {
      formErrors.username = this.state.usernameError;
    }

    if (errors) {
      errors.details.forEach((error) => {
        if (this.isDirty(error.path)) {
          formErrors[error.path] = error.message;
        }
      });
    }
    this.setState({ formErrors });
    return formErrors;
  }

  /*
  * Calls the user service and creates a new user.
  *
  * If the registered user came from an invite link,
  * the farm from the link will instantly be added
  * to the list of the farms where he is employed.
  */
  @autobind
  async register(user) {
    const usersService = client.service('api/users');
    try {
      const { username, password } = user;

      await usersService.create(user);
      const result = await client.authenticate({
        strategy: 'local',
        username,
        password,
      });

      if (window.localStorage.inviteLink) {
        this.props.history.push(window.localStorage.inviteLink);
      }

      const payload = await client.passport.verifyJWT(result.accessToken);
      const userData = await client.service('/api/users').get(payload.userId);

      client.emit('login', userData);
      this.setState({
        isRegistered: true,
        open: true,
      });
    } catch (error) {
      const formErrors = {};
      formErrors.username = error.message;
      this.setState({ formErrors });
    }
  }

  render() {
    return (
      <div>
        {
        this.state.isRegistered ?
          <Redirect to="/" />
        :
          <div>
            {
              this.state.hasLoaded ?
                <form onSubmit={this.handleSubmit}>
                  <UserRegistrationForm
                    onInputChange={this.handleInputChange}
                    formErrors={this.state.formErrors}
                    onBlur={this.handleOnBlur}
                    onUsernameBlur={this.handleUsernameBlur}
                  />
                </form>
              : <LinearProgress mode="indeterminate" />
            }
          </div>
      }
      </div>
    );
  }
}

UserRegistrationFormContainer.propTypes = {
  history: PropTypes.object.isRequired,
};

export default UserRegistrationFormContainer;
