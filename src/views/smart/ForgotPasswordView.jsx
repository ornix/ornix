import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Joi from 'joi';
import PropTypes from 'prop-types';

import autobind from '../../decorators/autobind';
import ForgotPasswwordForm from '../dumb/forgotPasswordForm';
import ChangePasswordForm from '../dumb/changePasswordForm';
import VerificationForm from '../dumb/verificationForm';
import client from '../client';
import CurrentUser from '../user';
import getErrorMessage from './functions/errorMessages';

const authManagementService = client.service('authManagement');

class ForgetPasswordView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formErrors: {},
      fields: {},
      loading: false,
      stepIndex: 0,
      verified: false,
      errorText: '',
    };

    this.user = CurrentUser();
  }

  getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <div>
            <p>
              Welcome to the password reset facility of Harvestree!
            </p>
            <p>To get started, please enter your username</p>
            <div>
              <TextField
                hintText="Input Username"
                floatingLabelText="Username"
                onChange={this.handleInputChange}
                errorText={this.state.formErrors.username}
                name="username"
              />
            </div>
            <RaisedButton
              label="next"
              primary
              onClick={() => {
                this.handleSendResetCode(false);
              }}
            />
          </div>
        );
      case 1:
        return (
          <div>
            <form onSubmit={this.handleVerificationSubmit}>
              <VerificationForm
                handleCancel={this.handleCancel}
                onInputChange={this.handleInputChange}
                errorText={this.state.formErrors.validationCode}
                user={this.state.user ? this.state.user : {}}
                resendCode={() => {
                  this.handleSendResetCode(true);
                }}
              />
            </form>
          </div>
        );
      case 2:
        return (
          <div>
            <form onSubmit={this.handleChangePasswordSubmit}>
              <ChangePasswordForm
                onInputChange={this.handleInputChange}
                formErrors={this.state.formErrors}
              />
            </form>
          </div>
        );
      case 3:
        return (
          <div>
            <p>
              Congratulations! You have successfully changed your password!
            </p>
            <RaisedButton
              label="Back to Home"
              primary
              onClick={() => {
                window.location.href = window.location.origin;
              }}
            />
          </div>
        );
      default:
        return 'You\'re a long way from home sonny jim!';
    }
  }

  @autobind
  handleCancel() {
    this.props.history.push('/');
  }

  @autobind
  handleInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    const fieldData = this.state.fields;
    fieldData[name] = value;
    this.setState({ fields: fieldData });
    this.showFormErrors();
  }

  @autobind
  hasErrors() {
    const { formErrors } = this.state;
    const errors = Object.keys(formErrors);
    return errors.length > 0;
  }

  /*
  * Shows visual cues for form errors.
  */
  showFormErrors() {
    const { fields } = this.state;
    const passwordValidation = Joi.string().required().min(6).label('Password');
    const passwordSchema = {
      oldPassword: passwordValidation,
      newPassword: passwordValidation,
    };
    const validation = Joi.validate({
      oldPassword: fields.oldPassword,
      newPassword: fields.newPassword,
    }, passwordSchema, { abortEarly: false });

    const errors = validation.error;

    const formErrors = {};
    if (errors) {
      errors.details.forEach((error) => {
        formErrors[error.path] = error.message;
      });
      this.setState({ formErrors });
      return false;
    }

    if (fields.oldPassword !== fields.newPassword) {
      formErrors.oldPassword = 'Passwords do not match';
      formErrors.newPassword = formErrors.oldPassword;
    }

    this.setState({ formErrors });
    return true;
  }

  @autobind
  handleNext() {
    this.setState({
      stepIndex: this.state.stepIndex + 1,
    });
  }

  @autobind
  handleBack() {
    this.setState({
      stepIndex: this.state.stepIndex - 1,
    });
  }

  @autobind
  async handleSendResetCode(isResend) {
    try {
      const user = await authManagementService.create({
        action: 'sendResetPwd',
        value: {
          username: this.state.fields.username,
        },
        notifierOptions: { preferredComm: 'cellphone' },
      });

      this.setState({
        user,
      });

      if (!isResend) {
        this.handleNext();
      }

      this.setState({
        formErrors: {},
      });
    } catch (error) {
      const errorText = getErrorMessage(error.errors.$className);
      this.setState({
        formErrors: {
          username: errorText,
        },
      });
    }
  }

  @autobind
  async handleVerificationSubmit(event) {
    event.preventDefault();
    this.handleNext();
  }

  @autobind
  async handleChangePasswordSubmit(event) {
    event.preventDefault();

    if (!this.hasErrors()) {
      try {
        await authManagementService.create({
          action: 'resetPwdShort',
          value: {
            user: {
              username: this.state.user.username,
            },
            token: this.state.fields.verificationCode,
            password: this.state.fields.newPassword,
          },
        });

        this.handleNext();
      } catch (error) {
        const errorText = getErrorMessage(error.errors.$className);

        this.setState({
          formErrors: {
            validationCode: errorText,
          },
        });
        this.handleBack();
      }
    }
  }

  renderContent() {
    const { stepIndex } = this.state;
    const contentStyle = { margin: '0 16px', overflow: 'hidden' };

    return (
      <div style={contentStyle}>
        <div>{this.getStepContent(stepIndex)}</div>
      </div>
    );
  }

  render() {
    const { loading, stepIndex } = this.state;

    return (
      <ForgotPasswwordForm
        stepIndex={stepIndex}
        loading={loading}
        renderContent={this.renderContent()}
      />
    );
  }
}

ForgetPasswordView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default ForgetPasswordView;
