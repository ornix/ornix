import React from 'react';
import PropTypes from 'prop-types';

import client from '../client';
import Login from '../dumb/login';
import CurrentUser from '../user';
import Spinner from '../dumb/spinner';
import loadImages from './functions/loadImages';

const srcs = [
  '/images/harvestreeLogo3.png',
  '/images/backgrounds/background3.png',
];

class LoginView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: undefined,
      password: undefined,
      errorMessage: '',
      hasLoaded: false,
    };

    this.submitHandler = this.submitHandler.bind(this);
    this.inputHandler = this.inputHandler.bind(this);
    this.verify = this.verify.bind(this);
    this.loadImages = loadImages;
  }

  async componentWillMount() {
    const currentUser = await CurrentUser();

    if (currentUser) {
      this.props.history.push('/');
    }

    this.setState({
      hasLoaded: true,
    });
  }

  async componentDidMount() {
    this.loadImages(srcs);
  }

  submitHandler(event) {
    event.preventDefault();

    const password = this.state.password;
    const username = this.state.username;

    this.verify(username, password);
  }

  inputHandler(event, data) {
    const state = {};

    state[event.target.name] = data;
    this.setState(state);
  }

  async verify(user, pass) {
    try {
      const result = await client.authenticate({
        strategy: 'local',
        username: user,
        password: pass,
      });
      const payload = await client.passport.verifyJWT(result.accessToken);
      const userData = await client.service('/api/users').get(payload.userId);

      client.emit('login', userData);
      this.props.history.push('/farmList');
    } catch (error) {
      this.setState({ errorMessage: error.message });
    }
  }

  get hasLoadedImages() {
    const { status } = this.state;
    if (status) {
      const imgSrcs = Object.keys(status);
      return imgSrcs.every(src => status[src]);
    }
    return false;
  }

  render() {
    if (!this.hasLoadedImages) {
      return <Spinner />;
    }

    return (
      <div>
        {this.state.hasLoaded ?
          <Login
            errorMessage={this.state.errorMessage}
            inputHandler={this.inputHandler}
            submitHandler={this.submitHandler}
          />
      : <Spinner />}
      </div>
    );
  }
}

LoginView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default LoginView;
