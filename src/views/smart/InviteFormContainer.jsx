import React from 'react';
import _ from 'underscore';
import PropTypes from 'prop-types';


import client from '../client';
import CurrentUser from '../user';
import Error from '../dumb/error404';
import RedirectFromInvite from '../dumb/redirectFromInvite';

class InviteFormContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: {},
      farm: {},
    };
    this.redirectToMap = this.redirectToMap.bind(this);
  }

  componentWillMount() {
    (async () => {
      await this.getCurrentUser();
      try {
        const { farmId } = this.props.match.params;
        const service = await client.callService('farms');
        const farm = await service.get(farmId);
        const { employeeIds } = farm;
        const { user } = this.state;
        await service.patch(farm._id, {
          employeeIds: employeeIds.concat(user._id),
        });
        this.setState({ farm });
      } catch (e) {
        this.setState({ farm: undefined });
        if (e.name === 'NotAuthenticated') {
          window.localStorage.inviteLink = this.props.location.pathname;
          this.props.history.push('/register');
        }
      }
    })();
  }

  async getCurrentUser() {
    const wholeUser = await CurrentUser(); // user w/out the ommited fields
    const restrictedFields = ['username', 'createdAt'];
    const user = _(wholeUser).omit(...restrictedFields);
    const { dateOfBirth } = user;
    user.dateOfBirth = new Date(dateOfBirth);
    this.setState({ user });
  }

  redirectToMap() {
    const { farmId } = this.props.match.params;
    this.props.history.push(`/map/${farmId}`);
  }

  render() {
    return (
      <div>
        {
          this.state.farm ?
            <RedirectFromInvite
              redirectToMap={this.redirectToMap}
            />
            :
            <Error />
        }
      </div>
    );
  }
}

InviteFormContainer.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.shape({
    params: PropTypes.string.isRequired,
  }).isRequired,
};

export default InviteFormContainer;
