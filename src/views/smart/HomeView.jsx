import React from 'react';
import { Redirect } from 'react-router-dom';

import Home from '../dumb/home';
import CurrentUser from '../user';
import Spinner from '../dumb/spinner';
import loadImages from './functions/loadImages';

const srcs = [
  '/images/backgrounds/background3.png',
  './images/firstDivisionImage1.png',
  '/images/harvestreeLogo3.png',
];

class HomeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
    this.getCurrentUser = this.getCurrentUser.bind(this);
    this.loadImages = loadImages;
  }

  async componentWillMount() {
    await this.getCurrentUser();
    this.setState({
      isLoading: false,
    });
  }

  componentDidMount() {
    this.loadImages(srcs);
  }

  async getCurrentUser() {
    const user = await CurrentUser();
    this.setState({ user });
  }

  get hasLoadedImages() {
    const { status } = this.state;
    if (status) {
      const imgSrcs = Object.keys(status);
      return imgSrcs.every(src => status[src]);
    }
    return false;
  }

  get renderedComponent() {
    const { user } = this.state;

    if (!this.hasLoadedImages) {
      return <Spinner />;
    }

    try {
      if (user._id) {
        return (
          <Redirect to={{
            pathname: '/farmList',
          }}
          />
        );
      }
      return (
        <Home />
      );
    } catch (e) {
      return (<Home />);
    }
  }

  render() {
    return (this.state.isLoading ? <Spinner /> : this.renderedComponent);
  }
}

export default HomeView;
