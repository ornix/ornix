import React from 'react';
import { AppBar } from 'material-ui';
import MediaQuery from 'react-responsive';

import NavigationDrawer from '../dumb/navigationDrawer';
import CurrentUser from '../user';
import client from '../client';

class HeaderContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openDrawer: false,
    };

    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
    this.getCurrentUser = this.getCurrentUser.bind(this);
    this.logout = this.logout.bind(this);
  }

  async componentWillMount() {
    await this.getCurrentUser();
  }

  componentDidMount() {
    client.on('login', async () => {
      await this.getCurrentUser();
    });
  }

  async getCurrentUser() {
    const user = await CurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
      });
    }
  }

  toggleDrawer() {
    this.setState({
      openDrawer: !this.state.openDrawer,
    });
  }

  closeDrawer() {
    this.setState({
      openDrawer: false,
    });
  }

  async logout() {
    await client.logout();
    this.setState({
      currentUser: null,
    });
  }

  render() {
    let className = '';
    const fullPath = window.location.pathname.substring(1);
    const subPath = fullPath.substring(0, fullPath.indexOf('/'));

    if (fullPath === 'changeProfile' || subPath === 'activityProfile' || subPath === 'employee' || fullPath === 'about') {
      className = 'overlayComponent';
    }

    return (
      <div>

        {/* For tablets, laptops, desktops, large minitors */}
        {this.state.currentUser ?
          <MediaQuery minWidth={768}>
            <AppBar
              title="Harvestree"
              className={className}
              onLeftIconButtonTouchTap={this.toggleDrawer}
              showMenuIconButton={!!this.state.currentUser}
            />
            <NavigationDrawer open={this.state.openDrawer} docked={false} closeDrawer={this.closeDrawer} logout={this.logout} state={this.state} style={{ zIndex: '2' }} />
          </MediaQuery>
          :
          <div />
        }

        {/* For mobile phones and other devices which has a with of less than 1023spx*/}
        {location.pathname.substring(1, 4) === 'map' ?
          '' :
          <MediaQuery maxWidth={767}>
            {this.state.currentUser ?
              <div>
                <AppBar
                  title="Harvestree"
                  titleStyle={{ marginTop: -11, marginLeft: -5, height: 45, fontSize: '1.3em' }}
                  zDepth={0}
                  onLeftIconButtonTouchTap={this.toggleDrawer}
                  showMenuIconButton={!!this.state.currentUser}
                  style={{ height: 45 }}
                  iconStyleLeft={{ marginTop: -3, marginLeft: -25 }}
                />

                <NavigationDrawer open={this.state.openDrawer} docked={false} closeDrawer={this.closeDrawer} logout={this.logout} state={this.state} style={{ zIndex: '2' }} />
              </div>
              :
              <div />
            }
          </MediaQuery>
        }
      </div>
    );
  }
}

export default HeaderContainer;
