import React from 'react';
import Joi from 'joi';
import Snackbar from 'material-ui/Snackbar';

import ChangePasswordForm from '../dumb/changePasswordForm';
import client from '../client';
import CurrentUser from '../user';

class ChangePasswordFormContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formErrors: {},
      password: {},
      updateSuccess: 0, // 0 - neutral, 1 - success, 2 - fail
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.hasErrors = this.hasErrors.bind(this);
    this.user = CurrentUser();
  }

  componentDidMount() {
    this.getCurrentUser();
  }

  async getCurrentUser() {
    const user = await CurrentUser();
    const { dateOfBirth } = user;
    user.dateOfBirth = new Date(dateOfBirth) || new Date();
    this.setState({ user });
  }


  /*
  * Converts the form into a user object and does a
  * validation check before submitting to the service.
  */

  handleSubmit(event) {
    event.preventDefault();
    this.showFormErrors();
    this.updatePassword();
  }

  /*
  * Handles state management and visual validation cues at
  * each user input-interaction.
  */
  handleInputChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    const passwordData = this.state.password;
    passwordData[name] = value;
    this.setState({ password: passwordData });
    this.showFormErrors();
  }

  hasErrors() {
    const { formErrors } = this.state;
    const errors = Object.keys(formErrors);
    return errors.length > 0;
  }

  /*
  * Shows visual cues for form errors.
  */
  showFormErrors() {
    const { password } = this.state;
    const passwordValidation = Joi.string().required().min(6).label('Password');
    const passwordSchema = {
      oldPassword: passwordValidation,
      newPassword: passwordValidation,
    };
    const validation = Joi.validate(password, passwordSchema, { abortEarly: false });
    const errors = validation.error;
    const formErrors = {};
    if (errors) {
      errors.details.forEach((error) => {
        formErrors[error.path] = error.message;
      });
    }
    this.setState({ formErrors });
  }

  async updatePassword() {
    const { user, password } = this.state;
    const { oldPassword, newPassword } = password;
    const { username } = user;

    const authManagement = client.service('authManagement');

    try {
      const response = await authManagement.create({
        action: 'passwordChange',
        value: {
          user: { username },
          oldPassword,
          password: newPassword,
        },
      });

      if (response._id) {
        this.setState({
          updateSuccess: 1,
        });
      } else {
        this.setState({
          updateSuccess: 2,
        });
      }
    } catch (error) {
      this.setState({
        updateSuccess: 2,
      });
    }
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <ChangePasswordForm
            onInputChange={this.handleInputChange}
            formErrors={this.state.formErrors}
          />
        </form>

        <Snackbar
          open={this.state.updateSuccess !== 0}
          message={
            this.state.updateSuccess === 1 ?
            'Password updated successfully!' : 'Update Failed: Current password is incorrect!'
          }
          autoHideDuration={4000}
          onRequestClose={() => {
            this.setState({
              updateSuccess: 0,
            });
          }}
        />
      </div>
    );
  }
}

export default ChangePasswordFormContainer;
