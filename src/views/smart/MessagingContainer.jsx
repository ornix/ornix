import React from 'react';
import PropTypes from 'prop-types';
import { Paper, Divider } from 'material-ui';
import Subheader from 'material-ui/Subheader';
import { Grid, Row, Col } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';

import client from '../client';
import Thread from '../dumb/thread';
import CurrentUser from '../user';
import Spinner from '../dumb/spinner';
import Message from '../../models/Message';
import autobind from '../../decorators/autobind';

class LoginView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      outgoingMessage: '',
      hasLoaded: false,
    };
  }

  componentDidMount() {
    const farmId = this.props.match.params.farmId;

    (async () => {
      this.farmService = await client.callService('farms');
      const farm = await this.farmService.get(farmId);
      const currentUser = await CurrentUser();
      this.messageService = await client.callService('messages');

      const messages = await this.messageService.find({ query: { farmId } });

      this.messageService.on('created', (message) => {
        if (message.farmId === farm._id) {
          this.addMessage(message);
        }
      });

      this.setState({
        messages: messages.data.reverse(),
        farm,
        currentUser,
        hasLoaded: true,
        pageIndex: 0,
        pageLimit: messages.limit,
      });

      this.scrollToBottom();
    })();
  }

  @autobind
  async onSubmit(evt) {
    evt.preventDefault();
    const rawMessage = new Message({
      content: this.state.outgoingMessage,
      userId: this.state.currentUser._id,
      farmId: this.state.farm._id,
    });

    await this.messageService.create(rawMessage);

    this.setState({
      outgoingMessage: '',
    });
  }

  @autobind
  onInputChange(evt) {
    this.setState({
      outgoingMessage: evt.target.value,
    });
  }

  @autobind
  addMessage(message) {
    this.setState({
      messages: this.state.messages.concat([message]),
    });
    this.lastRef.scrollIntoView({ behavior: 'smooth' });
  }

  @autobind
  scrollToBottom() {
    this.lastRef.scrollIntoView({ behavior: 'instant' });
  }

  @autobind
  threadEnd(element) {
    this.lastRef = element;
  }

  @autobind
  async loadMoreMessages() {
    const pageNumber = (this.state.pageIndex += 1) * this.state.pageLimit;
    const messages = await this.messageService.find({
      query: { farmId: this.state.farm.farmId, $skip: pageNumber },

    });

    this.setState({
      messages: messages.data.reverse().concat(this.state.messages),
      pageIndex: messages.skip,
    });
  }

  render() {
    return (
      <div>
        {/* For tablets, laptops, desktops, large monitors */}
        <MediaQuery minWidth={768}>
          {this.state.hasLoaded ?
            <Grid fluid style={{ margin: 0, padding: 0, display: 'grid', height: window.innerHeight - 64 }}>
              <Row center="sm" style={{ marginTop: 'auto', marginBottom: 'auto' }}>

                {/* To be implemented later. User's list of farm chat */}
                <Col xs={2}>
                  <Paper className="circular" style={{ height: '100%' }}>
                    <Row start="sm">
                      <Col xs={12}>
                        <Subheader style={{ fontSize: '1em' }}>Farm Messaging</Subheader>
                      </Col>
                      <Col xs={12}>
                        <Divider />
                      </Col>
                    </Row>
                  </Paper>
                </Col>

                <Col xs={8}>
                  <Paper className="circular">
                    <Row start="sm">
                      <Col xs={12}>
                        <Subheader style={{ fontSize: '1.5em' }}>{this.state.farm.name}</Subheader>
                      </Col>
                      <Col xs={12}>
                        <Divider />
                      </Col>
                      <Col xs={12}>
                        <Thread
                          messages={this.state.messages}
                          onSubmit={this.onSubmit}
                          onInputChange={this.onInputChange}
                          value={this.state.outgoingMessage}
                          currentUser={this.state.currentUser}
                          threadEnd={this.threadEnd}
                          loadMoreMessages={this.loadMoreMessages}
                          itemCount={(this.state.pageIndex) * this.state.pageLimit}
                        />
                      </Col>
                    </Row>
                  </Paper>
                </Col>
              </Row>
            </Grid>
            : <Spinner />
          }
        </MediaQuery>

        {/* For mobile phones and other devices which has a with of less than 767px */}
        <MediaQuery maxWidth={767}>
          {this.state.hasLoaded ?
            <Grid fluid style={{ margin: 0, padding: 0 }}>
              <Row center="xs">
                <Col xs={12}>
                  <Paper style={{ height: window.innerHeight - 45 }}>
                    <Row start="xs">
                      <Col xs={12}>
                        <Subheader style={{ fontSize: '1.5em' }}>{this.state.farm.name}</Subheader>
                      </Col>
                      <Col xs={12}>
                        <Divider />
                      </Col>
                      <Col xs={12}>
                        <Thread
                          messages={this.state.messages}
                          onSubmit={this.onSubmit}
                          onInputChange={this.onInputChange}
                          value={this.state.outgoingMessage}
                          currentUser={this.state.currentUser}
                          threadEnd={this.threadEnd}
                          loadMoreMessages={this.loadMoreMessages}
                        />
                      </Col>
                    </Row>
                  </Paper>
                </Col>
              </Row>
            </Grid>
            : <Spinner />
          }
        </MediaQuery>
      </div>
    );
  }
}

LoginView.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.string.isRequired,
  }).isRequired,
};

export default LoginView;
