import React from 'react';

import VerificationForm from '../dumb/verificationForm';
import autobind from '../../decorators/autobind';
import SuccessfulRegistration from '../dumb/successfulRegistration';
import CurrentUser from '../user';
import client from '../client';
import getErrorMessage from './functions/errorMessages';

class VerificationFormContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      verified: false,
      errorText: '',
    };
  }

  componentDidMount() {
    (async () => {
      const currentUser = await CurrentUser();
      this.authManagementService = await client.service('authManagement');
      this.setState({ currentUser });
    })();
  }

  @autobind
  handleOnInputChange(event) {
    this.setState({
      code: event.target.value,
    });
  }

  @autobind
  async handleSubmit(evt) {
    evt.preventDefault();
    const code = this.state.code;
    const user = this.state.currentUser.username;

    try {
      const verificationResponse = await this.authManagementService.create({
        action: 'verifySignupShort',
        value: {
          user: { username: user },
          token: code,
        },
      });

      if (verificationResponse.isVerified) {
        this.setState({
          verified: true,
        });
      }
    } catch (error) {
      const errorText = getErrorMessage(error.errors.$className);

      this.setState({
        errorText,
        verified: false,
      });
    }
  }

  @autobind
  async resendCode() {
    const user = this.state.currentUser.username;

    try {
      await this.authManagementService.create({
        action: 'resendVerifySignup',
        value: {
          username: user,
        },
        notifierOptions: { preferredComm: 'cellphone' },
      });

      this.setState({
        errorText: '',
      });
    } catch (error) {
      this.setState({
        errorText: error.message,
      });
    }
  }

  // eslint-disable-next-line class-methods-use-this
  handleCancel() {
    client.logout();
  }

  // eslint-disable-next-line class-methods-use-this
  redirectToHome() {
    window.location.href = window.location.origin;
  }

  render() {
    return (
      <div>
        {
        this.state.verified ?
          <div>
            <SuccessfulRegistration
              redirectToHome={this.redirectToHome}
            />
          </div> :
          <div>
            <form onSubmit={this.handleSubmit}>
              <VerificationForm
                handleCancel={this.handleCancel}
                onInputChange={this.handleOnInputChange}
                errorText={this.state.errorText}
                user={this.state.currentUser ? this.state.currentUser : {}}
                resendCode={this.resendCode}
              />
            </form>
          </div>
      }
      </div>
    );
  }
}

export default VerificationFormContainer;
