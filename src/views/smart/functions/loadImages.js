function load(srcs) {
  // array of objects which contains the status of the images
  // like [{ '/images/mypath/1.jpg': 'false' }]
  // false if the image hasn't been cached yet
  const statuses = srcs.map((src) => {
    const obj = {};
    // initial status of the image should be false
    // meaning it hasn't been loaded yet.
    obj[src] = false;
    return obj;
  });
  // for easier access of the status, we make it as an object
  // like { '/images/1.jpg': false, '/images/2.jpg': true }
  // something like that
  const initialStatus = Object.assign({}, ...statuses);
  this.setState({ status: initialStatus });
  srcs.forEach((src) => {
    const img = new Image();
    img.onload = () => {
      const { status } = this.state;
      status[src] = true;
      this.setState({ status });
    };
    img.src = src;
  });
}

export default load;
