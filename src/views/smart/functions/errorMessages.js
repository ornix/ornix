const errors = {
  verifyExpired: 'Verification token has expired, please request for a new one.',
  badParam: 'Invalid verification token, please request for a new one.',
  badParams: 'User not found',
  default: 'Internal Server Error',
};

function getErrorMessage(className) {
  return errors[className];
}

export default getErrorMessage;
