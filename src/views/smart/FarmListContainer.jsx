import React from 'react';
import { Paper, RaisedButton, AutoComplete, Snackbar, Tabs, Tab } from 'material-ui';
import Divider from 'material-ui/Divider';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentAddCircle from 'material-ui/svg-icons/content/add-circle';
import Farm from 'material-ui/svg-icons/maps/place';
import Assignment from 'material-ui/svg-icons/action/work';
import MediaQuery from 'react-responsive';
import { Link } from 'react-router-dom';
import { Grid, Row, Col } from 'react-flexbox-grid';
import SwipeableViews from 'react-swipeable-views';
import PropTypes from 'prop-types';

import FarmItem from '../dumb/farmItem';
import User from '../../models/User';
import CurrentUser from '../user';
import client from '../client';
import Modal from '../dumb/doneCancelModal';
import EmployeeList from '../dumb/employeeList';
import FarmDetails from '../dumb/farmDetails';
import ConfirmationModal from '../dumb/confirmationModal';
import autobind from '../../decorators/autobind';
import Spinner from '../dumb/spinner';

// const mobileStyle = {
//   grid: {
//     position: 'relative',
//     height: '100vh',
//     width: '50vw',
//   },
//   row: {
//     height: '100%',
//     width: '118%',
//     display: 'block',
//     overflowY: 'auto',
//     overflowX: 'hidden',
//   },
// };

class FarmListContainer extends React.Component {
  constructor(props) {
    super(props);

    this.query = props.location.search;

    this.state = {
      open: {
        doneCancelModal: false,
        deleteConfirmationModal: false,
        removeEmployeeModal: false,
      },
      users: [new User({ _id: 123, firstName: '123', middleName: '456', lastName: '789' })],
      newEmployeeIds: [],
      currentUser: {},
      usernames: [],
      employees: [],
      visibleDetails: [],
      value: '',
      snackbarMessage: '',
      showSnackbar: false,
      copied: false,
      checked: false,
      slideIndex: 0,
      page: 0,
      selectedEmployee: {},
      position: {},
    };
  }

  async componentWillMount() {
    this.farmService = await client.callService('farms');
    this.farmService.on('created', this.retrieveFarms);
    this.farmService.on('patched', this.retrieveFarms);
    this.userService = await client.callService('users');
    this.currentUser = await CurrentUser();

    // retains only the letters in a string
    this.openSnackBar();
    await this.retrieveFarms();
  }

  @autobind
  onCopy(text, result) {
    this.setState({
      value: text,
      copied: result,
    });
    setTimeout(() => {
      this.setState({
        copied: false,
      });
    }, 5000);
  }

  @autobind
  setPosition(userId) {
    return (event, index, value) => {
      const { position } = this.state;
      position[userId] = value;
      this.setState({ position });
    };
  }

  @autobind
  async inputHandler(input) {
    const users = await this.userService.find({
      query: {
        $limit: 5,
        $search: input,
      },
    });

    this.setState({
      users: users.data,
      username: input,
    });
  }

  @autobind
  newRequestHander(input) {
    this.setState({
      username: input,
    });
  }

  /**
   *  Show notification message
   */

  @autobind
  handleRequestClose() {
    this.setState({
      showSnackbar: false,
    });
  }

  @autobind
  showMessage(message) {
    this.setState({
      showSnackbar: true,
      snackbarMessage: message,
    });
  }

  @autobind
  handleChangeTab(value) {
    this.setState({
      slideIndex: value,
    });
  }

  @autobind
  openSnackBar() {
    const stripWord = string => string.replace(/[^a-zA-Z]/, '');
    const message = {
      isUpdated: 'Farm has been successfully updated!',
      isCreated: 'Farm has been successfully created!',
      'isUpdated=false': 'Farm has been successfully created!',
      'isCreated=false': 'Farm has been successfully updated!',
    };
    const hasAMessage = Object.keys(message).includes(stripWord(this.query));
    const outputMessage = message[stripWord(this.query)];

    if (hasAMessage) {
      this.showMessage(outputMessage);
    }
  }

  /**
   *  Modal actions (open, close)
   * @param {*} farm - retrieve the current farm
   * @param {*} action - to indicate if removefarm or addEmployee
   */
  @autobind
  async handleOpenModal(farm, action) {
    if (farm.ownerId !== this.currentUser._id) {
      return;
    }

    if (action === 'removeFarm') {
      this.setState({
        open: {
          deleteConfirmationModal: true,
          doneCancelModal: false,
          removeEmployeeModal: false,
        },
      });
    }

    if (action === 'addEmployee') {
      this.setState({
        open: {
          doneCancelModal: true,
          deleteConfirmationModal: false,
          removeEmployeeModal: false,
        },
      });
    }

    if (action === 'removeEmployee') {
      this.setState({
        open: {
          deleteConfirmationModal: false,
          doneCancelModal: true,
          removeEmployeeModal: true,
        },
      });
    }

    const currentFarm = await this.farmService.get(farm._id);
    const { position } = this.state;
    Object.assign(position, currentFarm.employeeIds.reduce((mem, id) => {
      mem[id] = 'employee'; // eslint-disable-line
      return mem;
    }, {}));
    Object.assign(position, currentFarm.managerIds.reduce((mem, id) => {
      mem[id] = 'manager'; // eslint-disable-line
      return mem;
    }, {}));
    this.setState({
      openModal: true,
      currentFarm,
      action,
      position,
    });
  }

  @autobind
  handleCloseModal() {
    this.setState({
      open: {
        doneCancelModal: false,
        deleteConfirmationModal: false,
        removeEmployeeModal: false,
      },
      position: {},
    });
    this.retrieveFarms();
  }

  @autobind
  closeRemoveEmployeeModal() {
    this.setState({
      open: {
        doneCancelModal: true,
        deleteConfirmationModal: false,
        removeEmployeeModal: false,
      },
    });
    this.retrieveFarms();
  }

  @autobind
  navigateToFarm(id) {
    this.props.history.push(`/map/${id}`);
  }

  @autobind
  updateFarm(farm) {
    if (farm.ownerId !== this.currentUser._id) {
      return;
    }

    this.props.history.push(`/farm/update/${farm._id}`);
  }

  @autobind
  async retrieveFarms() {
    const owner = await CurrentUser();
    const service = await client.callService('farms');
    const ownedFarms = await service.find({ query: { ownerId: owner._id } });
    const employedFarms = await service.find({
      query: {
        $or: [
          { employeeIds: owner._id },
          { managerIds: owner._id },
        ],
      },
    });

    this.setState({
      ownedFarms: ownedFarms.data,
      employedFarms: employedFarms.data,
      ownedFarmsTotal: ownedFarms.total,
      employedFarmsTotal: employedFarms.total,
    });
  }

  navigateToEmployeeList(farm) {
    if (farm.ownerId !== this.currentUser._id) {
      return;
    }

    this.props.history.push(`/employees/${farm._id}`);
  }

  @autobind
  toggleDetailVisibility(farmId) {
    if (this.state.visibleDetails.includes(farmId)) {
      const visibleDetails = this.state.visibleDetails.filter(detail => detail !== farmId);
      this.setState({
        visibleDetails,
      });
    } else {
      const visibleDetails = this.state.visibleDetails;
      visibleDetails.push(farmId);
      this.setState({
        visibleDetails,
      });
    }
  }

  // Employee modal handler
  @autobind
  async handleAddToEmployeeList() {
    this.addEmployee(this.state.username._id);
    this.setState({ username: '' });
  }

  @autobind
  removeEmployee(selectedEmployee) {
    const updatedEmployeeListIds =
      this.state.currentFarm.employeeIds.filter(employee => selectedEmployee._id !== employee);

    const updatedEmployeeList =
      this.state.currentFarm.employees.filter(employee => selectedEmployee._id !== employee._id);

    const removedEmployee = {
      employeeId: selectedEmployee._id,
      terminationDate: new Date(),
    };

    let removedEmployees = this.state.currentFarm.terminatedEmployees;
    if (!removedEmployees) {
      removedEmployees = [];
    }
    removedEmployees.push(removedEmployee);

    const currentFarm = this.state.currentFarm;
    currentFarm.employeeIds = updatedEmployeeListIds;
    currentFarm.employees = updatedEmployeeList;
    currentFarm.terminatedEmployees = removedEmployees;

    const { position } = this.state;
    delete position[selectedEmployee._id];

    this.setState({ currentFarm, position });
  }

  @autobind
  async addEmployee(id) {
    const userService = client.service('api/users');
    const employee = await userService.get(id);
    const user = await CurrentUser();
    const owner = new User(user);

    const { currentFarm, newEmployeeIds } = this.state;

    if (id !== owner._id) {
      currentFarm.employees.push(employee);
      currentFarm.employeeIds.push(id);
      this.showMessage('Employee successfully added!');
    }

    const { position } = this.state;
    const employeePosition = {};
    employeePosition[id] = 'employee';
    Object.assign(position, employeePosition);

    this.setState({
      currentFarm,
      newEmployeeIds: newEmployeeIds.concat(id),
      position,
    });
  }

  @autobind
  async handleSubmit(event) {
    event.preventDefault();

    const currentFarm = this.state.currentFarm;

    try {
      const { position } = this.state;
      const personnelIds = Object.keys(position);
      const employeeIds = personnelIds.filter(id => position[id] === 'employee');
      const managerIds = personnelIds.filter(id => position[id] === 'manager');
      await this.farmService.patch(currentFarm._id, {
        employeeIds,
        managerIds,
        terminatedEmployees: currentFarm.terminatedEmployees,
      });

      this.showMessage('Employee list successfully updated!');
      this.handleCloseModal();
    } catch (error) {
      console.log(error);
      this.showMessage('There was an error in updating the employee list, please try again.');
    }
  }

  /**
   *  Remove Farm
   *  Logic, and handler
   */
  @autobind
  async removeFarm(id) {
    try {
      await this.farmService.remove(id);
      this.showMessage('Farm successfully removed!');
    } catch (error) {
      console.log(error);
      this.showMessage('There was an error in removing the farm, please try again.');
    }
    this.retrieveFarms();
  }

  @autobind
  handleRemoveFarm() {
    const action = this.state.action;
    const farm = this.state.currentFarm;

    if (action === 'removeFarm') {
      this.removeFarm(farm._id);
    }

    this.handleCloseModal();
  }

  @autobind
  selectEmployeeToRemove(selectedEmployee) {
    this.setState({ selectedEmployee });
    this.handleOpenModal(this.state.currentFarm, 'removeEmployee');
  }

  @autobind
  handleRemoveEmployee() {
    const action = this.state.action;
    const selectedEmployee = this.state.selectedEmployee;
    if (action === 'removeEmployee') {
      this.removeEmployee(selectedEmployee);
      this.showMessage('Employee successfully removed!');
    }

    this.closeRemoveEmployeeModal();
  }

  @autobind
  navigateToEmployeeProfile(id) {
    this.props.history.push(`/employee/${id}`);
  }

  get isLoading() {
    const { ownedFarms, employedFarms } = this.state;
    const farms = [ownedFarms, employedFarms];
    return farms.some(farm => !farm);
  }

  // type refers to either owned or employed
  @autobind
  async retrieveFarmPage(page = 0, limit = 10, type = 'owned') {
    const owner = await CurrentUser();
    const skip = page * limit;
    let farms = [];
    if (type === 'owned') {
      farms = await this.farmService.find({ query: { ownerId: owner._id, $skip: skip } });
    } else if (type === 'employed') {
      farms = await this.farmService.find({ query: { employeeIds: owner._id, $skip: skip } });
    }
    return farms;
  }

  @autobind
  async handleShowMore() {
    const nextPage = this.state.page + 1;
    let ownedFarms = this.state.ownedFarms;
    const { data } = await this.retrieveFarmPage(nextPage);
    ownedFarms = ownedFarms.concat(data);
    this.setState({ page: nextPage, ownedFarms });
  }

  @autobind
  navigateToMessagingArea(farmId) {
    this.props.history.push(`/messaging/${farmId}`);
  }

  render() {
    let employees = [];

    const dataSourceConfig = {
      text: 'meta',
      value: '_id',
    };

    if (this.isLoading) {
      return <Spinner />;
    }

    if (this.state.currentFarm) {
      employees = this.state.currentFarm.employees;
      employees = employees.concat(this.state.currentFarm.managers);
    }


    const addEmployeeDialogContent = [
      <div key={'autocompleteTextField'}>
        <Grid fluid style={{ marginLeft: '2vw', marginRight: '2vw', padding: 0 }}>
          <Row center="xs" middle="xs">
            <Col sm={9} xs={12}>
              <AutoComplete
                floatingLabelText="Search by name"
                filter={AutoComplete.fuzzyFilter}
                dataSource={this.state.users}
                dataSourceConfig={dataSourceConfig}
                onUpdateInput={this.inputHandler}
                onNewRequest={this.newRequestHander}
                maxSearchResults={5}
                animated
                fullWidth
                key="searchEmployee"
              />
            </Col>
            <Col sm={3} xs={12} style={{ marginTop: 5 }}>
              <RaisedButton
                label="Add Employee"
                primary
                key="searchEmployeeButton"
                fullWidth
                onTouchTap={this.handleAddToEmployeeList}
              />
            </Col>
          </Row>
        </Grid>
      </div>,
      employees && employees.length ?
        <div key="employeeList">
          <MediaQuery minWidth={768}>
            <Grid fluid>
              <Row>
                <Col sm={12} style={{ height: '50vh', overflowY: 'auto', overflowX: 'hidden', marginTop: '10px' }}>
                  {employees.map(employee =>
                  (
                    <EmployeeList
                      key={employee._id}
                      employee={employee}
                      removeEmployee={() => {
                        this.selectEmployeeToRemove(employee);
                      }}
                      setPosition={this.setPosition}
                      position={this.state.position[employee._id.toString()]}
                      navigateToEmployeeProfile={() => {
                        this.navigateToEmployeeProfile(employee._id);
                      }}
                    />
                  ),
                )}
                </Col>
              </Row>
            </Grid>
          </MediaQuery>

          <MediaQuery maxWidth={767}>
            <Grid fluid style={{ margin: 0, padding: 0 }}>
              <Row key="employeeList">
                <Col xs={12} style={{ height: '50vh', overflowY: 'auto', overflowX: 'hidden', marginTop: '10px' }}>
                  {employees.map(employee =>
                    (
                      <EmployeeList
                        key={employee._id}
                        employee={employee}
                        removeEmployee={() => {
                          this.selectEmployeeToRemove(employee);
                        }}
                        navigateToEmployeeProfile={() => {
                          this.navigateToEmployeeProfile(employee._id);
                        }}
                      />
                    ),
                  )}
                </Col>
              </Row>
            </Grid>
          </MediaQuery>
        </div>
        :
        <div key="undefinedList" className="no-farm-data-available" style={{ marginTop: '9vh', marginBottom: '6vh', textAlign: 'center' }}>
          There are no employees as of now
        </div>,
    ];

    if (this.state.currentFarm) {
      this.addEmployeeDialogTitle = `Employees from ${this.state.currentFarm.name}`;
    }
    const deleteConfirmationDialogTitle = 'Are you sure you want to give up this farm?';
    const employeeRemovalConfirmTitle = `Are you sure you want to remove ${this.state.selectedEmployee.username} as your employee?`;
    const rootLink = window.location.origin;

    return (
      <div>
        <Snackbar
          open={this.state.showSnackbar}
          message={this.state.snackbarMessage}
          autoHideDuration={5000}
          onRequestClose={this.handleRequestClose}
        />

        <Modal
          key="normalModal"
          open={this.state.open.doneCancelModal}
          handleClose={this.handleCloseModal}
          dialogContent={addEmployeeDialogContent}
          dialogTitle={this.addEmployeeDialogTitle}
          handleDone={this.handleSubmit}
        />

        <ConfirmationModal
          key="deleteConfirmModal"
          open={this.state.open.deleteConfirmationModal}
          handleClose={this.handleCloseModal}
          dialogTitle={deleteConfirmationDialogTitle}
          handleDone={this.handleRemoveFarm}
        />

        <ConfirmationModal
          key="removeEmployeeModal"
          open={this.state.open.removeEmployeeModal}
          handleClose={this.closeRemoveEmployeeModal}
          dialogContent={[]}
          dialogTitle={employeeRemovalConfirmTitle}
          handleDone={this.handleRemoveEmployee}
        />

        {/* For tablets, laptops, desktops, large minitors */}
        <MediaQuery minWidth={768}>
          <Grid fluid>
            <Row center="sm" style={{ paddingTop: '5vh' }}>
              <Col sm={6}>
                <Paper style={{ margin: '0 0.5vw 4vh 0' }} className="circular" zDepth={2}>
                  <Row start="sm">
                    <Col sm={6} style={{ padding: '3vh 0 0 2vw' }}>
                      <h2 style={{ margin: '0 0 0.5vh 0', fontWeight: 500 }}>Farms</h2>
                    </Col>
                    <Col sm={6} style={{ position: 'relative', top: '2vh', right: '1vw' }}>
                      <Link to="/farm/create">
                        <RaisedButton
                          primary
                          label="Create Farm"
                          style={{ float: 'right' }}
                          labelPosition="after"
                          icon={<ContentAddCircle />}
                        />
                      </Link>
                    </Col>
                    <Col sm={9} className="faint-text" style={{ padding: '0 0 2vh 2vw' }}>
                      <h3 style={{ margin: 0, fontWeight: 400 }}>
                        These are the farms that you currently own.
                      </h3>
                    </Col>
                    <Col sm={12} className="">
                      <Divider />
                    </Col>
                    {this.state.ownedFarms.map(farm =>
                      (
                        <Col key={`owned${farm._id}`} sm={12}>
                          <FarmItem
                            key={farm._id}
                            currentUser={this.currentUser}
                            farm={farm}
                            removeFarm={() => {
                              this.handleOpenModal(farm, 'removeFarm');
                            }}
                            updateFarm={() => {
                              this.updateFarm(farm);
                            }}
                            addEmployee={() => {
                              this.handleOpenModal(farm, 'addEmployee');
                            }}
                            onTouchTap={() => {
                              this.navigateToFarm(farm._id);
                            }}
                            handleCopy={this.onCopy}
                            isCopied={this.state.copied}
                            inviteLink={`${rootLink}/invite/${farm._id}`}
                            employeeList={() => {
                              this.navigateToEmployeeList(farm);
                            }}
                            toggleDetailVisibility={() => {
                              this.toggleDetailVisibility(farm._id);
                            }}
                            isDetailVisible={this.state.visibleDetails}
                            navigateToMessagingArea={this.navigateToMessagingArea}
                          />
                          {this.state.visibleDetails.includes(farm._id) ?
                            <FarmDetails key={`ownedDetails${farm._id}`} farm={farm} /> : undefined}
                          <Divider style={{ width: '97%', marginLeft: 'auto', marginRight: 'auto' }} />
                        </Col>
                      ),
                    )}
                    {this.state.ownedFarms && this.state.ownedFarmsTotal >= 10
                      && this.state.ownedFarms.length < this.state.ownedFarmsTotal ?
                        <Col sm={12}>
                          <Row center="xs" className="link" onTouchTap={() => this.handleShowMore('owned')}>
                            <span >Show more...</span>
                          </Row>
                        </Col> : undefined
                    }
                    {this.state.ownedFarms.length === 0 ?
                      <Col sm={12} className="no-farm-data-available" style={{ textAlign: 'center', padding: '5vh 0 5vh 0' }}>
                        <span style={{ margin: '0 10px 0 10px' }}>You currently have no farm.</span>
                      </Col> : ''}
                  </Row>
                </Paper>
              </Col>
              <Col sm={6}>
                <Paper style={{ margin: '0 0 4vh 0.5vw' }} className="circular" zDepth={2}>
                  <Row start="sm">
                    <Col sm={6} style={{ padding: '3vh 0 0 2vw' }}>
                      <h2 style={{ margin: '0 0 0.5vh 0', fontWeight: 500 }}>Employers</h2>
                    </Col>
                    <Col sm={11} className="faint-text" style={{ padding: '0 0 2vh 2vw' }}>
                      <h3 style={{ margin: 0, fontWeight: 400 }}>
                        These are the farms that you are currently employed to.
                      </h3>
                    </Col>
                    <Col sm={12}>
                      <Divider />
                    </Col>
                    {this.state.employedFarms.map(farm =>
                      (
                        <Col key={`employed${farm._id}`} sm={12}>
                          <FarmItem
                            key={farm._id}
                            currentUser={this.currentUser}
                            farm={farm}
                            owned={false}
                            removeFarm={() => {
                              this.handleOpenModal(farm, 'removeFarm');
                            }}
                            updateFarm={() => {
                              this.updateFarm(farm);
                            }}
                            addEmployee={() => {
                              this.handleOpenModal(farm, 'addEmployee');
                            }}
                            onTouchTap={() => {
                              this.navigateToFarm(farm._id);
                            }}
                            handleCopy={this.onCopy}
                            isCopied={this.state.copied}
                            inviteLink={`${rootLink}/invite/${farm._id}`}
                            employeeList={() => {
                              this.navigateToEmployeeList(farm);
                            }}
                            toggleDetailVisibility={() => {
                              this.toggleDetailVisibility(farm._id);
                            }}
                            isDetailVisible={this.state.visibleDetails}
                            navigateToMessagingArea={this.navigateToMessagingArea}
                          />
                          {this.state.visibleDetails.includes(farm._id) ?
                            <FarmDetails key={`employedDetail${farm._id}`} farm={farm} /> : undefined}
                          <Divider style={{ width: '97%', marginLeft: 'auto', marginRight: 'auto' }} />
                        </Col>
                      ),
                    )}
                    {this.state.employedFarms && this.state.employedFarmsTotal >= 10
                      && this.state.employedFarms.length < this.state.employedFarmsTotal ?
                        <Col sm={12}>
                          <Row center="xs" className="link" onTouchTap={() => this.handleShowMore('employed')}>
                            <span >Show more...</span>
                          </Row>
                        </Col> : undefined
                    }
                    {this.state.employedFarms.length === 0 ?
                      <Col sm={12} className="no-farm-data-available" style={{ textAlign: 'center', padding: '5vh 0 5vh 0' }}>
                        <div style={{ margin: '0 10px 0 10px' }}>You are not currently employed to any farm.</div>
                      </Col> : ''}
                  </Row>
                </Paper>
              </Col>
            </Row>
          </Grid>
        </MediaQuery>

        {/* For mobile phones and other devices which has a with of less than 767px */}
        <MediaQuery maxWidth={767}>
          <Grid fluid style={{ margin: 0, padding: 0, overflowY: 'auto' }}>

            {this.state.slideIndex === 0 ?
              <Link to="/farm/create">
                <FloatingActionButton secondary style={{ position: 'fixed', bottom: '20px', right: '20px', zIndex: '1' }}>
                  <ContentAdd />
                </FloatingActionButton>
              </Link>
              : ''}

            <Tabs
              value={this.state.slideIndex}
              onChange={this.handleChangeTab}
              tabItemContainerStyle={{ height: 45, marginTop: -3, backgroundColor: '#43A047' }}
            >
              <Tab label={<Farm color={this.state.slideIndex === 0 ? 'white' : '#A5D6A7'} />} value={0} />
              <Tab label={<Assignment color={this.state.slideIndex === 1 ? 'white' : '#A5D6A7'} />} value={1} />
            </Tabs>

            <SwipeableViews
              index={this.state.slideIndex}
              onChangeIndex={this.handleChangeTab}
            >

              {/* Farm Tab */}
              <div style={{ height: '100%', overflowX: 'hidden', overflowY: 'auto' }} >
                {this.state.ownedFarms.map(farm =>
                  (
                    <div key={`owned${farm._id}`}>
                      <FarmItem
                        farm={farm}
                        currentUser={this.currentUser}
                        removeFarm={() => {
                          this.handleOpenModal(farm, 'removeFarm');
                        }}
                        updateFarm={() => {
                          this.updateFarm(farm);
                        }}
                        addEmployee={() => {
                          this.handleOpenModal(farm, 'addEmployee');
                        }}
                        onTouchTap={() => {
                          this.navigateToFarm(farm._id);
                        }}
                        handleCopy={this.onCopy}
                        isCopied={this.state.copied}
                        inviteLink={`${rootLink}/invite/${farm._id}`}
                        employeeList={() => {
                          this.navigateToEmployeeList(farm);
                        }}
                        toggleDetailVisibility={() => {
                          this.toggleDetailVisibility(farm._id);
                        }}
                        isDetailVisible={this.state.visibleDetails}
                        navigateToMessagingArea={this.navigateToMessagingArea}
                      />
                      {this.state.visibleDetails.includes(farm._id) ?
                        <FarmDetails key={`ownedDetails${farm._id}`} farm={farm} /> : undefined}
                      <Divider />
                    </div>
                  ),
                )}
                {this.state.ownedFarms && this.state.ownedFarmsTotal >= 10
                  && this.state.ownedFarms.length < this.state.ownedFarmsTotal ?
                    <Col xs={12}>
                      <Row center="xs" className="link" onTouchTap={() => this.handleShowMore('owned')}>
                        <span >Show more...</span>
                      </Row>
                    </Col> : undefined
                }
                {this.state.ownedFarms.length === 0 ?
                  <Col xs={12} className="no-farm-data-available" style={{ textAlign: 'center', padding: '0 5vw 0 5vw', height: window.innerHeight - 113, display: 'grid' }}>
                    <span style={{ margin: 'auto' }}>You currently have no farm.</span>
                  </Col> : ''}
              </div>

              {/* Employer Tab */}
              <div style={{ height: '100%', overflowX: 'hidden', overflowY: 'auto' }}>
                {this.state.employedFarms.map(farm =>
                  (
                    <div key={`employed${farm._id}`}>
                      <FarmItem
                        farm={farm}
                        currentUser={this.currentUser}
                        owned={false}
                        removeFarm={() => {
                          this.handleOpenModal(farm, 'removeFarm');
                        }}
                        updateFarm={() => {
                          this.updateFarm(farm);
                        }}
                        addEmployee={() => {
                          this.handleOpenModal(farm, 'addEmployee');
                        }}
                        onTouchTap={() => {
                          this.navigateToFarm(farm._id);
                        }}
                        handleCopy={this.onCopy}
                        isCopied={this.state.copied}
                        inviteLink={`${rootLink}/invite/${farm._id}`}
                        employeeList={() => {
                          this.navigateToEmployeeList(farm);
                        }}
                        toggleDetailVisibility={() => {
                          this.toggleDetailVisibility(farm._id);
                        }}
                        isDetailVisible={this.state.visibleDetails}
                        navigateToMessagingArea={this.navigateToMessagingArea}
                      />
                      {this.state.visibleDetails.includes(farm._id) ?
                        <FarmDetails key={`employedDetail${farm._id}`} farm={farm} /> : undefined}
                      <Divider />
                    </div>
                  ),
                )}
                {this.state.employedFarms && this.state.employedFarmsTotal >= 10
                  && this.state.employedFarms.length < this.state.employedFarmsTotal ?
                    <Col xs={12}>
                      <Row center="xs" className="link" onTouchTap={() => this.handleShowMore('employed')}>
                        <span >Show more...</span>
                      </Row>
                    </Col> : undefined
                }
                {this.state.employedFarms.length === 0 ?
                  <Col xs={12} className="no-farm-data-available" style={{ textAlign: 'center', padding: '0 5vw 0 5vw', height: window.innerHeight - 113, display: 'grid' }}>
                    <span style={{ margin: 'auto' }}>You are not currently employed to any farm.</span>
                  </Col> : ''}
              </div>
            </SwipeableViews>
          </Grid>
        </MediaQuery>
      </div>
    );
  }
}

FarmListContainer.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string.isRequired,
  }),
  history: PropTypes.object,
};

FarmListContainer.defaultProps = {
  location: { search: '' },
  history: {},
};

export default FarmListContainer;
