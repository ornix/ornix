import React from 'react';
import PropTypes from 'prop-types';
import Snackbar from 'material-ui/Snackbar';

import Farm from '../../models/Farm';
import EmployeeList from '../dumb/employeeList';
import client from '../client';
import autobind from '../../decorators/autobind';

class EmployeeListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.farmId = props.match.params.id;

    this.state = {
      employees: [],
      showNotification: false,
      notificationMessage: '',
    };
  }

  componentDidMount() {
    this.retrieveEmployees();
  }

  @autobind
  async retrieveEmployees() {
    const farmsService = client.service('api/farms');
    const currentFarm = await farmsService.get(this.farmId);
    const usersService = client.service('api/users');

    const employeeList = [];
    currentFarm.employeeIds.forEach(async (employee) => {
      const employeeData = await usersService.get(employee);

      employeeList.push(employeeData);
      this.setState({
        employees: employeeList,
      });
    });
  }

  @autobind
  async removeEmployee(employee) {
    const farmService = client.service('api/farms');
    const currentEmployees = this.state.employees.filter(data => data._id !== employee._id);

    this.setState({
      employees: currentEmployees,
    });

    const farmData = await farmService.get(this.farmId);
    farmData.employeeIds = this.state.employees.map(data => data._id);

    const farm = new Farm(farmData);

    try {
      await farmService.patch(this.farmId, farm.doc);
      this.setState({
        showNotification: true,
        notificationMessage: 'Employees list successfully updated!',
      });
    } catch (error) {
      this.setState({
        showNotification: true,
        notificationMessage: 'There was an error saving the employee list, try again.',
      });
    }
  }

  @autobind
  navigateToEmployeeProfile(id) {
    this.props.history.push(`/employee/${id}`);
  }

  @autobind
  handleRequestClose() {
    this.setState({
      showNotification: false,
    });
  }

  render() {
    return (
      <div>
        {this.state.employees.map(employee =>
          (<EmployeeList
            key={employee._id}
            employee={employee}
            removeEmployee={() => {
              this.removeEmployee(employee);
            }}
            hidden={false}
            navigateToEmployeeProfile={() => {
              this.navigateToEmployeeProfile(employee._id);
            }}
          />),
        )}

        <Snackbar
          open={this.state.showNotification}
          message={this.state.notificationMessage}
          autoHideDuration={4000}
          onRequestClose={this.handleRequestClose}
        />
      </div>
    );
  }
}

EmployeeListContainer.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,
};

export default EmployeeListContainer;
