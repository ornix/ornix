import socketio from 'socket.io-client';
import feathersSocketio from 'feathers-socketio';

function setupSocketio() {
  return function setupPlugin() {
    const app = this;
    const server = app.listen(location.port);

    app.configure(feathersSocketio(socketio(server)));
  };
}

export default setupSocketio;
