import React from 'react';
import { Route, Redirect } from 'react-router';
import currentUser from './user';
import Spinner from './dumb/spinner';

class ProtectedRoute extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      hasLoaded: false,
    };
  }

  async componentDidMount() {
    await this.retrieveUsers();
  }

  async retrieveUsers() {
    const user = await currentUser();
    this.setState({
      user,
      hasLoaded: true,
    });
  }

  get renderedComponent() {
    const { ...routeProps } = this.props;
    const { user, hasLoaded } = this.state;

    if (hasLoaded) {
      if (!user.isVerified) {
        if (window.location.pathname === '/verify-user') {
          return <Route {...routeProps} />;
        }
        return <Redirect to={'/verify-user'} />;
      }

      if (user.isVerified && window.location.pathname === '/verify-user') {
        return <Redirect to={'/farmList'} />;
      }

      return <Route {...routeProps} />;
    } else if (user === null) {
      return <Redirect to={'/'} />;
    }

    return <Spinner />;
  }

  render() {
    return this.renderedComponent;
  }
}

export default ProtectedRoute;
