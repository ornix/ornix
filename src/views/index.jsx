import 'react-big-calendar/lib/css/react-big-calendar.css';

import React from 'react';
import ReactDOM from 'react-dom';
import injectTapPlugin from 'react-tap-event-plugin';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import customTheme from './theme';

import './../globals.css';
import HomeView from './smart/HomeView';
import Dashboard from './dumb/dashboard';
import LoginView from './smart/LoginView';
import HeaderContainer from './smart/HeaderContainer';
import UserRegistrationFormContainer from './smart/UserRegistrationFormContainer';
import FarmListContainer from './smart/FarmListContainer';
import CrUpFarmFormContainer from './smart/CrUpFarmFormContainer';
import ChangeProfileFormContainer from './smart/ChangeProfileFormContainer';
import MapContainer from './smart/MapContainer';
import EmployeeListContainer from './smart/EmployeeListContainer';
import InviteFormContainer from './smart/InviteFormContainer';
import EmployeeProfileContainer from './smart/EmployeeProfileContainer';
import ProtectedRoute from './protectedRoute';
import Error404 from './dumb/error404';
import ActivityContainer from './smart/ActivityContainer';
import Spinner from './dumb/spinner';
import About from './dumb/about';
import ActivityViewContainer from './smart/ActivityViewContainer';
import MessagingContainer from './smart/MessagingContainer';
import VerificationForm from './smart/VerificationForm';
import ForgotPasswordView from './smart/ForgotPasswordView';


const Index = () => (
  <Router>
    <MuiThemeProvider muiTheme={customTheme}>
      <div>
        <HeaderContainer />
        <Switch>
          <Route exact path="/" component={HomeView} />
          <Route exact path="/spinner" component={Spinner} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/login" component={LoginView} />
          <Route exact path="/register" component={UserRegistrationFormContainer} />
          <Route exact path="/register/invite/:farmId" component={UserRegistrationFormContainer} />
          <Route exact path="/invite/:farmId" component={InviteFormContainer} />
          <ProtectedRoute exact path="/farmList" component={FarmListContainer} />
          <ProtectedRoute exact path="/about" component={About} />
          <ProtectedRoute exact path="/farm/:action" component={CrUpFarmFormContainer} />
          <ProtectedRoute exact path="/farm/:action/:id" component={CrUpFarmFormContainer} />
          <ProtectedRoute exact path="/changeProfile" component={ChangeProfileFormContainer} />
          <ProtectedRoute exact path="/map/:id" component={MapContainer} />
          <ProtectedRoute exact path="/employees/:id" component={EmployeeListContainer} />
          <ProtectedRoute exact path="/employee/:employeeId" component={EmployeeProfileContainer} />
          <ProtectedRoute exact path="/activity/:type/:id" component={ActivityContainer} />
          <ProtectedRoute exact path="/activityProfile/:type/:id" component={ActivityViewContainer} />
          <ProtectedRoute exact path="/messaging/:farmId" component={MessagingContainer} />
          <ProtectedRoute exact path="/verify-user" component={VerificationForm} />
          <Route exact path="/forgot-password" component={ForgotPasswordView} />
          <Route component={Error404} />
        </Switch>
      </div>
    </MuiThemeProvider>
  </Router>
);

injectTapPlugin();
ReactDOM.render(
  <Index />, document.getElementById('app'));
