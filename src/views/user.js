import client from './client';
import User from '../models/User';

/**
 * Gets the JWT token from the Local Storage
 * to verifies if the user is legitimate.
 */
async function currentUser() {
  if (window.localStorage['feathers-jwt'] !== undefined) {
    try {
      const result = await client.authenticate();
      const payload = await client.passport.verifyJWT(result.accessToken);
      const service = await client.callService('users');
      const user = await service.get(payload.userId);

      return new User(user);
    } catch (error) {
      return null;
    }
  }
  return null;
}

export default currentUser;
