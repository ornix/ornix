function setupService(serviceName, hooks) {
  return function setupPlugin() {
    const app = this;
    const service = app.service(`api/${serviceName}`);

    service.hooks(hooks);
  };
}

export default setupService;
