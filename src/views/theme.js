import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { green700, green500, green300, grey400, grey500, grey100, amberA700, white, darkBlack, fullBlack } from 'material-ui/styles/colors';

const customTheme = getMuiTheme({
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: green500,
    primary2Color: green700,
    primary3Color: green300,
    accent1Color: amberA700,
    accent2Color: grey100,
    accent3Color: grey500,
    canvasColor: white,
    textColor: darkBlack,
    alternateTextColor: white,
    shadowColor: fullBlack,
    grey400,
  },
});

export default customTheme;
