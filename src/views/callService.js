async function callService(path) {
  const client = this;
  const service = client.service(`api/${path}`);
  await client.authenticate();

  return service;
}

export default callService;
