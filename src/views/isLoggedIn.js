import decode from 'jwt-decode';

function isLoggedIn() {
  const jwt = window.localStorage.getItem('feathers-jwt');
  if (jwt) {
    const now = new Date();
    const time = now.getTime();
    const payload = decode(jwt);
    const payloadExpiration = payload.exp * 1000;
    const timeTillExpiration = payloadExpiration - time;

    return timeTillExpiration > 0;
  }
  return false;
}

export default isLoggedIn;
