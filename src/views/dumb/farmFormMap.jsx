/* global google */

import React from 'react';
import { withGoogleMap, GoogleMap, Marker, Polyline } from 'react-google-maps';
import DrawingManager from 'react-google-maps/lib/drawing/DrawingManager';
import SearchBox from 'react-google-maps/lib/places/SearchBox';

const INPUT_STYLE = {
  boxSizing: 'border-box',
  MozBoxSizing: 'border-box',
  border: '1px solid transparent',
  width: '250px',
  height: '32px',
  marginTop: '10px',
  padding: '0 12px',
  borderRadius: '1px',
  boxShadow: '0 2px 6px rgba(0, 0, 0, 0.3)',
  fontSize: '14px',
  outline: 'none',
  textOverflow: 'ellipses',
};

const farmFormMap = withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapLoad}
    defaultZoom={18}
    center={props.center}
    onClick={props.onMapClick}
    onBoundsChanged={props.onBoundsChanged}
    mapTypeId="satellite"
  >
    <SearchBox
      ref={props.onSearchBoxMounted}
      bounds={props.bounds}
      controlPosition={google.maps.ControlPosition.TOP_CENTER}
      onPlacesChanged={props.onPlacesChanged}
      inputPlaceholder="Search places..."
      inputStyle={INPUT_STYLE}
    />

    {props.center && (
      <Marker
        position={props.marker}
      />
    )}
    {/* Boundary */}
    {props.boundary ?
      <Polyline
        path={props.boundary}
        options={{
          strokeColor: '#ff0000',
          strokeWeight: 5,
        }}
      /> : <div />
    }
    <DrawingManager
      defaultDrawingMode={google.maps.drawing.OverlayType.POLYLINE}
      defaultOptions={{
        drawingControl: true,
        drawingControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT,
          drawingModes: [
            google.maps.drawing.OverlayType.POLYLINE,
          ],
        },
        polylineOptions: {
          strokeColor: '#ff0000',
          strokeWeight: 5,
        },
      }}
      onOverlayComplete={props.onOverlayComplete}
    />
  </GoogleMap>
));

export default farmFormMap;
