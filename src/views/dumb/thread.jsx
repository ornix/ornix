import React from 'react';
import { TextField, Divider, Paper } from 'material-ui';
import ArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

import Message from './message';

const thread = ({
  messages,
  onSubmit,
  onInputChange,
  value,
  currentUser,
  threadEnd,
  loadMoreMessages,
  itemCount }) => (
    <div>
      {/* For tablets, laptops, desktops, large monitors */}
      <MediaQuery minWidth={768}>
        <Grid fluid style={{ margin: 0, padding: 0 }}>
          <Row center="xs">
            <Col xs={12}>
              {
                itemCount >= messages.length ? undefined
                  : <Row center="xs">
                    <Paper onClick={loadMoreMessages}>
                      <ArrowDropUp /> See Previous Messages <ArrowDropUp />
                    </Paper>
                  </Row>
              }
              <Row start="xs" style={{ height: window.innerHeight - 220, width: '100%', margin: 0, padding: 0, overflowY: 'auto', paddingTop: '10px', marginBottom: '5px' }}>
                <Col xs={12}>
                  {messages.map(message => (
                    <Row key={message._id}>
                      <Col xs={12} style={{ marginBottom: '5px', paddingLeft: '10px', paddingRight: '10px' }}>
                        <Message messageObject={message} currentUser={currentUser} />
                      </Col>
                    </Row>
                ))}
                  <div ref={threadEnd} />
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <Divider />
                </Col>
              </Row>
              <Row center="xs" style={{ margin: '8px', position: 'relative', bottom: 0 }}>
                <Col xs={12}>
                  <form onSubmit={onSubmit}>
                    <TextField
                      name="outgoingMessage"
                      onChange={onInputChange}
                      value={value}
                      fullWidth
                      autoComplete={'off'}
                      autoFocus
                      hintText="Write a message..."
                    />
                  </form>
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </MediaQuery>

      {/* For mobile phones and other devices which has a with of less than 767px */}
      <MediaQuery maxWidth={767}>
        <Grid fluid style={{ margin: 0, padding: 0, height: window.innerHeight - 93 }}>
          <Row center="xs">
            <Col xs={12}>
              <Row center="xs">
                <Paper onClick={loadMoreMessages}>
                  <ArrowDropUp /> See Previous Messages <ArrowDropUp />
                </Paper>
              </Row>
              <Row start="xs" style={{ height: window.innerHeight - 93 - 53, width: '100%', margin: 0, padding: 0, overflowY: 'auto', paddingTop: '10px', marginBottom: '5px' }}>
                <Col xs={12}>
                  {messages.map(message => (
                    <Row key={message._id}>
                      <Col xs={12} style={{ marginBottom: '5px', paddingLeft: '10px', paddingRight: '10px' }}>
                        <Message messageObject={message} currentUser={currentUser} />
                      </Col>
                    </Row>
                  ))}
                  <div ref={threadEnd} />
                </Col>
              </Row>
              <Row>
                <Col xs={12}>
                  <Divider />
                </Col>
              </Row>
              <Row center="xs" style={{ position: 'fixed', bottom: 0, width: '100%', margin: 0, padding: 0, marginTop: '5px' }}>
                <Col xs={11}>
                  <form onSubmit={onSubmit}>
                    <TextField
                      name="outgoingMessage"
                      onChange={onInputChange}
                      value={value}
                      fullWidth
                      autoComplete={'off'}
                      autoFocus
                      hintText="Write a message..."
                    />
                  </form>
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </MediaQuery>
    </div>
);

thread.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object).isRequired,
  onSubmit: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  currentUser: PropTypes.object.isRequired,
  threadEnd: PropTypes.func.isRequired,
  loadMoreMessages: PropTypes.func.isRequired,
  itemCount: PropTypes.number,
};

thread.defaultProps = {
  itemCount: 10,
};

export default thread;
