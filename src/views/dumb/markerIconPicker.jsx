import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Toggle, Divider } from 'material-ui';
import Subheader from 'material-ui/Subheader';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

import CreateTreeForm from './createTreeForm';
import theme from '../theme';

const MarkerIconPicker = ({
  selectedIcon,
  assets,
  onIconClick,
  onInputChange,
  formErrors,
  values,
  handleDrawingModeAction,
  handleQuickPlotAction,
  selectedTool,
}) => (
  <div>
    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid className={selectedTool === 'mapMarker' ? 'map-drawer open-drawer' : 'map-drawer close-drawer'} style={{ height: '100vh', width: '20vw' }}>
        <Row className="map-drawer-content" center="sm">
          <Row start="sm">
            <Subheader>Tools</Subheader>
          </Row>
          <Col sm={12} style={{ paddingBottom: '7%' }}>
            <Row start="sm" style={{ marginBottom: '5%' }}>
              <Col sm={12}>
                <Toggle
                  label="Drawing Mode"
                  onToggle={handleDrawingModeAction}
                  trackStyle={{ backgroundColor: theme.palette.grey400 }}
                />
              </Col>
            </Row>
            <Row start="sm">
              <Col sm={12}>
                <Toggle
                  label="Enable quick-plot"
                  onToggle={handleQuickPlotAction}
                  trackStyle={{ backgroundColor: theme.palette.grey400 }}
                />
              </Col>
              <Col id="quickPlotForm" sm={12} style={{ marginLeft: '7px', marginTop: '15px', display: 'none', fontSize: '0.8em' }}>
                <CreateTreeForm
                  onInputChange={onInputChange}
                  formErrors={formErrors}
                  values={values}
                />
              </Col>
            </Row>
          </Col>
          <Divider />
          <Row start="sm">
            <Col xs={12}>
              <Row start="sm">
                <Subheader>Markers</Subheader>
              </Row>
              <Row center="sm">
                {assets.map(asset => (
                  <Col
                    sm={3}
                    onTouchTap={() => { onIconClick(asset); }}
                    key={asset}
                    style={{ display: 'inline', margin: '0px 6px 0px 6px' }}
                  >
                    <img
                      className={selectedIcon === asset ? 'marker-icon-selected' : 'marker-icon'}
                      src={`/images/assets/${asset}`}
                      alt={asset}
                      id={asset}
                    />
                  </Col>
                ))}
              </Row>
            </Col>
          </Row>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid className={selectedTool === 'mapMarker' ? 'map-drawer open-drawer' : 'map-drawer close-drawer'} style={{ height: '100vh', width: '40vw' }}>
        <Row className="map-drawer-content" center="xs">
          <Row start="xs">
            <Subheader>Tools</Subheader>
          </Row>
          <Col xs={12} style={{ paddingBottom: '7%' }}>
            <Row start="xs" style={{ marginBottom: '5%' }}>
              <Col xs={12}>
                <Toggle
                  label="Drawing Mode"
                  onToggle={handleDrawingModeAction}
                />
              </Col>
            </Row>
            <Row start="xs">
              <Col xs={12}>
                <Toggle
                  label="Enable quick-plot"
                  onToggle={handleQuickPlotAction}
                />
              </Col>
              <Col id="quickPlotForm" xs={12} style={{ paddingTop: '2vh', marginTop: '3vh', display: 'none' }}>
                <CreateTreeForm
                  onInputChange={onInputChange}
                  formErrors={formErrors}
                  values={values}
                />
              </Col>
            </Row>
          </Col>
          <Divider />
          <Row start="xs">
            <Col xs={12}>
              <Row start="xs">
                <Subheader>Markers</Subheader>
              </Row>
              <Row center="xs">
                {assets.map(asset => (
                  <Col
                    xs={3}
                    onTouchTap={() => { onIconClick(asset); }}
                    key={asset}
                    style={{ display: 'inline', margin: '0px 2px 0px 2px' }}
                  >
                    <img
                      className={selectedIcon === asset ? 'marker-icon-selected' : 'marker-icon'}
                      src={`/images/assets/${asset}`}
                      alt={asset}
                      id={asset}
                    />
                  </Col>
                ))}
              </Row>
            </Col>
          </Row>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

MarkerIconPicker.propTypes = {
  assets: PropTypes.array.isRequired,
  onIconClick: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  formErrors: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
  handleQuickPlotAction: PropTypes.func.isRequired,
  selectedIcon: PropTypes.string.isRequired,
  handleDrawingModeAction: PropTypes.func.isRequired,
  selectedTool: PropTypes.string.isRequired,
};

export default MarkerIconPicker;
