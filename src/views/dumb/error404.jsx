import React from 'react';
import { Paper } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';

const Error404 = () => (
  <Paper zDepth={0}>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ height: window.innerHeight - 64, display: 'grid' }}>
        <Row center="sm" style={{ margin: 'auto' }}>
          <Col sm={12}>
            <img src="/images/error404Icon.webp" alt="Tree" width="25%" />
          </Col>
          <Col sm={12}>
            <span className="title">PAGE NOT FOUND</span>
          </Col>
          <Col sm={12}>
            <span className="subtitle">Oops! It looks like you are lost.</span>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ height: window.innerHeight - 64, display: 'grid' }}>
        <Row center="xs" style={{ margin: 'auto' }}>
          <Col xs={12}>
            <img src="/images/error404Icon.webp" alt="Tree" width="75%" />
          </Col>
          <Col xs={12}>
            <span className="mobile-title">PAGE NOT FOUND</span>
          </Col>
          <Col xs={12}>
            <span className="mobile-subtitle">Oops! It looks like you are lost.</span>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>
  </Paper>
);

export default Error404;
