import React from 'react';
import Paper from 'material-ui/Paper';
import { RaisedButton, TextField, CardActions, Divider, Subheader, IconButton } from 'material-ui';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Update from 'material-ui/svg-icons/image/edit';
import Delete from 'material-ui/svg-icons/action/delete';
import Upload from 'material-ui-upload/Upload';
import MediaQuery from 'react-responsive';
import { Row, Col } from 'react-flexbox-grid';

import utilities from '../../utilities';

const capitalize = utilities.capitalize;

const ActivityView = ({
  activity,
  imageGallery,
  modifyActivity,
  onInputChange,
  modify,
  cancelChanges,
  saveChanges,
  formErrors,
  onFileLoad,
  removeActivity,
  author,
  upload }) => (
    <div>

      {/* For tablets, laptops, desktops, large monitors */}
      <MediaQuery minWidth={768}>
        <Paper zDepth={3} className="circular" style={{ marginBottom: '6vh' }}>
          <Row start="sm" style={{ paddingTop: '3vh', paddingBottom: '3vh' }}>
            <Col md={5} sm={4}>
              <h2 style={{ margin: '0.8vh 0 0 1vw', fontWeight: 500 }}>Activity Information</h2>
            </Col>
            <Col md={7} sm={8}>
              <RaisedButton
                primary
                label="Edit"
                style={{ float: 'right', marginRight: 10 }}
                labelPosition="after"
                onClick={modifyActivity}
                icon={<Update />}
              />
              <RaisedButton
                primary
                label="Remove"
                style={{ float: 'right', marginRight: 10 }}
                labelPosition="after"
                onClick={removeActivity}
                icon={<Delete />}
              />
            </Col>
          </Row>
          <Row>
            <Col sm={12}>
              <Divider />
            </Col>
          </Row>
          <Row start="sm" style={{ margin: 10 }}>
            <Col sm={12} style={{ marginTop: '1vh' }}>
              <span className="label">Author: </span>
              <Link to={`/employee/${author._id.toString()}`}>{author.fullName}</Link>
            </Col>
            <Col sm={12} style={{ marginTop: '1vh' }}>
              <span className="label">Type: </span>
              <span>{capitalize(activity.type)}</span>
            </Col>
            <Col sm={12} style={{ marginTop: '1vh' }}>
              <span className="label">Date: </span>
              <span>{new Date(activity.createdAt).toDateString()}</span>
            </Col>
            <Col sm={12} style={{ marginTop: '1vh' }}>
              {(activity.type !== 'water' && activity.type !== 'harvest' && activity.type !== 'report' && activity.type !== 'land-prep') ? <div><span className="label">Brand: </span><span>{activity.brand}</span></div> : null}
            </Col>
            {
            (activity.type === 'harvest') ?
              <div>
                <Col sm={12}><span className="label">Amount of Harvest: </span><span>{activity.amountOfHarvest}</span></Col>
                <Col sm={12} style={{ marginTop: '1vh' }}><span className="label">Profit: ₱</span><span>{activity.profit}</span></Col>
              </div>
            : null
          }
          </Row>
          <Row start="sm" style={{ margin: 10 }}>
            <Col sm={12} style={{ marginBottom: '1vh' }}>
              <span className="label">Remarks: </span>
            </Col>
            <Col sm={12} style={{ margin: 'auto' }}>
              {
              modify ?
                <TextField
                  hintText="Remarks"
                  floatingLabelText="Remarks"
                  name="remarks"
                  type="text"
                  fullWidth
                  multiLine
                  rows={1}
                  rowsMax={3}
                  onChange={onInputChange}
                  value={activity.remarks || ''}
                  errorText={formErrors.remarks}
                />
              : <div style={{ marginLeft: 20 }}>{activity.remarks}</div>
            }
            </Col>
          </Row>
          <Row start="sm" style={{ margin: 10 }}>
            <Col sm={12}>
              {
              modify ?
                <Upload
                  label="Add Image"
                  onFileLoad={onFileLoad}
                  hoverColor={'rgba(255, 0, 0, 0)'}
                  onClick={upload}
                />
              :
              undefined
            }
            </Col>
          </Row>
          <Row end="sm">
            <Col sm={12}>
              {
              modify ?
                <CardActions style={{ marginBottom: '1vh' }}>
                  <RaisedButton backgroundColor={'#F44336'} labelColor={'white'} label="cancel" onClick={cancelChanges} />
                  <RaisedButton primary label="save changes" onClick={saveChanges} />
                </CardActions>
              : undefined
            }
            </Col>
          </Row>
        </Paper>
        <div>
          {imageGallery}
        </div>
      </MediaQuery>

      {/* For mobile phones and other devices which has a with of less than 767px */}
      <MediaQuery maxWidth={767}>
        <Row start="xs">
          <Col xs={6}>
            <Subheader>Activity Information</Subheader>
          </Col>
          <Col xs={6}>
            <IconButton style={{ float: 'right' }} touch onTouchTap={removeActivity}><Delete color="#212121" /></IconButton>
            <IconButton style={{ float: 'right' }} touch onTouchTap={modifyActivity}><Update color="#212121" /></IconButton>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <Divider />
          </Col>
        </Row>
        <Row start="xs" style={{ margin: 10, fontSize: '0.9em' }}>
          <Col xs={12} style={{ marginTop: '1vh' }}>
            <span className="label">Type: </span>
            <span>{capitalize(activity.type)}</span>
          </Col>
          <Col xs={12} style={{ marginTop: '1vh' }}>
            <span className="label">Date: </span>
            <span>{new Date(activity.createdAt).toDateString()}</span>
          </Col>
          <Col xs={12} style={{ marginTop: '1vh' }}>
            {(activity.type !== 'water' && activity.type !== 'harvest' && activity.type !== 'report' && activity.type !== 'land-prep') ? <div><span className="label">Brand: </span><span>{activity.brand}</span></div> : null}
          </Col>
          {
          (activity.type === 'harvest') ?
            <div>
              <Col xs={12}><span className="label">Amount of Harvest: </span><span>{activity.amountOfHarvest}</span></Col>
              <Col xs={12} style={{ marginTop: '1vh' }}><span className="label">Profit: ₱</span><span>{activity.profit}</span></Col>
            </div>
          : null
        }
        </Row>
        <Row start="xs" style={{ margin: 10, fontSize: '0.9em' }}>
          <Col xs={12} style={{ marginBottom: '1vh' }}>
            <span className="label">Remarks: </span>
          </Col>
          <Col xs={12} style={{ margin: 'auto' }}>
            {
            modify ?
              <TextField
                hintText="Remarks"
                floatingLabelText="Remarks"
                name="remarks"
                type="text"
                fullWidth
                multiLine
                rows={1}
                rowsMax={3}
                onChange={onInputChange}
                value={activity.remarks || ''}
                errorText={formErrors.remarks}
              />
            : <div style={{ marginLeft: 20 }}>{activity.remarks}</div>
          }
          </Col>
        </Row>
        <Row start="xs" style={{ margin: 10 }}>
          <Col xs={12}>
            {
            modify ?
              <Upload
                label="Add Image"
                onFileLoad={onFileLoad}
                hoverColor={'rgba(255, 0, 0, 0)'}
              />
            :
            undefined
          }
          </Col>
        </Row>
        { modify ?
          <Row end="xs">
            <Col xs={12} style={{ marginBottom: 10 }}>
              <RaisedButton backgroundColor={'#F44336'} labelColor={'white'} label="cancel" onClick={cancelChanges} />
              <RaisedButton primary style={{ marginLeft: 10, marginRight: '4vw' }} label="Update" onClick={saveChanges} />
            </Col>
            <Col xs={12}>
              <Divider />
            </Col>
          </Row>
        : undefined
      }
        <Row start="xs" style={{ margin: 10, fontSize: '0.9em' }}>
          <Col xs={12}>
            <span className="label">Photos</span>
          </Col>
          <Col xs={12}>
            {imageGallery}
          </Col>
        </Row>
      </MediaQuery>
    </div>
);

ActivityView.propTypes = {
  activity: PropTypes.object.isRequired,
  imageGallery: PropTypes.object.isRequired,
  modifyActivity: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  modify: PropTypes.bool.isRequired,
  cancelChanges: PropTypes.func.isRequired,
  saveChanges: PropTypes.func.isRequired,
  formErrors: PropTypes.object.isRequired,
  author: PropTypes.object.isRequired,
  onFileLoad: PropTypes.func.isRequired,
  removeActivity: PropTypes.func.isRequired,
  upload: PropTypes.func,
};

ActivityView.defaultProps = {
  upload: () => {},
};

export default ActivityView;
