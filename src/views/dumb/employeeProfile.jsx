import React from 'react';
import Divider from 'material-ui/Divider';
import Toggle from 'material-ui/Toggle';
import Paper from 'material-ui/Paper';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Username from 'material-ui/svg-icons/action/account-box';
import Birthday from 'material-ui/svg-icons/social/cake';
import Home from 'material-ui/svg-icons/action/home';
import Work from 'material-ui/svg-icons/action/work';
import Subheader from 'material-ui/Subheader/Subheader';

const EmployeeProfile = ({ employee }) => (
  <div>
    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Row center="sm">
          <Col sm={6}>
            <Paper zDepth={3} className="circular">
              <Row center="sm">
                <Col sm={12} style={{ marginTop: '15px' }}>
                  <img
                    src="http://www.freeiconspng.com/uploads/profile-icon-9.png"
                    alt="Profile Icon"
                    width="100px"
                    height="100px"
                    style={{ borderRadius: '50%', backgroundColor: '#E0E0E0', padding: 15 }}
                  />
                </Col>
                <Col sm={12} style={{ fontSize: '1.7em', fontWeight: 500, margin: '10px 0px 10px 0px' }}>
                  {`${employee.firstName} ${employee.lastName}`}
                </Col>
                <Col sm={12}>
                  <Divider />
                </Col>
              </Row>
              <Row start="sm" style={{ fontSize: '1.1em', margin: '0px 15px 0px 15px', padding: '15px 0px 15px 0px' }}>
                <Col sm={12}>
                  <Row>
                    <Col sm={12} style={{ paddingLeft: 0 }}>
                      <Subheader>Operation</Subheader>
                    </Col>
                  </Row>
                  <Row style={{ margin: '7px 0px 7px 10px' }}>
                    <Col sm={1}>
                      <Work />
                    </Col>
                    <Col sm={11}>
                      <Toggle label="On duty" />
                    </Col>
                  </Row>
                  <Row>
                    <Col sm={12} style={{ paddingLeft: 0 }}>
                      <Subheader>Personal Information</Subheader>
                    </Col>
                  </Row>
                  <Row style={{ margin: '0px 0px 7px 10px' }}>
                    <Col sm={1}>
                      <Username />
                    </Col>
                    <Col sm={11}>
                      @{employee.username}
                    </Col>
                  </Row>
                  <Row style={{ margin: '7px 0px 7px 10px' }}>
                    <Col sm={1}>
                      <Birthday />
                    </Col>
                    <Col sm={11}>
                      {new Date(employee.dateOfBirth).toDateString()}
                    </Col>
                  </Row>
                  <Row style={{ margin: '7px 0px 7px 10px' }}>
                    <Col sm={1}>
                      <Home />
                    </Col>
                    <Col sm={11}>
                      {employee.address}
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Paper>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Row center="xs">
          <Col xs={12}>
            <Row center="xs">
              <Col xs={12} style={{ marginTop: '15px' }}>
                <img
                  src="http://www.freeiconspng.com/uploads/profile-icon-9.png"
                  alt="Profile Icon"
                  width="60px"
                  height="60px"
                  style={{ borderRadius: '50%', backgroundColor: '#E0E0E0', padding: 15 }}
                />
              </Col>
              <Col xs={12} style={{ fontSize: '1.4em', fontWeight: 500, margin: '10px 0px 10px 0px' }}>
                {`${employee.firstName} ${employee.lastName}`}
              </Col>
              <Col xs={12}>
                <Divider />
              </Col>
            </Row>
            <Row start="xs" style={{ fontSize: '1em' }}>
              <Col xs={12}>
                <Row>
                  <Col xs={12} style={{ paddingLeft: 0 }}>
                    <Subheader>Operation</Subheader>
                  </Col>
                </Row>
                <Row style={{ margin: '7px 0px 7px 10px' }}>
                  <Col xs={2}>
                    <Work />
                  </Col>
                  <Col xs={10}>
                    <Toggle label="On duty" />
                  </Col>
                </Row>
                <Row>
                  <Col xs={12} style={{ paddingLeft: 0 }}>
                    <Subheader>Personal Information</Subheader>
                  </Col>
                </Row>
                <Row style={{ margin: '0px 0px 7px 10px' }}>
                  <Col xs={2}>
                    <Username />
                  </Col>
                  <Col xs={10}>
                    @{employee.username}
                  </Col>
                </Row>
                <Row style={{ margin: '7px 0px 7px 10px' }}>
                  <Col xs={2}>
                    <Birthday />
                  </Col>
                  <Col xs={10}>
                    {new Date(employee.dateOfBirth).toDateString()}
                  </Col>
                </Row>
                <Row style={{ margin: '7px 0px 7px 10px' }}>
                  <Col xs={2}>
                    <Home />
                  </Col>
                  <Col xs={10}>
                    {employee.address}
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

EmployeeProfile.propTypes = {
  employee: PropTypes.object.isRequired,
};

export default EmployeeProfile;
