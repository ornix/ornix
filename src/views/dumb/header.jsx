import React from 'react';
import { Drawer, AppBar, MenuItem, Divider } from 'material-ui';
import { Link } from 'react-router-dom';
import ActionInfo from 'material-ui/svg-icons/action/info';
import FlatButton from 'material-ui/FlatButton';
import PropTypes from 'prop-types';

const Header = ({ toggleDrawer, closeDrawer, openDrawer, logout }) => (
  <div>
    <AppBar
      title="HARVESTREE"
      titleStyle={{ fontWeight: 300 }}
      onLeftIconButtonTouchTap={toggleDrawer}
      iconElementRight={
        <Link to="/">
          <FlatButton label="logout" onTouchTap={logout} />
        </Link>
          }
    />
    <Drawer
      open={openDrawer}
      docked={false}
      onRequestChange={closeDrawer}
    >
      <MenuItem leftIcon={<ActionInfo />}><h3>Optionsss</h3></MenuItem>
      <Divider />
      <Link to="/login">
        <MenuItem onTouchTap={closeDrawer}>
          Login
        </MenuItem>
      </Link>
      <Link to="/register">
        <MenuItem onTouchTap={closeDrawer}>
          Register
        </MenuItem>
      </Link>
      <Link to="/farmList">
        <MenuItem onTouchTap={closeDrawer}>
          Farms
        </MenuItem>
      </Link>
      <Link to="/changeProfile">
        <MenuItem onTouchTap={closeDrawer}>
          Change Profile Details
        </MenuItem>
      </Link>
      <MenuItem onTouchTap={closeDrawer}>Another Item</MenuItem>
    </Drawer>
  </div>
);

Header.propTypes = {
  toggleDrawer: PropTypes.func.isRequired,
  closeDrawer: PropTypes.func.isRequired,
  openDrawer: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired,
};

export default Header;
