import React from 'react';
import { Divider, Step, StepButton, StepContent, Stepper } from 'material-ui';
import Cloud from 'material-ui/svg-icons/file/cloud';
import Announcement from 'material-ui/svg-icons/action/announcement';
import PropTypes from 'prop-types';

const style = {
  header: {
    fontWeight: 'bold',
    fontSize: '13px',
  },
  body: {
    fontSize: '11px',
  },
  smallFontSze: {
    fontSize: '11px',
  },
  stepper: {
    maxWidth: '100%',
    maxHeight: 400,
    margin: 'auto',
    textAlign: 'left',
  },
};

const WeatherData = ({ currentWeatherMapData, forecastWeatherMapData, currentStep, stepIndex }) => (
  <div style={style.stepper}>
    <Stepper
      activeStep={currentStep}
      linear={false}
      orientation="vertical"
    >
      <Step>
        <StepButton
          icon={<Cloud />}
          onClick={() => {
            stepIndex(0);
          }}
        >
          <span style={style.header} >Current Weather</span>
        </StepButton>
        <StepContent>
          {
            currentWeatherMapData.weather.map(data => (
              <div key={`${data.dt}secretKey`}>
                <div>
                  <div style={style.smallFontSze}>
                    <span><b>{new Date(currentWeatherMapData.dt * 1000).toLocaleString()}</b></span>
                    <br />
                    <span>{currentWeatherMapData.name}, {currentWeatherMapData.sys.country}</span>
                  </div>
                  <div style={style.body}>
                    <center><img src={`http://openweathermap.org/img/w/${data.icon}.png`} alt="icon" /></center>
                  </div>
                  <div style={style.body}>
                    <center><b>{data.description}</b></center>
                    <br />
                    Temp:
                    {currentWeatherMapData.main.temp} °C
                    <br />
                    Pressure:
                    {currentWeatherMapData.main.pressure} hpa
                    <br />
                    Humidity:
                    {currentWeatherMapData.main.humidity} %
                    <br />
                    Wind:
                    {currentWeatherMapData.wind.speed} mps / {currentWeatherMapData.wind.deg} °
                  </div>
                </div>
              </div>
            ))
          }
        </StepContent>
      </Step>

      {
        Object.keys(forecastWeatherMapData).map((value, index) => (
          <Step key={value} style={style.smallFontSze}>
            <StepButton
              icon={<Announcement />}
              onClick={() => {
                stepIndex(index + 1);
              }}
            >
              <span style={style.header} >{value}</span>
            </StepButton>
            <StepContent>
              {
                  forecastWeatherMapData[value].map(data => (
                    <div key={data.dt}>
                      <Divider />
                      <div>
                        <span><b>{new Date(data.dt_txt).toLocaleTimeString()}</b></span>
                      </div>
                      <div>
                        <div >
                          <img src={`http://openweathermap.org/img/w/${data.weather[0].icon}.png`} alt={`${data.weather[0].icon}.png`} />
                        </div>
                        <div >
                          <b>{data.weather[0].description}</b>
                          <br />
                          Temp: {data.main.temp} °C <br />
                          Max Temp: {data.main.temp_max} °C <br />
                          Min Temp: {data.main.temp_min} °C <br />
                        </div>
                        <br />
                      </div>
                    </div>
                  ))
                }
            </StepContent>
          </Step>
        ))
      }
    </Stepper>
  </div>
);

WeatherData.propTypes = {
  currentWeatherMapData: PropTypes.object.isRequired,
  forecastWeatherMapData: PropTypes.object.isRequired,
  currentStep: PropTypes.number.isRequired,
  stepIndex: PropTypes.func.isRequired,
};

export default WeatherData;
