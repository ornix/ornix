import React from 'react';
import { Card, CardHeader, CardText, CardActions, TextField, FlatButton, Divider } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';
import PropTypes from 'prop-types';

const VerificationForm = ({ handleCancel, onInputChange, errorText, user, resendCode }) => (
  <div>
    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ height: window.innerHeight - 64, display: 'grid' }}>
        <Row center="xs" style={{ marginTop: 'auto', marginBottom: 'auto' }}>
          <Col xs={7}>
            <Row start="xs">
              <Col xs={12}>
                <Card className="circular">
                  <CardHeader
                    title="Verify Account"
                    titleStyle={{ fontSize: '1.3em' }}
                  />
                  <Divider/>
                  <CardText>
                    <div style={{ marginLeft: '15px', marginTop: '2vh' }}>
                      Hello <b>{user.firstName}</b>!
                      <p>
                        We have sent a code to your registered
                        mobile number (<b>{user.contactNumber}</b>),
                        please enter it here so that we can verify your identity.
                      </p>
                    </div>
                  
                  </CardText>
                  <Row center="xs" style={{ marginBottom: '3vh' }}>
                    <Col xs={7}>
                      <TextField
                        hintText="Sample Code: 123456"
                        floatingLabelText="Verify Account"
                        onChange={onInputChange}
                        name="verificationCode"
                        errorText={errorText}
                        fullWidth={true}
                      />
                    </Col>
                  </Row>
                  <Row end="xs">
                    <CardActions>
                      <FlatButton label="Cancel" onClick={handleCancel} />
                      <FlatButton type="submit" label="Verify" />
                      <FlatButton label="Resend Code" onClick={resendCode} />
                    </CardActions>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ height: window.innerHeight - 64, display: 'grid' }}>
        <Row center="xs" style={{ marginTop: 'auto', marginBottom: 'auto' }}>
          <Col xs={12}>
            <Row start="xs">
              <Col xs={12}>   
                <Card className="circular">
                  <Row>
                    <Col xs={12}>
                      <CardHeader
                        title="Verify Account"
                        titleStyle={{ fontSize: '1.2em' }}
                      />
                    </Col>
                  </Row>
                  <Divider/>
                  <CardText>
                    <div style={{ marginLeft: '10px', marginTop: '2vh' }}>
                      Hello <b>{user.firstName}</b>!
                      <p>
                        We have sent a code to your registered
                        mobile number (<b>{user.contactNumber}</b>),
                        please enter it here so that we can verify your identity.
                      </p>
                    </div>
                  
                  </CardText>
                  <Row center="xs" style={{ marginBottom: '3vh' }}>
                    <Col xs={10}>
                      <TextField
                        hintText="Sample Code: 123456"
                        floatingLabelText="Verify Account"
                        onChange={onInputChange}
                        name="verificationCode"
                        errorText={errorText}
                        fullWidth={true}
                      />
                    </Col>
                  </Row>
                  <Row end="xs">
                    <CardActions>
                      {/* <FlatButton label="Cancel" onClick={handleCancel} /> */}
                      <FlatButton type="submit" label="Verify" />
                      <FlatButton label="Resend Code" onClick={resendCode} />
                    </CardActions>
                  </Row>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

VerificationForm.propTypes = {
  handleCancel: PropTypes.func.isRequired,
  onInputChange: PropTypes.func.isRequired,
  errorText: PropTypes.string,
  user: PropTypes.object.isRequired,
  resendCode: PropTypes.func.isRequired,
};

VerificationForm.defaultProps = {
  errorText: '',
};

export default VerificationForm;
