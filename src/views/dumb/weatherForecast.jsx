import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const DrawingMode = ({
  weatherData,
  selectedTool,
}) => (
  <div>
    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid className={selectedTool === 'weather' ? 'map-drawer open-drawer' : 'map-drawer close-drawer'} style={{ height: '100vh', width: '20vw' }}>
        <Row className="map-drawer-content" center="sm">
          <Col sm={11}>
            {weatherData}
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid className={selectedTool === 'weather' ? 'map-drawer open-drawer' : 'map-drawer close-drawer'} style={{ height: '100vh', width: '40vw' }}>
        <Row className="map-drawer-content" center="xs">
          <Col xs={11}>
            {weatherData}
          </Col>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

DrawingMode.propTypes = {
  weatherData: PropTypes.element,
  selectedTool: PropTypes.string.isRequired,
};

DrawingMode.defaultProps = {
  weatherData: <div />,
};

export default DrawingMode;

