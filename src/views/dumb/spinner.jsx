import React from 'react';
import Spinner from 'react-md-spinner';
import MediaQuery from 'react-responsive';

const container = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  position: 'absolute',
  top: '65px',
  bottom: '0px',
  left: '0px',
  right: '0px',
};

const spinner = () => (
  <div style={container}>
    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <Spinner
        singleColor={'#388E3C'}
        size={80}
      />
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Spinner
        singleColor={'#388E3C'}
        size={40}
      />
    </MediaQuery>
  </div>
);

export default spinner;
