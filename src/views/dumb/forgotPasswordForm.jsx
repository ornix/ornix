import React from 'react';
import { Stepper, Step, StepLabel } from 'material-ui';
import ExpandTransition from 'material-ui/internal/ExpandTransition';
import PropTypes from 'prop-types';

const ForgotPasswordForm = ({ stepIndex, loading, renderContent }) => (
  <div style={{ width: '100%', maxWidth: 700, margin: 'auto' }}>
    <Stepper activeStep={stepIndex}>
      <Step>
        <StepLabel>Confirm Username</StepLabel>
      </Step>
      <Step>
        <StepLabel>Verify Account</StepLabel>
      </Step>
      <Step>
        <StepLabel>Change Password</StepLabel>
      </Step>
      <Step>
        <StepLabel>Congratulations!</StepLabel>
      </Step>
    </Stepper>
    <ExpandTransition loading={loading} open>
      {renderContent}
    </ExpandTransition>
  </div>
);

ForgotPasswordForm.propTypes = {
  stepIndex: PropTypes.number.isRequired,
  loading: PropTypes.bool.isRequired,
  renderContent: PropTypes.object.isRequired,
};

export default ForgotPasswordForm;
