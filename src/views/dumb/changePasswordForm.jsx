import React from 'react';
import { Paper, TextField, RaisedButton, Subheader, Divider } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import PasswordField from 'material-ui-password-field';

const ChangePasswordForm = ({ onInputChange, formErrors }) => (
  <div>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Row center="xs">
        <Col xs={6}>
          <Paper zDepth={3} className="circular">
            <Row start="sm">
              <Col sm={12} style={{ marginTop: '5px' }}>
                <Subheader style={{ fontSize: '1.5em', color: 'black', fontWeight: 500 }}>Change Password</Subheader>
              </Col>
              <Col sm={12}>
                <Divider />
              </Col>
            </Row>
            <Row center="sm">
              <Col sm={11}>
                <Row start="sm">
                  <Col sm={12}>
                    <TextField
                      hintText="Enter Old Password"
                      floatingLabelText="Old Password"
                      errorText={formErrors.oldPassword}
                      type="password"
                      name="oldPassword"
                      fullWidth
                      onChange={onInputChange}
                    />
                  </Col>
                  <Col sm={12}>
                    <PasswordField
                      floatingLabelText="New Password"
                      hintText="Enter New Password"
                      type="password"
                      name="newPassword"
                      fullWidth
                      errorText={formErrors.newPassword}
                      onChange={onInputChange}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row end="sm">
              <Col sm={12} style={{ margin: '4vh 2vw 2vh 0' }}>
                <RaisedButton
                  label="Save Changes"
                  type="submit"
                  primary
                />
              </Col>
            </Row>
          </Paper>
        </Col>
      </Row>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Grid fluid>
        <Row style={{ marginBottom: '2vh' }}>
          <Paper zDepth={0}>
            <Row>
              <Col xs={12}>
                <TextField
                  hintText="Enter Old Password"
                  floatingLabelText="Old Password"
                  floatingLabelFixed
                  errorText={formErrors.oldPassword}
                  type="password"
                  name="oldPassword"
                  fullWidth
                  onChange={onInputChange}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Enter New Password"
                  floatingLabelText="New Password"
                  errorText={formErrors.newPassword}
                  floatingLabelFixed
                  type="password"
                  name="newPassword"
                  fullWidth
                  onChange={onInputChange}
                />
              </Col>
              <Col xs={12} style={{ marginTop: '2vh' }}>
                <RaisedButton
                  label="Save Changes"
                  type="submit"
                  primary
                  fullWidth
                />
              </Col>
            </Row>
          </Paper>
        </Row>
      </Grid>
    </MediaQuery>

  </div>
);

ChangePasswordForm.propTypes = {
  onInputChange: PropTypes.func.isRequired,
  formErrors: PropTypes.object.isRequired,
};

export default ChangePasswordForm;
