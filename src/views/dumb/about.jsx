import React from 'react';
import { Paper, TextField, DatePicker, RaisedButton, Divider, Subheader } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const About = () => (
  <div>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ top: 50, position: 'relative', zIndex: 2, margin: 0, padding: 0 }}>
        <Row center="sm">
          <Col sm={6} style={{ marginTop: '40px' }}>
            <Paper zDepth={3} className="circular">
              <Row start="sm">
                <Col sm={12} style={{ marginTop: '5px' }}>
                  <Subheader style={{ fontSize: '1.5em', color: 'black', fontWeight: 500 }}>About</Subheader>
                </Col>
                <Col sm={12}>
                  <Divider />
                </Col>
              </Row>
              <Row center="sm">
                <Col sm={11}>
                  <d style={{ float: 'left', textAlign: 'left', textIndent: '50px', fontSize: '1.2em', margin: '10px' }}>
                  
                  <p>The vast majority of Filipino farmers and farm owners lack access to modern technology when 
                  it comes to monitoring fruit trees. Since many trees are monitored all at once, it is tedious 
                  to take every detail into account. Harvestree is a web application designed to mitigate this 
                  problem and help farm owners and employees monitor fruit trees with the aid of geographic 
                  information technology. The system primarily uses the Google Maps API for interactive mapping 
                  and integrates various third-party services to transform data into information such as weather 
                  and vegetation index which may influence the decisions of the farm’s stakeholders.</p>

                  
                  <p>Monitoring a single fruit tree uses papers and since several trees are being monitored, 
                  the amount of paper that was used would be a significant figure. 
                  Also, recording the history is very arduous in terms of recording and retrieving. 
                  The Harvestree is a farm management application that provides a complete set of functionalities 
                  to promote a simple and efficient method of farm management.</p>
                  
                  <p>It aims to innovate the existing methods of farm owners and their employees in accomplishing 
                  their daily tasks with the aid of geographical technologies. The application contains features 
                  such as farm plotting, tree and area marking, harvest and profit statistics, activity logging, 
                  weather forecasting, and employee monitoring. It is a web-based application hosted by Heroku 
                  and uses Google Maps API.</p>
                  </d>
                </Col>
              </Row>
            </Paper>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ backgroundColor: 'white' }}>
        <Row style={{ marginBottom: '2vh' }}>
          <Paper zDepth={0}>
            <Row center="sm">
              <Col sm={11}>
                <d style={{ float: 'left', textAlign: 'left', textIndent: '50px', fontSize: '1em', margin: '10px' }}>
                
                <p>The vast majority of Filipino farmers and farm owners lack access to modern technology when 
                it comes to monitoring fruit trees. Since many trees are monitored all at once, it is tedious 
                to take every detail into account. Harvestree is a web application designed to mitigate this 
                problem and help farm owners and employees monitor fruit trees with the aid of geographic 
                information technology. The system primarily uses the Google Maps API for interactive mapping 
                and integrates various third-party services to transform data into information such as weather 
                and vegetation index which may influence the decisions of the farm’s stakeholders.</p>

                
                <p>Monitoring a single fruit tree uses papers and since several trees are being monitored, 
                the amount of paper that was used would be a significant figure. 
                Also, recording the history is very arduous in terms of recording and retrieving. 
                The Harvestree is a farm management application that provides a complete set of functionalities 
                to promote a simple and efficient method of farm management.</p>
                
                <p>It aims to innovate the existing methods of farm owners and their employees in accomplishing 
                their daily tasks with the aid of geographical technologies. The application contains features 
                such as farm plotting, tree and area marking, harvest and profit statistics, activity logging, 
                weather forecasting, and employee monitoring. It is a web-based application hosted by Heroku 
                and uses Google Maps API.</p>
                </d>
              </Col>
            </Row>
          </Paper>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

export default About;
