import React from 'react';
import { Paper, TextField, DatePicker, RaisedButton, FlatButton, AppBar, IconButton } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Link } from 'react-router-dom';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import Divider from 'material-ui/Divider/Divider';

const UserRegistrationForm = ({ onInputChange, formErrors, onBlur, onUsernameBlur }) => (
  <div style={{ fontWeight: 400 }}>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <AppBar
        iconElementLeft={
          <IconButton
            containerElement={<Link to="/login" />}
          >
            <ArrowBack />
          </IconButton>
        }
      />
      <Grid fluid>
        <Row center="sm" style={{ marginTop: '5vh', marginBottom: '6vh' }}>
          <Col sm={12}>
            <span className="create-account-title">Create your Harvestree Account!</span>
          </Col>
        </Row>
        <Row center="sm">
          <Col sm={6}>
            <Row>
              <Col sm={12} style={{ marginBottom: '2vh' }}>
                <span className="create-account-subtitle">
                  Choose from different pins to mark a tree or an area for monitoring. 
                </span>
              </Col>
              <Col sm={12} style={{ marginBottom: '8vh' }}>
                <img src="./images/pinList.png" alt="Geo" width="57%" />
              </Col>
              <Col sm={12} style={{ marginBottom: '2vh' }}>
                <span className="subtitle">
                  Monitor your farm at any time in the comfort of your home.
                </span>
              </Col>
              <Col sm={12}>
                <img
                  src="./images/loginPageImage1.png"
                  alt="Laptop"
                  width="53%"
                />
              </Col>
            </Row>
          </Col>
          <Col sm={4} smOffset={1} style={{ marginBottom: '4vh' }}>
            <Paper zDepth={2} className="circular">
              <Row start="sm" style={{ padding: '0 1.5vw 2vh 1.5vw' }}>
                <Col sm={12}>
                  <TextField
                    hintText="Username"
                    floatingLabelText="Username"
                    errorText={formErrors.username}
                    type="text"
                    name="username"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onUsernameBlur}
                  />
                </Col>
                <Col sm={6}>
                  <TextField
                    hintText="Password"
                    floatingLabelText="Password"
                    errorText={formErrors.password}
                    type="password"
                    name="password"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onBlur}
                  />
                </Col>
                <Col sm={6}>
                  <TextField
                    hintText="Confirm Password"
                    floatingLabelText="Confirm Password"
                    errorText={formErrors.confirmPassword}
                    type="password"
                    name="confirmPassword"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onBlur}
                  />
                </Col>
                <Col sm={12}>
                  <TextField
                    hintText="First Name"
                    floatingLabelText="First Name"
                    errorText={formErrors.firstName}
                    type="text"
                    name="firstName"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onBlur}
                  />
                </Col>
                <Col sm={12}>
                  <TextField
                    hintText="Middle Name"
                    floatingLabelText="Middle Name"
                    errorText={formErrors.middleName}
                    type="text"
                    name="middleName"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onBlur}
                  />
                </Col>
                <Col sm={12}>
                  <TextField
                    hintText="Last Name"
                    floatingLabelText="Last Name"
                    errorText={formErrors.lastName}
                    type="text"
                    name="lastName"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onBlur}
                  />
                </Col>
                <Col sm={12}>
                  <TextField
                    hintText="Address"
                    floatingLabelText="Address"
                    errorText={formErrors.address}
                    type="text"
                    multiLine
                    rows={1}
                    rowsMax={3}
                    name="address"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onBlur}
                  />
                </Col>
                <Col sm={12}>
                  <TextField
                    hintText="639*********"
                    floatingLabelText="Contact Number"
                    errorText={formErrors.contactNumber}
                    type="text"
                    name="contactNumber"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onBlur}
                  />
                </Col>
                <Col sm={12}>
                  <DatePicker
                    hintText="Date of Birth"
                    floatingLabelText="Date of Birth"
                    errorText={formErrors.dateOfBirth}
                    name="dateOfBirth"
                    fullWidth
                    onChange={onInputChange}
                    onBlur={onBlur}
                  />
                </Col>
                <Col sm={12} style={{ marginTop: '2vh' }}>
                <FlatButton className="button-register-form" type="submit" fullWidth label="Register" labelStyle={{ color: 'white' }} />
                </Col>
              </Row>
            </Paper>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <AppBar
        style={{ height: 45 }}
        iconStyleLeft={{ marginTop: -1, marginLeft: -24 }}
        iconElementLeft={
          <IconButton
            containerElement={<Link to="/login" />}
          >
            <ArrowBack />
          </IconButton>
        }
      />
      <Grid fluid style={{ backgroundColor: 'white' }}>
        <Row style={{ marginBottom: '2vh' }}>
          <Paper zDepth={0}>
            <Row>
              <Col xs={12}>
                <TextField
                  hintText="Username"
                  floatingLabelText="Username"
                  floatingLabelFixed
                  errorText={formErrors.username}
                  type="text"
                  name="username"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onUsernameBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Password"
                  floatingLabelText="Password"
                  floatingLabelFixed
                  errorText={formErrors.password}
                  type="password"
                  name="password"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Confirm Password"
                  floatingLabelText="Confirm Password"
                  floatingLabelFixed
                  errorText={formErrors.confirmPassword}
                  type="password"
                  name="confirmPassword"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="First Name"
                  floatingLabelText="First Name"
                  floatingLabelFixed
                  errorText={formErrors.firstName}
                  type="text"
                  name="firstName"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Middle Name"
                  floatingLabelText="Middle Name"
                  floatingLabelFixed
                  errorText={formErrors.middleName}
                  type="text"
                  name="middleName"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Last Name"
                  floatingLabelText="Last Name"
                  floatingLabelFixed
                  errorText={formErrors.lastName}
                  type="text"
                  name="lastName"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Address"
                  floatingLabelText="Address"
                  floatingLabelFixed
                  errorText={formErrors.address}
                  type="text"
                  multiLine
                  rows={1}
                  rowsMax={3}
                  name="address"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="639*********"
                  floatingLabelText="Contact Number"
                  floatingLabelFixed
                  errorText={formErrors.contactNumber}
                  type="text"
                  name="contactNumber"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <DatePicker
                  hintText="Date of Birth"
                  floatingLabelText="Date of Birth"
                  floatingLabelFixed
                  errorText={formErrors.dateOfBirth}
                  name="dateOfBirth"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12} style={{ marginTop: '2vh' }}>
                {/* <RaisedButton type="submit" primary fullWidth label="Register" className="circular" /> */}
                <FlatButton className="mobile-button-register" type="submit" fullWidth label="Register" labelStyle={{ color: 'white' }} />
              </Col>
            </Row>
          </Paper>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

UserRegistrationForm.propTypes = {
  onInputChange: PropTypes.func.isRequired,
  formErrors: PropTypes.object.isRequired,
  onBlur: PropTypes.func.isRequired,
  onUsernameBlur: PropTypes.func.isRequired,
};

export default UserRegistrationForm;
