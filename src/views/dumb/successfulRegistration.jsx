import React from 'react';
import { Paper, RaisedButton } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const SuccessfulRegistration = ({ redirectToHome }) => (
  <Paper zDepth={0}>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ height: window.innerHeight - 64, display: 'grid' }}>
        <Row center="sm" style={{ margin: 'auto' }}>
          <Col sm={12}>
            <img src="/images/checkIcon.png" alt="tree" width="10%" />
          </Col>
          <Col sm={12}>
            <span className="title">Registration Successful</span>
          </Col>
          <Col sm={12} style={{ marginBottom: '4vh' }}>
            <span className="subtitle">Congratulations! You have successfully created a Harvestree account.</span>
          </Col>
          <Col lg={2} md={3} sm={3}>
            <RaisedButton
              primary
              label="Home"
              fullWidth
              onTouchTap={redirectToHome}
            />
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ height: window.innerHeight - 64, display: 'grid', margin: 0, padding: 0 }}>
        <Row center="xs" style={{ margin: 'auto' }}>
          <Col xs={12}>
            <img src="/images/checkIcon.png" alt="tree" width="35%" />
          </Col>
          <Col xs={12} style={{ margin: '1vh 0 0 0' }}>
            <span className="mobile-title">Registration Successful</span>
          </Col>
          <Col xs={12} style={{ margin: '1vh 0 4vh 0' }}>
            <span className="mobile-subtitle">Congratulations! You have successfully created a Harvestree account.</span>
          </Col>
          <Col xs={10}>
            <RaisedButton
              primary
              label="Home"
              fullWidth
              onTouchTap={redirectToHome}
            />
          </Col>
        </Row>
      </Grid>
    </MediaQuery>
  </Paper>
);

SuccessfulRegistration.propTypes = {
  redirectToHome: PropTypes.func.isRequired,
};

export default SuccessfulRegistration;
