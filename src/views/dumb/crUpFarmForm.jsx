import React from 'react';
import { RaisedButton, Snackbar } from 'material-ui';
import { Link } from 'react-router-dom';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

import FarmFormMap from './farmFormMap';

const styles = {
  paper: {
    marginTop: '10%',
  },
  titleContainer: {
    margin: '5px',
  },
  title: {
    fontWeight: 100,
    fontSize: '40px',
  },
  contentContainer: {
    marginLeft: '10px',
    marginRight: '10px',
  },
  mapContainerElement: {
    position: 'absolute',
    top: 64,
    bottom: 0,
    left: 0,
    right: 0,
  },
  mobileMapContainerElement: {
    position: 'absolute',
    top: 45,
    bottom: 0,
    left: 0,
    right: 0,
  },
  mapElement: {
    height: '100%',
    width: '100%',
  },
  buttonContainer: {
    marginLeft: '5%',
    marginRight: '5%',
    marginBottom: '2%',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    display: 'flex',
    flexWrap: 'wrap',
  },
  mobileButtonContainer: {
    margin: '0 15vw 23px 6vw',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  createButtonContainer: {
    marginBottom: '5px',
  },
  createButtonLabel: {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  createButton: {
    height: '40px',
    width: '100px',
    flex: '1 1 auto',
    margin: '10px',
  },
  cancelButtonLabel: {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancelButton: {
    height: '40px',
    width: '100px',
    flex: '1 1 auto',
    display: 'flex',
  },
  cancelButtonLink: {
    flex: '1 1 auto',
    margin: '10px',
    display: 'flex',
    textDecoration: 'none',
  },
};

const CrUpFarmForm = ({
  onMapLoad,
  marker,
  onMapClick,
  revealNotification,
  message,
  handleRequestClose,
  center,
  onPlotFarm,
  onOverlayComplete,
  boundary,
  willCreate,
  onPlacesChanged,
  bounds,
  onSearchBoxMounted,
}) => (
  <Grid fluid>

    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <FarmFormMap
        containerElement={
          <div style={styles.mapContainerElement} />
        }
        mapElement={
          <div style={styles.mapElement} />
        }
        onMapLoad={onMapLoad}
        marker={marker}
        onMapClick={onMapClick}
        center={center}
        onOverlayComplete={onOverlayComplete}
        boundary={boundary}
        onPlacesChanged={onPlacesChanged}
        bounds={bounds}
        onSearchBoxMounted={onSearchBoxMounted}
      />

      <div style={styles.buttonContainer}>
        <Link to={'/farmList'} style={styles.cancelButtonLink}>
          <RaisedButton
            primary
            label={'Cancel'}
            labelStyle={styles.cancelButtonLabel}
            style={styles.cancelButton}
          />
        </Link>
        <RaisedButton
          primary
          label={willCreate ? 'Plot Farm' : 'Update Farm'}
          labelStyle={styles.createButtonLabel}
          style={styles.createButton}
          onClick={onPlotFarm}
        />
      </div>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <FarmFormMap
        containerElement={
          <div style={styles.mobileMapContainerElement} />
          }
        mapElement={
          <div style={styles.mapElement} />
          }
        onMapLoad={onMapLoad}
        marker={marker}
        onMapClick={onMapClick}
        center={center}
        onOverlayComplete={onOverlayComplete}
        boundary={boundary}
        onPlacesChanged={onPlacesChanged}
        bounds={bounds}
        onSearchBoxMounted={onSearchBoxMounted}
      />

      <Row center="xs" style={styles.mobileButtonContainer}>
        <Col xs={12}>
          <Row center="xs">
            <Col xs={12} style={{ marginBottom: 10 }}>
              <RaisedButton
                primary
                label={'Plot Farm'}
                onClick={onPlotFarm}
                fullWidth
              />
            </Col>
            <Col xs={12}>
              <Link to={'/farmList'}>
                <RaisedButton
                  primary
                  label={'Cancel'}
                  fullWidth
                />
              </Link>
            </Col>
          </Row>
        </Col>
      </Row>
    </MediaQuery>

    <Snackbar
      open={revealNotification}
      message={message || '✔ Map Center Successfully Captured'}
      autoHideDuration={3000}
      onRequestClose={handleRequestClose}
    />
  </Grid>
  );

CrUpFarmForm.propTypes = {
  onMapLoad: PropTypes.func.isRequired,
  marker: PropTypes.object.isRequired,
  onMapClick: PropTypes.func.isRequired,
  revealNotification: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  handleRequestClose: PropTypes.func.isRequired,
  center: PropTypes.object.isRequired,
  onPlotFarm: PropTypes.func.isRequired,
  onOverlayComplete: PropTypes.func.isRequired,
  boundary: PropTypes.array.isRequired,
  willCreate: PropTypes.bool.isRequired,
  onPlacesChanged: PropTypes.func.isRequired,
  bounds: PropTypes.array.isRequired,
};

export default CrUpFarmForm;
