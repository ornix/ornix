import React from 'react';
import { Avatar, List, ListItem, IconButton } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import CopyToClipboard from 'react-copy-to-clipboard';
import Update from 'material-ui/svg-icons/image/edit';
import Delete from 'material-ui/svg-icons/action/delete';
import People from 'material-ui/svg-icons/social/people';
import ContentCopy from 'material-ui/svg-icons/content/content-copy';
import Assessment from 'material-ui/svg-icons/action/assessment';
import KeyboardArrowUp from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import Comment from 'material-ui/svg-icons/communication/comment';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import utilities from '../../utilities';
import customTheme from './../theme';

const capitalize = utilities.capitalize;

const FarmItem = ({
  farm,
  currentUser,
  removeFarm,
  updateFarm,
  addEmployee,
  onTouchTap,
  handleCopy,
  isCopied,
  inviteLink,
  toggleDetailVisibility,
  isDetailVisible,
  navigateToMessagingArea,
    }) => (
      <div>

        {/* For tablets, laptops, desktops, large minitors */}
        <MediaQuery minWidth={768}>
          <Grid fluid style={{ margin: 0, padding: '0 1.5vw 0 1.5vw' }}>
            <Row middle="sm">
              <Col lg={5} sm={4}>
                <List>
                  <ListItem
                    onTouchTap={onTouchTap}
                    hoverColor={customTheme.palette.alternateTextColor}
                    primaryText={capitalize(farm.name)}
                    secondaryText={capitalize(farm.description)}
                    leftAvatar={
                      <Avatar
                        backgroundColor={
                          farm.employeeIds.includes(currentUser._id) ?
                          customTheme.palette.accent1Color : customTheme.palette.primary2Color
                        }
                        color={customTheme.palette.alternateTextColor}
                        size={45}
                        style={{ fontWeight: 300 }}
                      >
                        {farm.name.charAt(0).toUpperCase()}
                      </Avatar>
                  }
                  />
                </List>
              </Col>
              <Col lg={7} sm={8}>
                <Row end="sm">
                  <Col sm={12}>
                    <Row end="sm">
                      {
                      currentUser._id === farm.ownerId ?
                        <Col sm={12}>
                          <IconButton
                            tooltip={isDetailVisible.includes(farm._id) ? 'Hide Details' : 'Show Details'}
                            touch
                            tooltipPosition="top-center"
                            onTouchTap={toggleDetailVisibility}
                          >
                            {isDetailVisible.includes(farm._id) ?
                              <KeyboardArrowUp /> : <Assessment />}
                          </IconButton>

                          <CopyToClipboard text={inviteLink} onCopy={handleCopy}>
                            <IconButton
                              tooltip={isCopied ? 'Link Copied!' : 'Copy Link'}
                              touch
                              tooltipPosition="top-center"
                            >
                              <ContentCopy />
                            </IconButton>
                          </CopyToClipboard>

                          <IconButton
                            tooltip="Remove"
                            touch
                            tooltipPosition="top-center"
                            onTouchTap={removeFarm}
                          >
                            <Delete />
                          </IconButton>

                          <IconButton
                            tooltip="Update"
                            touch
                            tooltipPosition="top-center"
                            onTouchTap={updateFarm}
                          >
                            <Update />
                          </IconButton>
                          <IconButton
                            tooltip="Employees"
                            touch
                            tooltipPosition="top-center"
                            onTouchTap={addEmployee}
                          >
                            <People />
                          </IconButton>

                          <IconButton
                            tooltip="Messaging"
                            touch
                            tooltipPosition="top-center"
                            onTouchTap={() => { navigateToMessagingArea(farm._id); }}
                          >
                            <Comment />
                          </IconButton>
                        </Col>
                      :
                        <Col sm={12}>
                          <IconButton
                            tooltip={isDetailVisible.includes(farm._id) ? 'Hide Details' : 'Show Details'}
                            touch
                            tooltipPosition="top-center"
                            onTouchTap={toggleDetailVisibility}
                          >
                            {isDetailVisible.includes(farm._id) ?
                              <KeyboardArrowUp /> : <Assessment />}
                          </IconButton>

                          <CopyToClipboard text={inviteLink} onCopy={handleCopy}>
                            <IconButton
                              tooltip={isCopied ? 'Link Copied!' : 'Copy Link'}
                              touch
                              tooltipPosition="top-center"
                            >
                              <ContentCopy />
                            </IconButton>
                          </CopyToClipboard>

                          <IconButton
                            tooltip="Messaging"
                            touch
                            tooltipPosition="top-center"
                            onTouchTap={() => { navigateToMessagingArea(farm._id); }}
                          >
                            <Comment />
                          </IconButton>
                        </Col>
                    }

                    </Row>
                    <Row end="sm">
                      <Col sm={12}>
                        {new Date(farm.createdAt).toDateString()}
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Grid>
        </MediaQuery>

        {/* For mobile phones and other devices which has a with of less than 767px */}
        <MediaQuery maxWidth={767}>
          <ListItem
            onTouchTap={onTouchTap}
            primaryText={capitalize(farm.name)}
            secondaryText={capitalize(farm.description)}
            leftAvatar={
              <Avatar
                backgroundColor={
                  farm.employeeIds.includes(currentUser._id) ?
                  customTheme.palette.accent1Color : customTheme.palette.primary2Color
                }
                color={customTheme.palette.alternateTextColor}
                style={{ fontWeight: 300 }}
              >
                {farm.name.charAt(0).toUpperCase()}
              </Avatar>
            }
            initiallyOpen={false}
            primaryTogglesNestedList
            onNestedListToggle={
              isDetailVisible.includes(farm._id) ? toggleDetailVisibility : undefined
            }
            nestedItems={
              farm.ownerId === currentUser._id ?
              [
                <CopyToClipboard key={1} text={inviteLink} onCopy={handleCopy}>
                  <ListItem
                    primaryText={isCopied ? 'Link Copied!' : 'Copy Link'}
                    leftIcon={<ContentCopy />}
                  />
                </CopyToClipboard>,
                <ListItem
                  key={2}
                  primaryText="Remove"
                  onTouchTap={removeFarm}
                  leftIcon={<Delete />}
                />,
                <ListItem
                  key={3}
                  primaryText="Update"
                  onTouchTap={updateFarm}
                  leftIcon={<Update />}
                />,
                <ListItem
                  key={4}
                  primaryText="Employees"
                  onTouchTap={addEmployee}
                  leftIcon={<People />}
                />,
                <ListItem
                  key={6}
                  primaryText="Messaging"
                  onTouchTap={() => { navigateToMessagingArea(farm._id); }}
                  leftIcon={<Comment />}
                />,
                <ListItem
                  key={5}
                  primaryText="Details"
                  onTouchTap={toggleDetailVisibility}
                  leftIcon={
                  isDetailVisible.includes(farm._id) ? <KeyboardArrowUp /> : <Assessment />
                }
                />,
              ]
              :
              [
                <CopyToClipboard key={1} text={inviteLink} onCopy={handleCopy}>
                  <ListItem
                    primaryText={isCopied ? 'Link Copied!' : 'Copy Link'}
                    leftIcon={<ContentCopy />}
                  />
                </CopyToClipboard>,
                <ListItem
                  key={6}
                  primaryText="Messaging"
                  onTouchTap={() => { navigateToMessagingArea(farm._id); }}
                  leftIcon={<Comment />}
                />,
                <ListItem
                  key={5}
                  primaryText="Details"
                  onTouchTap={toggleDetailVisibility}
                  leftIcon={
                  isDetailVisible.includes(farm._id) ? <KeyboardArrowUp /> : <Assessment />
                }
                />,
              ]
            }
          />
        </MediaQuery>
      </div>
  );

FarmItem.propTypes = {
  farm: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
  removeFarm: PropTypes.func.isRequired,
  updateFarm: PropTypes.func.isRequired,
  addEmployee: PropTypes.func.isRequired,
  onTouchTap: PropTypes.func.isRequired,
  handleCopy: PropTypes.func.isRequired,
  inviteLink: PropTypes.string.isRequired,
  toggleDetailVisibility: PropTypes.func.isRequired,
  isDetailVisible: PropTypes.array.isRequired,
  isCopied: PropTypes.bool.isRequired,
  navigateToMessagingArea: PropTypes.func.isRequired,
};

export default FarmItem;
