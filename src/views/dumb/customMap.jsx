import React from 'react';
import { withGoogleMap, GoogleMap, Marker, InfoWindow, Polygon, Rectangle, Polyline } from 'react-google-maps';
import DrawingManager from 'react-google-maps/lib/drawing/DrawingManager';
import InfoContent from './infoContent';

const iconUrl = (marker) => {
  if (marker.selected) {
    return {
      url: '/images/assets/pinDefault.png',
    };
  }
  if (marker.icon) {
    return {
      url: marker.icon,
    };
  }
  return undefined;
};

const CustomMap = withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapLoad}
    defaultZoom={18}
    center={props.center ? props.center : { lat: 10.7202, lng: 122.5621 }}
    onClick={props.onMapClick}
    onRightClick={props.onMapRightClick}
    mapTypeId="satellite"
  >
    {props.markers.map(marker => (
      <Marker
        key={marker._id}
        position={marker.position}
        onClick={() => props.onMarkerClick(marker)}
        icon={iconUrl(marker)}
      >
        { marker.showInfo && (
          <InfoWindow onCloseClick={() => props.onMarkerClose(marker)}>
            <InfoContent
              marker={marker}
              removeMarker={() => { props.removeMarker(marker); }}
            />
          </InfoWindow>
        )}
      </Marker>
    ))}
    {/* Boundary */}
    <Polyline
      path={props.boundary}
      options={{
        strokeColor: '#ff4500',
        strokeWeight: 5,
      }}
    />
    {props.areas.map((area) => {
      // if (area.shape === 'circle') {
      //   return (
      //     <Circle
      //       key={area._id}
      //       radius={area.radius}
      //       center={area.center}
      //       onClick={(event) => {
      //         // eslint-disable-next-line no-param-reassign
      //         area.position = event.latLng;
      //         props.onMarkerClick(area);
      //       }}
      //     />
      //   );
      // }
      if (area.shape === 'rectangle') {
        return (
          <Rectangle
            key={area._id}
            bounds={area.bounds}
            onClick={(event) => {
              // eslint-disable-next-line no-param-reassign
              area.position = event.latLng;
              props.onMarkerClick(area);
            }}
          />
        );
      }
      if (area.shape === 'polygon') {
        return (
          <Polygon
            key={area._id}
            path={area.path}
            onClick={(event) => {
              // eslint-disable-next-line no-param-reassign
              area.position = event.latLng;
              props.onMarkerClick(area);
            }}
          />
        );
      }
      return false;
    })}
    {props.areas.map((area) => {
      if (area.showInfo) {
        return (
          <InfoWindow
            key={area._id}
            position={area.position}
            onCloseClick={() => props.onMarkerClose(area)}
          >
            <div>
              <InfoContent
                marker={area}
                removeMarker={() => { props.removeMarker(area); }}
              />
            </div>
          </InfoWindow>
        );
      }
      return undefined;
    })}
    {props.drawingMode ?
    (<DrawingManager
      defaultOptions={{
        drawingControl: props.drawingMode,
        drawingControlOptions: {
          // eslint-disable-next-line no-undef
          position: google.maps.ControlPosition.BOTTOM_CENTER,
          drawingModes: [
            // eslint-disable-next-line no-undef
            google.maps.drawing.OverlayType.POLYGON,
            // eslint-disable-next-line no-undef
            google.maps.drawing.OverlayType.RECTANGLE,
            // eslint-disable-next-line no-undef
            // google.maps.drawing.OverlayType.CIRCLE,
          ],
        },
        // circleOptions: {
        //   clickable: false,
        // },
        rectangleOptions: {
          clickable: false,
        },
        polygonOptions: {
          clickable: false,
        },
      }}
      onOverlayComplete={props.onOverlayComplete}
    />) :
    (<div />)}
  </GoogleMap>
));

export default CustomMap;
