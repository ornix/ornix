import React from 'react';
import { List, ListItem, IconButton, Divider } from 'material-ui';
import { Grid, Row } from 'react-flexbox-grid';
import ActionAssignment from 'material-ui/svg-icons/action/assignment';
import Subheader from 'material-ui/Subheader';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const MarkerInformation = ({
  markers,
  areas,
  onIconClick,
  activityButton,
  selectedTool,
}) => (
  <div>
    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid className={selectedTool === 'activityList' ? 'map-drawer open-drawer' : 'map-drawer close-drawer'} style={{ height: '100vh', width: '20vw' }}>
        <Row className="map-drawer-content" start="sm">
          <List>
            <Subheader>Trees</Subheader>
            <div style={{ height: '40vh', overflowX: 'hidden', overdlowY: 'auto' }}>
              {markers && markers.length ?
                markers.map(marker => (
                  <ListItem
                    primaryText={marker.type}
                    secondaryText={marker.description}
                    rightIconButton={
                      <IconButton tooltip="Activities" tooltipStyles={{ marginLeft: '-5px' }} onClick={() => activityButton(marker._id, 'tree')}>
                        <ActionAssignment />
                      </IconButton>
                      }
                    key={marker._id}
                    onTouchTap={() => { onIconClick(marker); }}
                  />
                  )) :
                <div className="faint-text no-farm-data-available" style={{ margin: 0, marginBottom: 15, fontWeight: 400, textAlign: 'center' }}>
                    No marker has been plotted
                  </div>}
            </div>
            <Divider />
            <Subheader>Areas</Subheader>
            <div style={{ height: '33vh', overflowX: 'hidden', overdlowY: 'auto' }}>
              {areas && areas.length ?
              areas.map(area => (
                <ListItem
                  primaryText={area.type}
                  secondaryText={area.description}
                  rightIconButton={
                    <IconButton tooltip="Activities" onClick={() => activityButton(area._id, 'area')}>
                      <ActionAssignment />
                    </IconButton>
                    }
                  key={area._id}
                  onTouchTap={() => { onIconClick(area); }}
                />
                )) :
              <div className="faint-text no-farm-data-available" style={{ margin: 0, fontWeight: 400, textAlign: 'center' }}>
                  No area has been plotted
                </div>}
            </div>
          </List>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Grid fluid className={selectedTool === 'activityList' ? 'map-drawer open-drawer' : 'map-drawer close-drawer'} style={{ height: '100vh', width: '40vw' }}>
        <Row className="map-drawer-content" start="xs">
          <List>
            <Subheader>Trees</Subheader>
            {markers && markers.length ?
                markers.map(marker => (
                  <ListItem
                    primaryText={marker.type}
                    secondaryText={marker.description}
                    rightIconButton={
                      <IconButton tooltip="Activities" onClick={() => activityButton(marker._id, 'tree')}>
                        <ActionAssignment />
                      </IconButton>
                      }
                    key={marker._id}
                    onTouchTap={() => { onIconClick(marker); }}
                  />
                  )) :
                <div className="faint-text no-farm-data-available" style={{ margin: 0, marginBottom: 15, fontWeight: 400, textAlign: 'center' }}>
                    No marker has been plotted
                  </div>}
            <Divider />
            <Subheader>Areas</Subheader>
            {areas && areas.length ?
                areas.map(area => (
                  <ListItem
                    primaryText={area.type}
                    secondaryText={area.description}
                    rightIconButton={
                      <IconButton tooltip="Activities" onClick={() => activityButton(area._id, 'area')}>
                        <ActionAssignment />
                      </IconButton>
                      }
                    key={area._id}
                    onTouchTap={() => { onIconClick(area); }}
                  />
                )) :
                <div className="faint-text no-farm-data-available" style={{ margin: 0, fontWeight: 400, textAlign: 'center' }}>
                  No area has been plotted
                </div>}
          </List>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
  );

MarkerInformation.propTypes = {
  markers: PropTypes.array.isRequired,
  areas: PropTypes.array.isRequired,
  onIconClick: PropTypes.func.isRequired,
  activityButton: PropTypes.func.isRequired,
  selectedTool: PropTypes.string.isRequired,
};

export default MarkerInformation;
