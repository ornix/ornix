import React from 'react';
import { Paper, TextField, RaisedButton, Divider, DropDownMenu, MenuItem, DatePicker, Subheader } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Upload from 'material-ui-upload/Upload';
import MediaQuery from 'react-responsive';
import PropTypes from 'prop-types';

const ActivityForm = ({
  onInputChange,
  formErrors,
  selectedActivity,
  onDropDownChange,
  onFileLoad }) => (
    <div>

      {/* For tablets, laptops, desktops, large monitors */}
      <MediaQuery minWidth={768}>
        <Grid fluid style={{ margin: 0, padding: 0 }}>
          <Row center="sm">
            <Col sm={12}>
              <Paper className="circular">
                <Row start="sm">
                  <Col sm={12}>
                    <Subheader style={{ fontSize: '1.5em' }}>Add Activity</Subheader>
                  </Col>
                  <Col sm={12}>
                    <Divider />
                  </Col>
                </Row>
                <Row center="sm">
                  <Col sm={12}>
                    <Row start="sm">
                      <Col sm={12} style={{ margin: '1vh 0 0 0' }}>
                        <DropDownMenu
                          value={selectedActivity || 'Select activity'}
                          onChange={onDropDownChange}
                          anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                          style={{ width: '99%', marginLeft: '0.5%' }}
                          autoWidth={false}
                        >
                          <MenuItem value="fertilizer" primaryText="Fertilizer Application" />
                          <MenuItem value="report" primaryText="Field Report" />
                          <MenuItem value="harvest" primaryText="Harvest" />
                          <MenuItem value="land-prep" primaryText="Land Preparation" />
                          <MenuItem value="pesticide" primaryText="Pest Control" />
                          <MenuItem value="water" primaryText="Watering" />
                          <MenuItem value="weed" primaryText="Weed Control" />
                        </DropDownMenu>
                      </Col>
                    </Row>
                  </Col>
                  <Col sm={11}>
                    <Row start="sm">
                      {(selectedActivity !== 'water' && selectedActivity !== 'harvest' && selectedActivity !== 'report' && selectedActivity !== 'land-prep') ?
                        <Col sm={12}>
                          <TextField
                            hintText="Brand"
                            floatingLabelText="Brand"
                            name="brand"
                            type="text"
                            fullWidth
                            onChange={onInputChange}
                            errorText={formErrors.brand}
                          />
                        </Col>
                      : null}
                      {selectedActivity === 'harvest' ?
                        <Col sm={12}>
                          <TextField
                            hintText="Amount of harvest"
                            floatingLabelText="Amount of harvest"
                            name="amountOfHarvest"
                            type="number"
                            fullWidth
                            onChange={onInputChange}
                            errorText={formErrors.amountOfHarvest}
                          />
                          <TextField
                            hintText="Profit"
                            floatingLabelText="Profit"
                            name="profit"
                            type="number"
                            fullWidth
                            onChange={onInputChange}
                            errorText={formErrors.profit}
                          />
                        </Col>
                      : null}
                      <Col sm={12}>
                        <DatePicker
                          hintText="Date"
                          floatingLabelText="Date"
                          name="date"
                          defaultDate={new Date()}
                          fullWidth
                          onChange={onInputChange}
                          errorText={formErrors.remarks}
                        />
                      </Col>
                      <Col sm={12}>
                        <TextField
                          hintText="Remarks"
                          floatingLabelText="Remarks"
                          name="remarks"
                          type="text"
                          fullWidth
                          onChange={onInputChange}
                          errorText={formErrors.remarks}
                        />
                      </Col>
                      <Col sm={12} style={{ margin: '2vh 0 0 0' }}>
                        <Upload
                          label="Add Image"
                          onFileLoad={onFileLoad}
                          hoverColor={'rgba(255, 0, 0, 0)'}
                        />
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row end="sm">
                  <Col sm={12} style={{ margin: '2vh 2vw 2vh 0' }}>
                    <RaisedButton
                      type="submit"
                      label="Add"
                      primary
                    />
                  </Col>
                </Row>
              </Paper>
            </Col>
          </Row>
        </Grid>
      </MediaQuery>

      {/* For mobile phones and other devices which has a with of less than 767px */}
      <MediaQuery maxWidth={767}>
        <Grid fluid style={{ margin: 0, padding: 0 }}>
          <Row center="xs">
            <Col xs={12}>
              <Row start="xs">
                <Col xs={12}>
                  <Subheader style={{ fontSize: '1em' }}>Add Activity</Subheader>
                </Col>
              </Row>
              <Row center="xs">
                <Col xs={12}>
                  <Row start="xs">
                    <Col xs={12} style={{ margin: '1vh 0 0 0' }}>
                      <DropDownMenu
                        value={selectedActivity || 'Select activity'}
                        onChange={onDropDownChange}
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                        style={{ width: '99%', marginLeft: '0.5%' }}
                        autoWidth={false}
                      >
                        <MenuItem value="fertilizer" primaryText="Fertilizer Application" />
                        <MenuItem value="report" primaryText="Field Report" />
                        <MenuItem value="harvest" primaryText="Harvest" />
                        <MenuItem value="land-prep" primaryText="Land Preparation" />
                        <MenuItem value="pesticide" primaryText="Pest Control" />
                        <MenuItem value="water" primaryText="Watering" />
                        <MenuItem value="weed" primaryText="Weed Control" />
                      </DropDownMenu>
                    </Col>
                  </Row>
                </Col>
                <Col xs={10}>
                  <Row start="xs">
                    {(selectedActivity !== 'water' && selectedActivity !== 'harvest' && selectedActivity !== 'report' && selectedActivity !== 'land-prep') ?
                      <Col xs={12}>
                        <TextField
                          hintText="Brand"
                          floatingLabelText="Brand"
                          name="brand"
                          type="text"
                          fullWidth
                          onChange={onInputChange}
                          errorText={formErrors.brand}
                        />
                      </Col>
                    : null}
                    {selectedActivity === 'harvest' ?
                      <Col xs={12}>
                        <TextField
                          hintText="Amount of harvest"
                          floatingLabelText="Amount of harvest"
                          name="amountOfHarvest"
                          type="number"
                          fullWidth
                          onChange={onInputChange}
                          errorText={formErrors.amountOfHarvest}
                        />
                        <TextField
                          hintText="Profit"
                          floatingLabelText="Profit"
                          name="profit"
                          type="number"
                          fullWidth
                          onChange={onInputChange}
                          errorText={formErrors.profit}
                        />
                      </Col>
                    : null}
                    <Col xs={12}>
                      <DatePicker
                        hintText="Date"
                        floatingLabelText="Date"
                        name="date"
                        defaultDate={new Date()}
                        fullWidth
                        onChange={onInputChange}
                        errorText={formErrors.remarks}
                      />
                    </Col>
                    <Col xs={12}>
                      <TextField
                        hintText="Remarks"
                        floatingLabelText="Remarks"
                        name="remarks"
                        type="text"
                        fullWidth
                        onChange={onInputChange}
                        errorText={formErrors.remarks}
                      />
                    </Col>
                    <Col xs={12} style={{ margin: '2vh 0 0 0' }}>
                      <Upload
                        label="Add Image"
                        onFileLoad={onFileLoad}
                        hoverColor={'rgba(255, 0, 0, 0)'}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row center="xs">
                <Col xs={10} style={{ margin: '3vh 0 3vh 0' }}>
                  <RaisedButton
                    type="submit"
                    label="Add"
                    primary
                    fullWidth
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </MediaQuery>
    </div>
);

ActivityForm.propTypes = {
  onInputChange: PropTypes.func.isRequired,
  formErrors: PropTypes.object.isRequired,
  selectedActivity: PropTypes.string.isRequired,
  onDropDownChange: PropTypes.func.isRequired,
  onFileLoad: PropTypes.func.isRequired,
};

export default ActivityForm;
