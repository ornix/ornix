import React from 'react';
import { Card, GridList, GridTile, Subheader, Divider } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const styles = {
  gridList: {
    display: 'flex',
    margin: '5px',
  },
  titleStyle: {
    color: 'rgb(0, 188, 212)',
  },
};

const ImageGallery = ({ uriList, imageView }) => (
  <div>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Card className="circular" zDepth={3}>
          <Row start="sm">
            <Col sm={12}>
              <Subheader style={{ fontSize: '1.3em' }}>Photos: </Subheader>
              <Divider />
            </Col>
          </Row>
          {
            <GridList style={styles.gridList} cols={3}>
              { uriList && uriList.length ?
                  uriList.map(uri => (
                    <GridTile
                      title=" "
                      key={Math.random().toString(36).substring(10, 15)}
                      titleBackground="linear-gradient(to top, rgba(0,0,0,0.8) 0%,rgba(0,0,0,0.4) 70%,rgba(0,0,0,0) 100%)"
                    >
                      <img src={uri} alt="Farm Preview" role="presentation" onClick={(event) => { imageView(event.target.src); }}/>
                    </GridTile>
                  )) :
                  <GridTile style={{ height: 0 }} />
                }
            </GridList>
            }
        </Card>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        {
          <GridList style={styles.gridList} cols={1}>
            { uriList && uriList.length ?
                  uriList.map(uri => (
                    <GridTile
                      title=""
                      style={{ borderRadius: 10 }}
                      key={Math.random().toString(36).substring(10, 15)}
                    >
                      <div role="presentation" onClick={(event) => { imageView(event.target.src); }}>
                        <img src={uri} alt="Farm Preview" />
                      </div>
                    </GridTile>
                  )) :
                  <GridTile style={{ height: 0 }} />
                }
          </GridList>
            }
      </Grid>
    </MediaQuery>
  </div>
  );

ImageGallery.propTypes = {
  uriList: PropTypes.arrayOf(PropTypes.string).isRequired,
  imageView: PropTypes.func.isRequired,
};

export default ImageGallery;
