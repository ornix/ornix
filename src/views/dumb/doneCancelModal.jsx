import React from 'react';
import { Dialog, FlatButton } from 'material-ui';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const Modal = ({ open, handleClose, dialogContent, dialogTitle, handleDone }) => (
  <div>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Dialog
        paperClassName="circular"
        title={dialogTitle}
        titleStyle={{ marginBottom: 0, paddingBottom: 0 }}
        actions={
        [
          <FlatButton
            label="Cancel"
            primary
            onClick={handleClose}
            key={'cancel'}
          />,
          <FlatButton
            label="Done"
            primary
            keyboardFocused
            onClick={handleDone}
            key={'done'}
          />,
        ]
        }
        modal
        open={open}
        onRequestClose={handleClose}
      >
        {dialogContent}
      </Dialog>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Dialog
        title={dialogTitle}
        titleStyle={{ marginBottom: 0, paddingBottom: 0, fontSize: '1.3em' }}
        contentStyle={{ width: '100%', height: '50%', maxWidth: 'none', maxHeight: 'none' }}
        actions={
        [
          <FlatButton
            label="Cancel"
            primary
            onClick={handleClose}
            key={'cancel'}
          />,
          <FlatButton
            label="Done"
            primary
            keyboardFocused
            onClick={handleDone}
            key={'done'}
          />,
        ]
        }
        modal
        open={open}
        onRequestClose={handleClose}
      >
        {dialogContent}
      </Dialog>
    </MediaQuery>
  </div>
);

Modal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  dialogTitle: PropTypes.string.isRequired,
  dialogContent: PropTypes.array.isRequired,
  handleDone: PropTypes.func.isRequired,
};

export default Modal;
