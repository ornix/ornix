import React from 'react';
import { FlatButton } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Link } from 'react-router-dom';
import MediaQuery from 'react-responsive';

const Home = () => (
  <div>

    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid className="desktop-background" style={{ height: '100vh', display: 'grid' }}>
        <Row style={{ margin: 'auto', zIndex: 2 }}>
          <Col sm={12}>

            {/* Title */}
            <Row end="sm">
              <Col sm={10}>
                <span className="title-desktop" style={{ color: '#424242', fontFamily: 'Lobster' }}>Welcome to Harvestree!</span>
              </Col>
              <Col sm={10} style={{ marginBottom: '10vh' }}>
                <span className="subtitle" style={{ color: '#424242', fontFamily: 'Lobster' }}>&quot;Simplified farm management at your fingertips.&quot;</span>
              </Col>
            </Row>

            {/* Buttons */}
            <Row end="sm" style={{ marginBottom: '2vh' }}>
              <Col lg={3} sm={4}>
                <Link to="/register">
                  <FlatButton className="button-register shadow">REGISTER</FlatButton>
                </Link>
              </Col>
              <Col lg={3} sm={4}>
                <Link to="/login">
                  <FlatButton className="button-login shadow" primary backgroundColor="white">LOGIN</FlatButton>
                </Link>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row start="sm">
          <img src="./images/firstDivisionImage1.png" alt="Desktop, Laptop, and phone" height="55%" style={{ position: 'absolute', bottom: 0, zIndex: 1 }} />
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid className="desktop-background" style={{ height: '100vh', padding: 0, margin: 0, display: 'grid' }}>
        <Row style={{ margin: '15vh auto auto auto' }}>
          <Col xs={12}>

            {/* Title */}
            <Row center="xs">
              <Col xs={12} style={{ marginBottom: '4vh' }}>
                <img src="/images/harvestreeLogo3.png" alt="Harvestree Logo" width="35%" style={{ backgroundColor: '#81C784', borderRadius: '50%', padding: 15 }} />
              </Col>
              <Col xs={12}>
                <span className="mobile-title" style={{ color: '#424242', fontFamily: 'Lobster' }}>Welcome to Harvestree!</span>
              </Col>
              <Col xs={12}>
                <span className="mobile-subtitle" style={{ color: '#424242', fontFamily: 'Lobster' }}>&quot;Simplified farm management at your fingertips.&quot;</span>
              </Col>
            </Row>

            {/* Buttons */}
            <Row center="xs" style={{ position: 'absolute', bottom: 20 }}>
              <Col xs={12} style={{ marginBottom: '1.5vh' }}>
                <Link to="/login">
                  <FlatButton className="mobile-button-login" primary backgroundColor="white">LOGIN</FlatButton>
                </Link>
              </Col>
              <Col xs={12}>
                <Link to="/register">
                  <FlatButton className="mobile-button-register">REGISTER</FlatButton>
                </Link>
              </Col>
            </Row>

          </Col>
        </Row>
      </Grid>
    </MediaQuery>

  </div>
);

export default Home;
