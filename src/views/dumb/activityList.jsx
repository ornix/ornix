import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import PropTypes from 'prop-types';

const style = {
  cursor: 'default',
};

const ActivityList = ({ activityList, navigateToActivityProfile }) => (
  <Grid fluid style={{ margin: 0, padding: 0 }}>
    <Row center="xs">
      <Col xs={12}>
        <Table
          height={'425px'}
          onRowHover={() => {
            style.cursor = 'pointer';
          }}
        >
          <TableHeader displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn colSpan="3" style={{ textAlign: 'center', fontSize: '30px', fontWeight: 100, color: 'rgba(0, 0, 0, 0.5)' }}>List of Activities</TableHeaderColumn>
            </TableRow>
            <TableRow>
              <TableHeaderColumn>Type</TableHeaderColumn>
              <TableHeaderColumn>Date</TableHeaderColumn>
              <TableHeaderColumn>Remarks</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}
            showRowHover
          >
            {activityList.map(activity => (
              <TableRow
                key={activity._id}
                onTouchTap={() => navigateToActivityProfile(activity._id)}
                style={style}
              >
                <TableRowColumn>{activity.type}</TableRowColumn>
                <TableRowColumn>{new Date(activity.createdAt).toDateString()}</TableRowColumn>
                <TableRowColumn>{activity.remarks}</TableRowColumn>
              </TableRow>
          ))}
          </TableBody>
        </Table>
      </Col>
    </Row>
  </Grid>
);

ActivityList.propTypes = {
  activityList: PropTypes.arrayOf(PropTypes.object).isRequired,
  navigateToActivityProfile: PropTypes.func.isRequired,
};

export default ActivityList;
