import React from 'react';
import { Paper, TextField, DatePicker, RaisedButton, Divider, Subheader } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const ChangeUserDetailsForm = ({ user, onInputChange, formErrors, onBlur }) => (
  <div>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Row center="sm">
          <Col sm={6}>
            <Paper zDepth={3} className="circular">
              <Row start="sm">
                <Col sm={12} style={{ marginTop: '5px' }}>
                  <Subheader style={{ fontSize: '1.5em', color: 'black', fontWeight: 500 }}>Edit Profile</Subheader>
                </Col>
                <Col sm={12}>
                  <Divider />
                </Col>
              </Row>
              <Row center="sm">
                <Col sm={11}>
                  <Row start="sm">
                    <Col sm={12}>
                      <TextField
                        hintText="First Name"
                        floatingLabelText="First Name"
                        errorText={formErrors.firstName}
                        value={user.firstName}
                        type="text"
                        name="firstName"
                        fullWidth
                        onChange={onInputChange}
                        onBlur={onBlur}
                      />
                      <TextField
                        hintText="Middle Name"
                        floatingLabelText="Middle Name"
                        errorText={formErrors.middleName}
                        value={user.middleName}
                        type="text"
                        name="middleName"
                        fullWidth
                        onChange={onInputChange}
                        onBlur={onBlur}
                      />
                      <TextField
                        hintText="Last Name"
                        floatingLabelText="Last Name"
                        errorText={formErrors.lastName}
                        value={user.lastName}
                        type="text"
                        name="lastName"
                        fullWidth
                        onChange={onInputChange}
                        onBlur={onBlur}
                      />
                      <TextField
                        hintText="Address"
                        floatingLabelText="Address"
                        errorText={formErrors.address}
                        value={user.address}
                        type="text"
                        rowsMax={4}
                        name="address"
                        fullWidth
                        onChange={onInputChange}
                        onBlur={onBlur}
                      />
                      <TextField
                        hintText="Contact Number"
                        floatingLabelText="Contact Number"
                        errorText={formErrors.contactNumber}
                        value={user.contactNumber}
                        type="text"
                        name="contactNumber"
                        fullWidth
                        onChange={onInputChange}
                        onBlur={onBlur}
                      />
                      <DatePicker
                        hintText="Date of Birth"
                        floatingLabelText="Date of Birth"
                        value={user.dateOfBirth}
                        errorText={formErrors.dateOfBirth}
                        name="dateOfBirth"
                        fullWidth
                        onChange={onInputChange}
                        onBlur={onBlur}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row end="sm">
                <Col sm={12} style={{ margin: '2vh 2vw 2vh 0' }}>
                  <RaisedButton
                    label="Save Changes"
                    type="submit"
                    primary
                  />
                </Col>
              </Row>
            </Paper>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid>
        <Row style={{ marginBottom: '2vh' }}>
          <Paper zDepth={0}>
            <Row>
              <Col xs={12}>
                <TextField
                  hintText="First Name"
                  floatingLabelText="First Name"
                  errorText={formErrors.firstName}
                  value={user.firstName}
                  type="text"
                  name="firstName"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Middle Name"
                  floatingLabelText="Middle Name"
                  errorText={formErrors.middleName}
                  value={user.middleName}
                  type="text"
                  name="middleName"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Last Name"
                  floatingLabelText="Last Name"
                  errorText={formErrors.lastName}
                  value={user.lastName}
                  type="text"
                  name="lastName"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Address"
                  floatingLabelText="Address"
                  errorText={formErrors.address}
                  value={user.address}
                  type="text"
                  rowsMax={4}
                  name="address"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <TextField
                  hintText="Contact Number"
                  floatingLabelText="Contact Number"
                  errorText={formErrors.contactNumber}
                  value={user.contactNumber}
                  type="text"
                  name="contactNumber"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12}>
                <DatePicker
                  hintText="Date of Birth"
                  floatingLabelText="Date of Birth"
                  value={user.dateOfBirth}
                  errorText={formErrors.dateOfBirth}
                  name="dateOfBirth"
                  fullWidth
                  onChange={onInputChange}
                  onBlur={onBlur}
                />
              </Col>
              <Col xs={12} style={{ marginTop: '2vh' }}>
                <RaisedButton
                  label="Save Changes"
                  type="submit"
                  primary
                  fullWidth
                />
              </Col>
            </Row>
          </Paper>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

ChangeUserDetailsForm.propTypes = {
  user: PropTypes.object.isRequired,
  onInputChange: PropTypes.func.isRequired,
  formErrors: PropTypes.object.isRequired,
  onBlur: PropTypes.func.isRequired,
};

export default ChangeUserDetailsForm;
