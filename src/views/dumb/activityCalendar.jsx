import React from 'react';
import { Paper, Subheader, Divider } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import BigCalendar from 'react-big-calendar';
import Moment from 'moment';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import ArrowForward from 'material-ui/svg-icons/navigation/arrow-forward';
import Today from 'material-ui/svg-icons/action/today';

BigCalendar.momentLocalizer(Moment);

function Event({ event }) {
  // Font weights greater than 600 or bold (700) will make text unusually bold
  return (
    <span>
      <span style={{ fontWeight: '550' }}>
        {event.title}
      </span>
      {event.remarks ?
        <span>
          <span style={{ fontWeight: '550' }}>:</span>
          <span> {event.remarks}</span>
        </span>
        : undefined
      }
    </span>
  );
}

Event.propTypes = {
  event: PropTypes.object.isRequired,
};

const ActivityCalendar = ({ events, onSelectEvent, onNavigate }) => (
  <div>
    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Paper className="circular">
          <Row start="sm">
            <Col sm={12}>
              <Subheader style={{ fontSize: '1.5em' }}>Activity Calendar</Subheader>

              {
                events.length <= 0 ?
                  <Row center="sm" style={{ marginBottom: '10px' }}>
                    <span style={{ color: '#F44336', fontStyle: 'italic' }}>
                      It seems like nothing&apos;s here yet. Add an activity to get started!
                    </span>
                  </Row>
                : undefined
              }

              <Divider />
            </Col>
          </Row>
          <Row center="sm">
            <Col sm={12}>
              <BigCalendar
                style={{ height: '80vh', paddingTop: '2vh', paddingBottom: '2vh', paddingLeft: '2vw', paddingRight: '2vw' }}
                selectable
                events={events}
                onSelectEvent={(event) => {
                  onSelectEvent(event.id);
                }}
                onNavigate={onNavigate}
                defaultDate={new Date()}
                views={['month', 'day', 'agenda']}
                messages={{
                  agenda: 'List',
                  month: 'Month',
                  day: 'Day',
                  today: <Today />,
                  previous: <ArrowBack />,
                  next: <ArrowForward />,
                }}
                components={{
                  event: Event,
                }}
              />
            </Col>
          </Row>
        </Paper>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Row start="xs">
          <Col xs={12}>
            <Subheader>Activity Calendar</Subheader>
            {
              events.length <= 0 ?
                <Row center="sm">
                  <span style={{ color: 'red', fontStyle: 'italic' }}>
                    It seems like nothing&apos;s here yet. Add an activity to get started!
                  </span>
                </Row>
              : undefined
            }
            <Divider />
          </Col>
        </Row>
        <Row center="xs">
          <Col xs={12}>
            <BigCalendar
              style={{ height: '125vh', paddingTop: '2vh', paddingBottom: '2vh', paddingLeft: '2vw', paddingRight: '2vw' }}
              selectable
              events={events}
              onSelectEvent={(event) => {
                onSelectEvent(event.id);
              }}
              onNavigate={onNavigate}
              defaultDate={new Date()}
              views={['month', 'day', 'agenda']}
              messages={{
                agenda: 'List',
                month: 'Month',
                day: 'Day',
                today: <Today />,
                previous: <ArrowBack />,
                next: <ArrowForward />,
              }}
              components={{
                event: Event,
              }}
            />
          </Col>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

ActivityCalendar.propTypes = {
  events: PropTypes.arrayOf(PropTypes.object).isRequired,
  onSelectEvent: PropTypes.func.isRequired,
  onNavigate: PropTypes.func.isRequired,
};

export default ActivityCalendar;

