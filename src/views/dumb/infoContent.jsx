import React from 'react';
import PropTypes from 'prop-types';
import { IconButton, Divider } from 'material-ui';
import Delete from 'material-ui/svg-icons/action/delete';
import ViewList from 'material-ui/svg-icons/action/view-list';
import { Link } from 'react-router-dom';
import utilities from '../../utilities';

const capitalize = utilities.capitalize;

const InfoContent = ({ marker, removeMarker }) => (
  <div>
    <div style={{ marginTop: '15px', fontSize: '1.2em' }}>
      <span style={{ fontWeight: 500, fontSize: '1.1em' }}>Type: </span> {capitalize(marker.type)}
    </div>
    <div style={{ margin: '4px 0px 4px 0px', fontSize: '1.2em' }}>
      <span style={{ fontWeight: 500, fontSize: '1.1em' }}>Description: </span> {capitalize(marker.description)}
    </div>
    <div style={{ fontSize: '1.2em' }}>
      <span style={{ fontWeight: 500, fontSize: '1.1em' }}>Last Activity: </span>
      {marker.lastActivity ? ` ${capitalize(marker.lastActivity.type)} on ${new Date(marker.lastActivity.date).toDateString()}`
        : 'No Activity'}
    </div>
    <div style={{ marginTop: '15px' }}>
      <Divider />
    </div>
    <div style={{ float: 'right' }}>
      <Link to={`/activity/${marker.shape ? 'area' : 'tree'}/${marker._id}`}>
        <IconButton touch>
          <ViewList />
        </IconButton>
      </Link>
      <IconButton touch onTouchTap={removeMarker}>
        <Delete />
      </IconButton>
    </div>
  </div>
  );

InfoContent.propTypes = {
  marker: PropTypes.object.isRequired,
  removeMarker: PropTypes.func.isRequired,
};

export default InfoContent;
