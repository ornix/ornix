import React from 'react';
import { Link } from 'react-router-dom';
import { Drawer, Divider, List, ListItem } from 'material-ui';
import { Card, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import muiThemeable from 'material-ui/styles/muiThemeable';
import Input from 'material-ui/svg-icons/action/input';
import Settings from 'material-ui/svg-icons/action/settings';
import About from 'material-ui/svg-icons/action/help';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import Home from 'material-ui/svg-icons/maps/place';
import PropTypes from 'prop-types';

const styles = {
  signOutButton: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    textAlign: 'center',
    backgroundColor: '#B71C1C',
    color: 'white',
  },
  removeLine: {
    textDecoration: 'none',
  },
};

const NavigationDrawer = ({ open, docked, closeDrawer, logout, state }) => (
  <Drawer
    open={open}
    docked={docked}
    onRequestChange={closeDrawer}
  >
    <Card zDepth={0}>
      <CardMedia
        overlay={
          <CardTitle
            title={`${state.currentUser.firstName} ${state.currentUser.lastName}`}
            subtitle={`@${state.currentUser.username}`}
            titleStyle={{ fontSize: '1.2em' }}
          />
        }
      >
        <img alt="Background" src="images/backgrounds/background2.png" />
      </CardMedia>
      {/* <CardTitle title="Menu" style={{ marginBottom: 0, paddingBottom: 0 }} />*/}

      <CardText style={{ margin: 0, padding: 0 }}>
        <List>
          <Link to="/" style={styles.removeLine}>
            <ListItem primaryText="Home" leftIcon={<Home />} onTouchTap={closeDrawer} />
          </Link>

          {state.currentUser !== null ?
            <div>
              <Link to="/changeProfile" style={styles.removeLine}>
                <ListItem primaryText="Settings" leftIcon={<Settings />} onTouchTap={closeDrawer} />
              </Link>
              <Link to="/about" style={styles.removeLine}>
                <ListItem primaryText="About" leftIcon={<About />} onTouchTap={closeDrawer} />
              </Link>
            </div>
              :
            <div>
              <Link to="/login" style={styles.removeLine}>
                <ListItem primaryText="Login" leftIcon={<Input />} onTouchTap={closeDrawer} />
              </Link>
              <Link to="/register" style={styles.removeLine}>
                <ListItem primaryText="Register" leftIcon={<PersonAdd />} onTouchTap={closeDrawer} />
              </Link>
            </div>
            }
          <Divider />

          {state.currentUser !== null ?
            <Link to="/" style={styles.removeLine}>
              <ListItem
                primaryText="Logout"
                onTouchTap={
                  () => { logout(); closeDrawer(); }
                }
                style={styles.signOutButton}
              />
            </Link>
            : null}

        </List>
      </CardText>
    </Card>
  </Drawer>
);

NavigationDrawer.propTypes = {
  open: PropTypes.bool.isRequired,
  docked: PropTypes.bool.isRequired,
  closeDrawer: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  state: PropTypes.object.isRequired,
};

export default muiThemeable()(NavigationDrawer);
