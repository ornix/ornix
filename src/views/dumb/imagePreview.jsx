import React from 'react';
import { Card, CardActions, IconButton, GridList, GridTile, Subheader, RaisedButton, Divider } from 'material-ui';
import StarBorder from 'material-ui/svg-icons/navigation/cancel';
import Delete from 'material-ui/svg-icons/action/delete';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const styles = {
  gridList: {
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
    margin: '2vh 2vw 2vh 2vw',
  },
  mobileGridList: {
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
    margin: '0 2vw 2vh 2vw',
  },
  mediumIcon: {
    width: 35,
    height: 35,
  },
};

const icon = (remove, uri) => (
  <IconButton
    iconStyle={styles.mediumIcon}
    style={{ marginBottom: 7, marginRight: 5 }}
    onClick={() => { remove(uri); }}
  >
    <StarBorder color="#F44336" />
  </IconButton>
);

const ImagePreview = ({ uriList, remove, removeAll }) => (
  <div>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Card className="circular">
          <Row start="sm">
            <Col sm={12}>
              <Subheader style={{ fontSize: '1.5em' }}>Image Preview</Subheader>
              <Divider />
            </Col>
          </Row>
          {
            <GridList style={styles.gridList} cols={2.2}>
              { uriList && uriList.length ?
                  uriList.map(uri => (
                    <GridTile
                      title=" "
                      actionIcon={icon(remove, uri)}
                      key={Math.random().toString(36).substring(10, 15)}
                    >
                      <img src={uri} alt="farm Preview" />
                    </GridTile>
                  )) :
                  <GridTile
                    title="No image to show"
                    cols={2.2}
                    style={{ backgroundColor: 'grey' }}
                  />
                }
            </GridList>
          }
          <CardActions>
            <Row end="sm">
              <Col sm={12} style={{ margin: '-1.5vh 0.5vw 1vh 0' }}>
                {
                    uriList && uriList.length ?
                      <RaisedButton label="Remove All" backgroundColor={'#F44336'} labelColor={'white'} onClick={removeAll} />
                    :
                      <RaisedButton label="Remove All" backgroundColor={'#F44336'} disabled labelColor={'white'} onClick={removeAll} />
                  }
              </Col>
            </Row>
          </CardActions>
        </Card>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Row start="xs">
        <Col xs={6}>
          <Subheader style={{ fontSize: '0.9em' }}>Image Preview</Subheader>
        </Col>
        <Col xs={6}>
          {
            uriList && uriList.length ?
              <IconButton
                style={{ float: 'right' }}
                touch
                onTouchTap={removeAll}
              >
                <Delete />
              </IconButton>
            : ''
          }
        </Col>
      </Row>
      <Row center="xs" style={{ width: '100vw', margin: 0, padding: 0 }}>
        <Col xs={12}>
          <GridList style={styles.mobileGridList} cols={2.2} cellHeight={90}>
            { uriList && uriList.length ?
                uriList.map(uri => (
                  <GridTile
                    title=" "
                    titleBackground="rgba(0, 0, 0, 0.5)"
                    titlePosition="top"
                    style={{ borderRadius: 10 }}
                    actionIcon={
                      <IconButton
                        iconStyle={{ height: 25, width: 25 }}
                        onClick={() => { remove(uri); }}
                      >
                        <StarBorder color="#F44336" />
                      </IconButton>
                    }
                    key={Math.random().toString(36).substring(10, 15)}
                  >
                    <img src={uri} alt="farm Preview" />
                  </GridTile>
                ))
                :
                <GridTile style={{ height: 0 }} />
              }
          </GridList>
        </Col>
      </Row>
    </MediaQuery>
  </div>
  );

ImagePreview.propTypes = {
  uriList: PropTypes.arrayOf(PropTypes.string).isRequired,
  remove: PropTypes.func.isRequired,
  removeAll: PropTypes.func.isRequired,
};

export default ImagePreview;
