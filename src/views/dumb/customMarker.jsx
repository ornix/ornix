import React from 'react';
import { Marker } from 'react-google-maps';
import PropTypes from 'prop-types';

const CustomMarker = ({ marker }) => (
  <Marker
    key={Date.now()}
    position={marker.position}
  />
);

CustomMarker.propTypes = {
  marker: PropTypes.object.isRequired,
};

export default CustomMarker;
