import React from 'react';
import { Avatar, List, ListItem, IconButton } from 'material-ui';
import Delete from 'material-ui/svg-icons/action/delete';
import Divider from 'material-ui/Divider/Divider';
import Face from 'material-ui/svg-icons/action/face';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import PropTypes from 'prop-types';

import customTheme from './../theme';

const EmployeeList = ({ employee, removeEmployee, navigateToEmployeeProfile, setPosition, position }) => (
  <div>
    <List>
      <ListItem
        onTouchTap={navigateToEmployeeProfile}
        primaryText={`${employee.lastName}, ${employee.firstName}`}
        secondaryText={`@${employee.username}`}
        leftAvatar={<Avatar backgroundColor={customTheme.palette.primary2Color} icon={<Face />} />}
        hoverColor={'#E8F5E9'}
        rightIconButton={
          <div>
            <SelectField
              value={position}
              onChange={setPosition(employee._id)}
              style={{ float: 'left', width: '75%' }}
            >
              <MenuItem value="employee" primaryText="Employee" />
              <MenuItem value="manager" primaryText="Manager" />
            </SelectField>
            <IconButton
              touch
              tooltipPosition="top-center"
              onTouchTap={removeEmployee}
              style={{ float: 'left', width: '25%' }}
            >
              <Delete color="#212121" />
            </IconButton>
          </div>
        }
      />
      <Divider inset />
    </List>
  </div>
);

EmployeeList.propTypes = {
  employee: PropTypes.object.isRequired,
  removeEmployee: PropTypes.func.isRequired,
  navigateToEmployeeProfile: PropTypes.func.isRequired,
  position: PropTypes.object.isRequired,
  setPosition: PropTypes.func.isRequired,
};

export default EmployeeList;
