import React from 'react';
import { Bar } from 'react-chartjs-2';
import PropTypes from 'prop-types';
import { Paper, Divider, Subheader } from 'material-ui';
import { Row } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';

import Statistics from '../../models/Statistics';

const processData = (data, year) => {
  const harvestData = data.filter(datum => datum.type === 'harvest' && new Date(datum.date).getFullYear() === year);
  const statistics = new Statistics(harvestData);
  const amountOfHarvest = statistics.getAmountOfHarvestArray();
  const profitArray = statistics.getProfitArray();

  const chartData = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    datasets: [
      {
        label: 'Amount of Harvest',
        backgroundColor: 'rgba(76,175,80,0.5)',
        borderColor: 'rgba(76,175,80,0.5)',
        hoverBackgroundColor: 'rgba(220,220,220,0.75)',
        hoverBorderColor: 'rgba(220,220,220,1)',
        data: amountOfHarvest,
      },
      {
        label: 'Profit',
        backgroundColor: 'rgba(151,187,205,0.5)',
        borderColor: 'rgba(151,187,205,0.8)',
        hoverBackgroundColor: 'rgba(151,187,205,0.75)',
        hoverBorderColor: 'rgba(151,187,205,1)',
        data: profitArray,
      },
    ],
  };
  return chartData;
};

const ProfitChart = ({ activities, year }) => (
  <div>
    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Paper className="circular">
        <Subheader style={{ fontSize: '1.5em' }}>{year} Harvest Statistics</Subheader>
        <Divider />
        {
          activities.length <= 0 ?
            <Row center="sm">
              <span style={{ color: '#F44336', fontStyle: 'italic', marginTop: '15px', marginBottom: '15px' }}>
                Insufficient data to generate harvest statistics
              </span>
            </Row> :
            <Bar
              data={processData(activities, year)}
              height={150}
              options={{
                maintainAspectRatio: false,
              }}
            />
        }
      </Paper>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Row start="xs">
        <Subheader>{year} Harvest Statistics</Subheader>
      </Row>
      <Divider />
      {
        activities.length <= 0 ?
          <Row center="xs">
            <span style={{ color: '#F44336', fontStyle: 'italic' }}>
              Not enough data
            </span>
          </Row> :
          <Bar
            data={processData(activities, year)}
            height={150}
            options={{
              maintainAspectRatio: false,
            }}
          />
        }
    </MediaQuery>
  </div>
);

ProfitChart.propTypes = {
  activities: PropTypes.arrayOf(PropTypes.object).isRequired,
  year: PropTypes.number.isRequired,
};

export default ProfitChart;
