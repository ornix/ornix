import React from 'react';
import { IconButton } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Link } from 'react-router-dom';
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const styles = {
  large: {
    width: 80,
    height: 80,
  },
  iconMargin: {
    marginBottom: '1vh',
  },
};

function getAssetHoverText(asset) {
  switch (asset) {
    case 'mapMarker.svg': return 'Map Marker';
    case 'weather.svg': return 'Weather Data';
    case 'fieldHealth.svg': return 'Field Health';
    default: return 'Activity List';
  }
}

const MapToolbox = ({
  assets,
  onIconClick,
  selectedTool,
}) => (
  <div>
    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ marginTop: '7vh' }}>
        <Row>
          <Col sm={1} smOffset={11}>
            {assets.map(asset => (
              <Row
                end="sm"
                key={asset}
                id={asset}
                style={styles.iconMargin}
              >
                <IconButton
                  tooltip={getAssetHoverText(asset)}
                  tooltipStyles={{ marginLeft: '-75px', marginTop: '-20px' }}
                  touch
                  onClick={() => { onIconClick(asset); }}
                  style={styles.large}
                >
                  <img
                    className={selectedTool === asset ? 'tool-icon tool-icon-selected' : 'tool-icon'}
                    src={`/images/tools/${asset}`}
                    alt={asset}
                    id={`tool_${asset}`}
                  />
                </IconButton>
              </Row>
              ))}
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px */}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ marginRight: -18 }}>
        <Row end="xs">
          <Col xs={12}>
            <Row end="xs" style={{ marginTop: '8px', marginBottom: '8px' }}>
              <IconButton
                className="shadow"
                style={{ backgroundColor: 'white', borderRadius: 14, marginRight: 2 }}
                containerElement={<Link to="/farmList" />}
              >
                <ArrowBack />
              </IconButton>
            </Row>
            {assets.map(asset => (
              <Row
                end="xs"
                key={asset}
                id={asset}
                onTouchTap={() => { onIconClick(asset); }}
              >
                <img
                  className={selectedTool === asset ? 'tool-icon tool-icon-selected' : 'tool-icon'}
                  src={`/images/tools/${asset}`}
                  alt={asset}
                  id={`tool_${asset}`}
                />
              </Row>
              ))}
          </Col>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

MapToolbox.propTypes = {
  selectedTool: PropTypes.string.isRequired,
  assets: PropTypes.array.isRequired,
  onIconClick: PropTypes.func.isRequired,
};

export default MapToolbox;
