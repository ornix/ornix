import React from 'react';
import { Paper, TextField, RaisedButton, FlatButton } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';
import { Link } from 'react-router-dom';
import PasswordField from 'material-ui-password-field';
import PropTypes from 'prop-types';

const Login = ({ errorMessage, submitHandler, inputHandler }) => (
  <div>
    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid className="desktop-background" style={{ height: '100vh', display: 'grid' }}>
        <Row center="sm" style={{ margin: 'auto' }}>
          <Col sm={4}>
            <Paper zDepth={3} className="circular">
              <Row center="sm">
                <Col sm={11} style={{ marginTop: '8vh', marginBottom: '3vh' }}>
                  <img src="/images/harvestreeLogo3.png" alt="Harvestree Logo" width="35%" style={{ backgroundColor: '#81C784', borderRadius: '50%', padding: 15 }} />
                </Col>
                <Col sm={11}>
                  <form onSubmit={submitHandler}>
                    <Row start="sm">
                      <Col sm={12}>
                        <TextField
                          hintText="Username"
                          floatingLabelText="Username"
                          type="text"
                          name="username"
                          fullWidth
                          errorText={errorMessage}
                          onChange={inputHandler}
                        />
                      </Col>
                      <Col sm={12}>
                        <PasswordField
                          floatingLabelText="Password"
                          type="password"
                          name="password"
                          fullWidth
                          errorText={errorMessage}
                          onChange={inputHandler}
                        />
                      </Col>
                      <Col sm={12} style={{ marginTop: '2.5vh', marginBottom: '2.5vh' }}>
                        <FlatButton
                          className="button-register-form"
                          backgroundColor="white"
                          type="submit"
                          primary
                          fullWidth
                          label="Login"
                        />
                      </Col>
                    </Row>
                  </form>
                </Col>
                <Col sm={11} style={{ marginTop: '4vh', marginBottom: '2vh' }}>
                  <div>
                    <span className="membership-question">
                      <Link to="/forgot-password" className="signup-link">
                      Forgot Password?
                    </Link>
                    </span>
                  </div>
                  <div>
                    <span className="membership-question">Don&apos;t have an account?&nbsp;
                    <Link to="/register" className="signup-link">
                      Sign up here
                    </Link>
                    </span>
                  </div>
                </Col>
              </Row>
            </Paper>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid className="desktop-background" style={{ height: '100vh', display: 'grid', margin: 0, padding: 0 }}>
        <Row center="xs" style={{ margin: 'auto' }}>
          <Col xs={11}>
            <Row center="xs">
              <Col xs={12} style={{ margin: '0 0 6vh 0' }}>
                <img src="/images/harvestreeLogo3.png" alt="Harvestree Logo" width="35%" style={{ backgroundColor: '#81C784', borderRadius: '50%', padding: 15 }} />
              </Col>
              <Col xs={12}>
                <form onSubmit={submitHandler}>
                  <Row start="xs">
                    <Col xs={12}>
                      <TextField
                        hintText="Username"
                        floatingLabelText="Username"
                        type="text"
                        name="username"
                        fullWidth
                        onChange={inputHandler}
                        floatingLabelStyle={{ color: '#388E3C' }}
                        underlineStyle={{ border: '1px solid #388E3C' }}
                      />
                    </Col>
                    <Col xs={12} style={{ marginTop: '-12px', paddingTop: 0 }}>
                      <PasswordField
                        floatingLabelText="Password"
                        type="password"
                        name="password"
                        fullWidth
                        errorText={errorMessage}
                        onChange={inputHandler}
                        floatingLabelStyle={{ color: '#388E3C' }}
                        underlineStyle={{ border: '1px solid #388E3C' }}
                      />
                    </Col>
                    <Col xs={12} style={{ marginTop: '5vh' }}>
                      <FlatButton
                        className="mobile-button-login"
                        backgroundColor="white"
                        type="submit"
                        primary
                        fullWidth
                        label="Login"
                      />
                    </Col>
                  </Row>
                </form>
              </Col>
              <Col xs={12} style={{ position: 'absolute', bottom: 15 }}>
                <div>
                  <span className="mobile-membership-question">
                    <Link to="/forgot-password" className="mobile-signup-link">
                      Forgot Password?
                    </Link>
                  </span>
                </div>
                <div>
                  <span className="mobile-membership-question">Don&apos;t have an account?&nbsp;
                    <Link to="/register" className="mobile-signup-link">
                      Sign up here
                    </Link>
                  </span>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

  </div>
);

Login.propTypes = {
  errorMessage: PropTypes.string.isRequired,
  submitHandler: PropTypes.func.isRequired,
  inputHandler: PropTypes.func.isRequired,
};

export default Login;
