import React from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';
import ReactCountdownClock from 'react-countdown-clock';
import PropTypes from 'prop-types';

const centerDesktop = (window.innerWidth / 2) - 75;
const centerMobile = (window.innerWidth / 2) - 50;

const RedirectFromInvite = ({ redirectToMap }) => (
  <div>

    {/* For tablets, laptops, desktops, large monitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ height: window.innerHeight - 64, display: 'grid', margin: 0, padding: 0 }}>
        <Row center="sm" style={{ margin: 'auto' }}>
          <Col sm={12}>
            <span className="title">Successfully employed!</span>
          </Col>
          <Col sm={12}>
            <span className="subtitle">You are being redirected to the farm.</span>
          </Col>
        </Row>
        <div style={{ position: 'relative', left: centerDesktop, margin: 0, padding: 0, marginTop: '-8vh' }}>
          <ReactCountdownClock
            seconds={5}
            color="#388E3C"
            font="Roboto"
            alpha={0.9}
            size={150}
            onComplete={redirectToMap}
          />
        </div>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ height: window.innerHeight - 45, display: 'grid', margin: 0, padding: 0 }}>
        <Row center="xs" style={{ margin: 'auto', marginTop: '30vh', position: 'relative' }}>
          <Col xs={12}>
            <span className="mobile-title">Successfuly employed!</span>
          </Col>
          <Col xs={12}>
            <span className="mobile-subtitle">You are being redirected to the farm.</span>
          </Col>
        </Row>
        <div style={{ position: 'relative', left: centerMobile, margin: 0, padding: 0, marginTop: '-20vh', height: '50%' }}>
          <ReactCountdownClock
            seconds={5}
            color="#388E3C"
            font="Roboto"
            alpha={0.9}
            size={100}
            onComplete={redirectToMap}
          />
        </div>
      </Grid>
    </MediaQuery>
  </div>
);


RedirectFromInvite.propTypes = {
  redirectToMap: PropTypes.func.isRequired,
};
export default RedirectFromInvite;
