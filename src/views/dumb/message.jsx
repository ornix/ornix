import React from 'react';
import PropTypes from 'prop-types';

const message = ({ messageObject, currentUser }) => (
  <div
    className={currentUser._id === messageObject.user._id
      ? 'message ownMessage' : 'message someoneElseMessage'}
    title={new Date(messageObject.createdAt).toLocaleString()}
  >
    <div>
      {currentUser._id === messageObject.user._id ?
        '' : <b>{`${messageObject.user.firstName} ${messageObject.user.lastName}: `}</b>
      }
      {messageObject.content}
    </div>
  </div>
);

message.propTypes = {
  messageObject: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
};

export default message;
