import React from 'react';
import { TextField, Divider } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import PropTypes from 'prop-types';
import MediaQuery from 'react-responsive';

const CreateTreeForm = ({
  onInputChange,
  formErrors,
  values,
}) => (
  <div>

    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Row center="xs" style={{ marginBottom: '1.5vh' }}>
          <Col xs={12}>
            <Row start="sm">
              <Col xs={12} style={{ fontSize: '1.5em', fontWeight: 500, marginBottom: '1vh' }}>
                Add Marker
              </Col>
              <Col xs={12} style={{ fontSize: '1.2em', fontWeight: 300 }}>
                Please supply these information first before we successfully record your plot.
              </Col>
            </Row>
          </Col>
        </Row>

        <Divider />

        <Row center="xs" style={{ marginLeft: '1vw', marginRight: '1vw' }}>
          <Col xs={12}>
            <TextField
              hintText="Type"
              floatingLabelText="Type"
              name="type"
              onChange={onInputChange}
              errorText={formErrors.type}
              value={values.type || ''}
              fullWidth
            />
          </Col>
          <Col xs={12}>
            <TextField
              hintText="Description"
              floatingLabelText="Description"
              name="description"
              onChange={onInputChange}
              errorText={formErrors.description}
              value={values.description || ''}
              fullWidth
            />
          </Col>
        </Row>
      </Grid>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Grid fluid style={{ margin: 0, padding: 0 }}>
        <Row center="xs">
          <Col xs={12}>
            <Row start="xs">
              <Col xs={12} className="faint-text" style={{ fontSize: '0.9em', fontWeight: 500, marginBottom: '1vh' }}>
                Add Marker
              </Col>
              <Col xs={12} style={{ fontSize: '0.9em', fontWeight: 300 }}>
                Please supply these information first before we successfully record your plot.
              </Col>
            </Row>
          </Col>
        </Row>

        <Row center="xs" style={{ marginTop: '-4vh', paddingTop: 0 }}>
          <Col xs={12}>
            <TextField
              hintText="Type"
              floatingLabelText="Type"
              name="type"
              onChange={onInputChange}
              errorText={formErrors.type}
              value={values.type || ''}
              fullWidth
            />
          </Col>
          <Col xs={12} style={{ marginTop: '-5vh' }}>
            <TextField
              hintText="Description"
              floatingLabelText="Description"
              name="description"
              onChange={onInputChange}
              errorText={formErrors.description}
              value={values.description || ''}
              fullWidth
            />
          </Col>
        </Row>
      </Grid>
    </MediaQuery>
  </div>
);

CreateTreeForm.propTypes = {
  onInputChange: PropTypes.func.isRequired,
  formErrors: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};

export default CreateTreeForm;
