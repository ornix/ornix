import React from 'react';
import { Paper } from 'material-ui';
import { Grid, Row, Col } from 'react-flexbox-grid';
import MediaQuery from 'react-responsive';
import PropTypes from 'prop-types';
import customTheme from './../theme';

const FarmDetails = ({ farm }) => (
  <div style={{ fontWeight: 300 }}>

    {/* For tablets, laptops, desktops, large minitors */}
    <MediaQuery minWidth={768}>
      <Paper zDepth={0} style={{ padding: '0 0 2vh 0.5vw', backgroundColor: '#EEEEEE' }} className="circular">
        <Grid fluid>
          <Row>
            <Col xs={12}>
              <h3 style={{ marginTop: '0.5em', marginBottom: '0.5em' }}>Farm Details</h3>
            </Col>
          </Row>
          <Row around="lg">
            <Col sm={12} xs={12}><strong>Name:</strong> {farm.name}</Col>
            <Col sm={12} xs={12}><strong>Description:</strong> {farm.description}</Col>
            <Col sm={12} xs={12}><strong>No. of Managers:</strong> {farm.managerIds ? farm.managerIds.length : '0'}</Col>
            <Col sm={12} xs={12}><strong>No. of Employees:</strong> {farm.employeeIds ? farm.employeeIds.length : '0'}</Col>
            <Col sm={12} xs={12}><strong>No. of Areas:</strong> {farm.areaIds ? farm.areaIds.length : '0'}</Col>
            <Col sm={12} xs={12}><strong>No. of Trees:</strong> {farm.treeIds ? farm.treeIds.length : '0'}</Col>
            <Col sm={12} xs={12}>
              <strong>Created At:</strong> {new Date(farm.createdAt).toDateString()}
            </Col>
            <Col sm={12} xs={12}>
              <strong>Last Updated On:</strong> {farm.updatedAt ? new Date(farm.updatedAt).toDateString() : 'No data'}
            </Col>
          </Row>
        </Grid>
      </Paper>
    </MediaQuery>

    {/* For mobile phones and other devices which has a with of less than 767px*/}
    <MediaQuery maxWidth={767}>
      <Paper zDepth={0} style={{ backgroundColor: customTheme.palette.accent2Color }}>
        <Grid fluid style={{ marginRight: 0, paddingRight: 0 }}>
          <Row>
            <Col xs={12}>
              <h3 style={{ marginTop: '0.5em', marginBottom: '0.5em' }}>Farm Details</h3>
            </Col>
          </Row>
          <Row style={{ paddingBottom: '2.5vh' }}>
            <Col xs={12} style={{ marginBottom: '0.7vh' }}><span className="mobile-farm-details">Name:</span> {farm.name}</Col>
            <Col xs={12} style={{ marginBottom: '0.7vh' }}><span className="mobile-farm-details">Employees:</span> { farm.employeeIds ? farm.employeeIds.length : '0' }</Col>
            <Col xs={12} style={{ marginBottom: '0.7vh' }}><span className="mobile-farm-details">Description:</span> { farm.description }</Col>
            <Col xs={12} style={{ marginBottom: '0.7vh' }}><span className="mobile-farm-details">Trees:</span> { farm.treeIds ? farm.treeIds.length : '0' }</Col>
            <Col xs={12} style={{ marginBottom: '0.7vh' }}><span className="mobile-farm-details">Created At:</span> { new Date(farm.createdAt).toDateString() }</Col>
            <Col xs={12} style={{ marginBottom: '0.7vh' }}><span className="mobile-farm-details">Areas:</span> { farm.areaIds ? farm.areaIds.length : '0' }</Col>
            <Col xs={12}><span className="mobile-farm-details">Last Updated On:</span> { farm.updatedAt ? new Date(farm.updatedAt).toDateString() : 'No data' }</Col>
          </Row>
        </Grid>
      </Paper>
    </MediaQuery>
  </div>
);

FarmDetails.propTypes = {
  farm: PropTypes.object.isRequired,
};

export default FarmDetails;
