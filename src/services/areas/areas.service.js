// Initializes the `areas` service on path `/areas`
import setupService from '../setupService';
import hooks from './areas.hooks';

function configureService() {
  const app = this;
  app.configure(setupService('areas', hooks));
}

export default configureService;
