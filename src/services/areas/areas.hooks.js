import validate from 'feathers-hooks-validate-joi';
import commonHooks, { softDelete } from 'feathers-hooks-common';
import hooks from 'feathers-authentication-hooks';

import partialValidation from '../../hooks/partialValidation';
import objectIdFix from '../../hooks/objectIdFix';
import transform from '../../hooks/transform';
import Area from '../../models/Area';
import restrictToRoles from '../../hooks/restrictToRoles';

const areaActivitySchema = {
  include: [
    {
      service: 'api/activities',
      nameAs: 'activities',
      parentField: 'activityIds',
      childField: '_id',
      asArray: true,
    },
  ],
};

module.exports = {
  before: {
    all: [
      hooks.restrictToAuthenticated(),
    ],
    find: [
      objectIdFix,
      restrictToRoles(),
      softDelete(),
    ],
    get: [
      softDelete(),
    ],
    create: [
      validate.form(Area.schema,
      { convert: true, abortEarly: false }),
      commonHooks.setCreatedAt(),
      restrictToRoles({
        farmField: 'farmId',
        roles: ['owner', 'manager'],
      }),
      softDelete(),
    ],
    update: [
      validate.form(Area.schema, { convert: true, abortEarly: false }),
      commonHooks.setUpdatedAt(),
      restrictToRoles(),
      softDelete(),
    ],
    patch: [
      transform(Area),
      partialValidation(Area.schema),
      commonHooks.setUpdatedAt(),
      commonHooks.dePopulate(),
      restrictToRoles(),
      softDelete(),
    ],
    remove: [
      restrictToRoles({ roles: ['manager', 'owner'] }),
      softDelete(),
    ],
  },

  after: {
    all: [
      commonHooks.populate({ schema: areaActivitySchema }),
    ],
    find: [
      transform(Area),
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
