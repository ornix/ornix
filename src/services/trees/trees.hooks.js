import validate from 'feathers-hooks-validate-joi';
import commonHooks, { softDelete } from 'feathers-hooks-common';
import hooks from 'feathers-authentication-hooks';
import partialValidation from '../../hooks/partialValidation';

import transform from '../../hooks/transform';
import objectIdFix from '../../hooks/objectIdFix';
import Tree from '../../models/Tree';
import markPagination from '../../hooks/markPagination';
import restrictToRoles from '../../hooks/restrictToRoles';
import updateUser from '../../hooks/updateUser';

const treeActivitySchema = {
  include: [
    {
      service: 'api/activities',
      nameAs: 'activities',
      parentField: 'activityIds',
      childField: '_id',
      asArray: true,
    },
    {
      service: 'api/farms',
      nameAs: 'farm',
      parentField: 'farmId',
      childField: '_id',
    },
  ],
};

module.exports = {
  before: {
    all: [
      hooks.restrictToAuthenticated(),
      updateUser(),
    ],
    find: [
      objectIdFix,
      restrictToRoles({
        farmField: 'farmId',
        roles: ['owner', 'manager', 'employee'],
      }),
      softDelete(),
    ],
    get: [
      softDelete(),
    ],
    create: [
      validate.form(Tree.schema,
      { convert: true, abortEarly: false }),
      commonHooks.setCreatedAt(),
      restrictToRoles({
        roles: ['owner', 'manager'], // instead of all
      }),
      softDelete(),
    ],
    update: [
      validate.form(Tree.schema, { convert: true, abortEarly: false }),
      commonHooks.setUpdatedAt(),
      restrictToRoles({
        roles: ['owner', 'manager'], // instead of all
      }),
      softDelete(),
    ],
    patch: [
      transform(Tree),
      partialValidation(Tree.schema),
      commonHooks.setUpdatedAt(),
      commonHooks.dePopulate(),
      commonHooks.iff(hook => Object.prototype.hasOwnProperty.call(hook.data, 'farmId'),
                      restrictToRoles({
                        farmField: 'farmId',
                        roles: ['owner'],
                      })),
      restrictToRoles({
        farmField: 'farmId',
        roles: ['owner', 'manager', 'employee'],
      }),
      softDelete(),
    ],
    remove: [
      restrictToRoles({
        farmField: 'farmId',
        roleNames: ['owner', 'manager'],
      }),
      softDelete(),
    ],
  },

  after: {
    all: [
      commonHooks.populate({ schema: treeActivitySchema }),
    ],
    find: [
      markPagination(),
      transform(Tree),
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
