import setupService from '../setupService';
import hooks from './trees.hooks';
import filters from './trees.filters';

function configureService() {
  const app = this;
  app.configure(setupService('trees', hooks, filters));
}

export default configureService;
