module.exports = async (data, connection) => {
  if (!connection.user) {
    return false;
  }

  if (connection.user.authorizedFarms.includes(data.farmId)) {
    return data;
  }

  return false;
};
