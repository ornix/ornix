import validate from 'feathers-hooks-validate-joi';
import commonHooks, { softDelete } from 'feathers-hooks-common';
import hooks from 'feathers-authentication-hooks';

import partialValidation from '../../hooks/partialValidation';
import transform from '../../hooks/transform';
import Farm from '../../models/Farm';
import markPagination from '../../hooks/markPagination';
import logger from '../../hooks/logger';
import restrictToRoles from '../../hooks/restrictToRoles';
import updateUser from '../../hooks/updateUser';


function throwError(message) {
  return function exec(hook) {
    if (hook.params.provider) {
      throw new Error(message);
    }
    return hook;
  };
}

function restrictEmployment() {
  return async function exec(hook) {
    if (!hook.params.provider) {
      return hook;
    }

    const { user } = hook.params;
    const previousData = await hook.service.get(hook.id);
    const { data } = hook;

    // we have to make sure the user could only employ himself.
    // this hook is for users with  no roles yet.

    const expectedField = previousData.employeeIds.concat(user._id);

    if (JSON.stringify(expectedField) !== JSON.stringify(data.employeeIds)) {
      throw new Error('User could only employ his/herself.');
    }
    return hook;
  };
}

const patchHooks = [
  commonHooks.iff(hook => Object.prototype.hasOwnProperty.call(hook.data, 'ownerId'),
                  throwError('You cannot change the owner of this farm.')),
  // manager should not be able to change the array of managerIds, only the owner
  commonHooks.iff(hook => Object.prototype.hasOwnProperty.call(hook.data, 'managerIds'),
                  restrictToRoles({
                    farmField: '_id',
                    roles: ['owner'],
                  })),
  // means user has no role yet
  commonHooks.iffElse(hook => hook.params.user.getRoles(hook.id).length < 1,
    [
      // we only allow employeeIds field to be updated
      // for the invite link
      commonHooks.pluck('employeeIds'),
      // then restrict the employeeIds field changes
      // to limit the employment with the logic that
      // the logged in user could only employ his/herself
      // not other users
      restrictEmployment(),
    ],
    [
      restrictToRoles({
        farmField: '_id',
        roles: ['owner', 'manager'],
      }),
    ],
  ),
];

const farmUserSchema = {
  include: [
    {
      service: 'api/users',
      nameAs: 'employees',
      parentField: 'employeeIds',
      childField: '_id',
      asArray: true,
    },
    {
      service: 'api/users',
      nameAs: 'managers',
      parentField: 'managerIds',
      childField: '_id',
      asArray: true,
    },
    {
      service: 'api/trees',
      nameAs: 'trees',
      parentField: 'treeIds',
      childField: '_id',
      asArray: true,
      useInnerPopulate: true,
    },
    {
      service: 'api/areas',
      nameAs: 'areas',
      parentField: 'areaIds',
      childField: '_id',
      asArray: true,
      useInnerPopulate: true,
    },
  ],
};

module.exports = {
  before: {
    all: [
      hooks.restrictToAuthenticated(),
    ],
    find: [
      restrictToRoles({
        farmField: '_id',
      }),
      softDelete(),
    ],
    get: [
      softDelete(),
    ],
    create: [
      validate.form(Farm.schema,
      { convert: true, abortEarly: false }),
      commonHooks.setCreatedAt(),
      commonHooks.iff((hook) => {
        // dont use hook if it's an internal call
        if (!hook.params.provider) {
          return false;
        }
        return hook.params.user._id.toString() !== hook.data.ownerId;
      }, throwError('You can only create your own farm.')),
      softDelete(),
    ],
    update: [
      validate.form(Farm.schema, { convert: true, abortEarly: false }),
      commonHooks.setUpdatedAt(),
      restrictToRoles({
        farmField: '_id',
        // instead of manager and owner since
        // if we allow the manager to update,
        // he can revolt and overthrow the owner position
        roles: ['owner'],
      }),
      softDelete(),
    ],
    patch: [
      transform(Farm),
      partialValidation(Farm.schema),
      updateUser(),
      commonHooks.setUpdatedAt(),
      commonHooks.dePopulate(),
      // we can't just use restrictToRoles since there's an invite link
      commonHooks.preventChanges('ownerId'),
      commonHooks.iff(hook => hook.params.provider, ...patchHooks),
      softDelete(),
    ],
    remove: [
      restrictToRoles({
        farmField: '_id',
        roles: ['owner'],
      }),
      softDelete(),
    ],
  },

  after: {
    all: [
      commonHooks.populate({ schema: farmUserSchema }),
    ],
    find: [
      markPagination(),
      logger(),
    ],
    get: [
      commonHooks.iff(hook => !hook.params.user, commonHooks.pluck('_id')),
    ],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
