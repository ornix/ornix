module.exports = async (data, connection, hook) => {
  const user = await hook.app.service('api/users').get(connection.user._id);

  if (user.authorizedFarms.includes(data._id.toString())) {
    return data;
  }

  return false;
};
