import setupService from '../setupService';
import hooks from './farms.hooks';
import filters from './farms.filters';

function configureService() {
  const app = this;
  const paginate = app.get('paginate');
  const options = { paginate };

  app.configure(setupService('farms', hooks, filters, options));
}

export default configureService;
