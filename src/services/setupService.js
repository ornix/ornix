import createService from 'feathers-mongodb';

function setupService(name, hooks, filters, options = {}, indexOptions, uniqueIndexes) {
  return function setupPlugin() {
    const app = this;
    const mongoClient = app.get('mongoClient');

    app.use(`api/${name}`, createService(options));

    const service = app.service(`api/${name}`);

    mongoClient.then((db) => {
      service.Model = db.collection(name);

      if (indexOptions) {
        service.Model.createIndex(indexOptions);
      }

      if (uniqueIndexes) {
        service.Model.createIndex(uniqueIndexes, { unique: true });
      }
    });

    service.hooks(hooks);

    if (filters && service.filter) {
      service.filter(filters);
    }
  };
}

export default setupService;
