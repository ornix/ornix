import path from 'path';
import blobService from 'feathers-blob';
import fs from 'fs-blob-store';

import hooks from './uploads.hooks';

function configureService() {
  const app = this;

  const blobStorage = fs(path.join(app.get('public'), 'uploads'));

// Upload Service
  app.use('/api/uploads', blobService({ Model: blobStorage }));
  const service = app.service('api/uploads');

  service.hooks(hooks);
}

export default configureService;
