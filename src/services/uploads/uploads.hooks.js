import hooks from 'feathers-authentication-hooks';

import cs from '../../hooks/console';

module.exports = {
  before: {
    all: [
      hooks.restrictToAuthenticated(),
    ],
    find: [],
    get: [],
    create: [
      cs(),
    ],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
