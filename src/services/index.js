import users from './users/users.service';
import farms from './farms/farms.service';
import trees from './trees/trees.service';
import areas from './areas/areas.service';
import activities from './activities/activities.service';
import messages from './messages/messages.service';
import uploads from './uploads/uploads.services';

function execute() {
  const app = this;
  app.configure(users);
  app.configure(farms);
  app.configure(trees);
  app.configure(areas);
  app.configure(activities);
  app.configure(messages);
  app.configure(uploads);
}

export default execute;
