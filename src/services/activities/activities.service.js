// Initializes the `activities` service on path `/activities`
import setupService from '../setupService';
import hooks from './activities.hooks';

function configureService() {
  const app = this;
  const paginate = app.get('paginate');
  const options = { paginate };

  app.configure(setupService('activities', hooks, undefined, options));
}

export default configureService;
