import validate from 'feathers-hooks-validate-joi';
import commonHooks, { softDelete } from 'feathers-hooks-common';
import hooks from 'feathers-authentication-hooks';

import objectIdFix from '../../hooks/objectIdFix';
import transform from '../../hooks/transform';
import Activity from '../../models/Activity';
import partialValidation from '../../hooks/partialValidation';
import restrictToRoles from '../../hooks/restrictToRoles';
import trackAuthor from '../../hooks/trackAuthor';

const activitySchema = {
  include: [
    {
      service: 'api/users',
      nameAs: 'author',
      parentField: 'authorId',
      childField: '_id',
    },
  ],
};

module.exports = {
  before: {
    all: [
      hooks.restrictToAuthenticated(),
    ],
    find: [
      objectIdFix,
      restrictToRoles(),
      softDelete(),
    ],
    get: [
      softDelete(),
    ],
    create: [
      validate.form(Activity.schema,
      { convert: true, abortEarly: false }),
      commonHooks.setCreatedAt(),
      restrictToRoles(),
      trackAuthor('authorId'),
      softDelete(),
    ],
    update: [
      commonHooks.dePopulate(),
      validate.form(Activity.schema, { convert: true, abortEarly: false }),
      commonHooks.setUpdatedAt(),
      restrictToRoles(),
      trackAuthor('authorId'),
      softDelete(),
    ],
    patch: [
      commonHooks.dePopulate(),
      transform(Activity),
      partialValidation(Activity.schema),
      commonHooks.setUpdatedAt(),
      restrictToRoles(),
      trackAuthor('authorId'),
      softDelete(),
    ],
    remove: [
      restrictToRoles('manager', 'owner'),
      softDelete(),
    ],
  },

  after: {
    all: [],
    find: [
      transform(Activity),
      commonHooks.populate({ schema: activitySchema }),
    ],
    get: [
      commonHooks.populate({ schema: activitySchema }),
    ],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
