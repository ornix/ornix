module.exports = (data, connection) => {
  if (!connection.user) {
    return false;
  }

  if (connection.user._id === data._id) {
    return data;
  }

  return false;
};
