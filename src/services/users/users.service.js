import setupService from '../setupService';
import hooks from './users.hooks';
import filters from './users.filters';

function configureService() {
  const app = this;
  const paginate = app.get('paginate');
  const options = { paginate };

  const indexOptions = {
    firstName: 'text',
    middleName: 'text',
    lastName: 'text',
    username: 'text',
  };

  const uniqueIndexes = {
    username: 1,
  };

  app.configure(setupService('users', hooks, filters, options, indexOptions, uniqueIndexes));
}

export default configureService;
