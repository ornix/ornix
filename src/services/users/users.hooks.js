import validate from 'feathers-hooks-validate-joi';
import { hooks as localHooks } from 'feathers-authentication-local';
import hooks from 'feathers-authentication-hooks';
import commonHooks, { softDelete, preventChanges } from 'feathers-hooks-common';
import search from 'feathers-mongodb-fuzzy-search';
import authManagement from 'feathers-authentication-management';

import transform from '../../hooks/transform';
import User from '../../models/User';
import noDuplicateUsername from '../../hooks/noDuplicateUsername';
import partialValidation from '../../hooks/partialValidation';
import objectIdFix from '../../hooks/objectIdFix';
import markPagination from '../../hooks/markPagination';
import { sendCode, renewCode, internalVerification } from '../../hooks/processVerification';

const authManagementHooks = authManagement.hooks;
const hashPassword = localHooks.hashPassword;

const restrictedFields = ['username', 'createdAt'];

const userSchema = {
  include: [
    {
      service: 'api/farms',
      nameAs: 'ownedFarms',
      asArray: true,
      select: (hook, parentItem) => ({ ownerId: parentItem._id.toString() }),
    },
    {
      service: 'api/farms',
      nameAs: 'employedFarms',
      asArray: true,
      select: (hook, parentItem) => ({ employeeIds: parentItem._id.toString() }),
    },
    {
      service: 'api/farms',
      nameAs: 'managedFarms',
      asArray: true,
      select: (hook, parentItem) => ({ managerIds: parentItem._id.toString() }),
    },
  ],
};

module.exports = {
  before: {
    all: [
      softDelete(),
    ],
    find: [
      commonHooks.iff(hook => hook.params.query.$search, search()),
      objectIdFix,
    ],
    get: [],
    create: [
      noDuplicateUsername(),
      authManagementHooks.addVerification(),
      validate.form(User.schema, { convert: true, abortEarly: false }),
      commonHooks.discard('confirmPassword'),
      hashPassword(),
      commonHooks.setCreatedAt(),
      internalVerification,
    ],
    update: [
      hooks.restrictToOwner({ idField: '_id', ownerField: '_id' }),
      hooks.restrictToAuthenticated(),
      validate.form(User.schema, { convert: true, abortEarly: false }),
      commonHooks.setUpdatedAt(),
      preventChanges(...restrictedFields),
    ],
    patch: [
      hooks.restrictToOwner({ idField: '_id', ownerField: '_id' }),
      hooks.restrictToAuthenticated(),
      transform(User),
      partialValidation(User.schema),
      commonHooks.setUpdatedAt(),
      preventChanges(...restrictedFields),
    ],
    remove: [
      hooks.restrictToAuthenticated(),
    ],
  },

  after: {
    all: [
      commonHooks.when(
        hook => hook.params.provider,
        commonHooks.discard('password'),
      ),
    ],
    find: [
      markPagination(),
      transform(User),
      commonHooks.populate({ schema: userSchema }),
    ],
    get: [
      transform(User),
      commonHooks.populate({ schema: userSchema }),
    ],
    create: [
      sendCode,
      authManagementHooks.removeVerification(),
    ],
    update: [],
    patch: [
      renewCode,
    ],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
