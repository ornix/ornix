import setupService from '../setupService';
import hooks from './messages.hooks';
import filters from './messages.filter';

function configureService() {
  const app = this;
  const paginate = app.get('paginate');
  const options = { paginate };

  app.configure(setupService('messages', hooks, filters, options));
}

export default configureService;
