import validate from 'feathers-hooks-validate-joi';
import commonHooks, { softDelete } from 'feathers-hooks-common';
import hooks from 'feathers-authentication-hooks';

import restrictToRoles from '../../hooks/restrictToRoles';
import transform from '../../hooks/transform';
import Message from '../../models/Message';
import latestMessages from '../../hooks/latestMessages';

const messageUserSchema = {
  include: [
    {
      service: 'api/users',
      nameAs: 'user',
      parentField: 'userId',
      childField: '_id',
    },
  ],
};

module.exports = {
  before: {
    all: [
      hooks.restrictToAuthenticated(),
      softDelete(),
    ],
    find: [
      restrictToRoles({
        farmField: 'farmId',
        roles: ['owner', 'manager', 'employee'],
      }),
      latestMessages,
    ],
    get: [
      restrictToRoles({
        farmField: 'farmId',
        roles: ['owner', 'manager', 'employee'],
      }),
    ],
    create: [
      commonHooks.setCreatedAt(),
      validate.form(Message.schema,
        { convert: true, abortEarly: false }),
      hooks.associateCurrentUser({ idField: '_id', as: 'userId' }),
    ],
    update: [
      commonHooks.disallow(),
    ],
    patch: [
      commonHooks.disallow(),
    ],
    remove: [
      commonHooks.when(hook => hook.params.user._id !== hook.data.userId, commonHooks.disallow()),
    ],
  },

  after: {
    all: [
      transform(Message),
      commonHooks.populate({ schema: messageUserSchema }),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
