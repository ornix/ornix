import { MongoClient } from 'mongodb';

function execute() {
  const app = this;
  let config;
  if (process.env.NODE_ENV === 'ci') {
    config = app.get('mongodb:ci');
  } else if (process.env.NODE_ENV === 'production') {
    config = app.get('mongodb:production');
  } else {
    config = app.get('mongodb');
  }
  const promise = MongoClient.connect(config);

  app.set('mongoClient', promise);
}

export default execute;
