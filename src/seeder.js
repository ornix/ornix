/* eslint-disable no-console */

import faker from 'faker';
import app from './app';

async function seeder() {
  await app.get('mongoClient');
  const numberOfIterations = process.argv[2];
  const userService = app.service('api/users');

  const userPromise = [];
  for (let iteration = 0; iteration < numberOfIterations; iteration += 1) {
    const user = {
      firstName: faker.name.firstName(),
      middleName: faker.name.lastName(),
      lastName: faker.name.lastName(),
      address: faker.address.streetAddress(),
      contactNumber: `63${faker.random.number({ min: 9000000000, max: 9999999999 })}`,
      dateOfBirth: faker.date.past(),
      username: faker.internet.userName(),
      password: '123456',
      confirmPassword: '123456',
    };
    console.log(`User seed: ${user.username} - ${user.password}`);
    userPromise.push(userService.create(user));
  }

  await Promise.all(userPromise);
  console.log(`${numberOfIterations} USERS SUCCESSFULLY SEEDED`);
}

seeder().then(() => {
  process.exit(0);
}).catch((err) => {
  console.log(err);
  process.exit(1);
});
