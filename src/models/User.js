import Joi from 'joi';
import objectId from 'joi-objectid';
import Model from './Model';

Joi.objectId = objectId(Joi);

class User extends Model {
  static get schema() {
    return {
      firstName: Joi.string()
        .regex(/[\w\s]+/)
        .required()
        .label('First Name')
        .options({
          language: {
            string: {
              regex: { base: 'must be composed of letters and numbers only.' },
            },
          },
        }),
      middleName: Joi.string()
        .allow('')
        .regex(/[\w\s]+/)
        .label('Middle Name')
        .default('')
        .options({
          language: {
            string: {
              regex: { base: 'must be composed of letters and numbers only.' },
            },
          },
        }),
      lastName: Joi.string()
        .regex(/[\w\s]+/)
        .required()
        .label('Last Name')
        .options({
          language: {
            string: {
              regex: { base: 'must be composed of letters and numbers only.' },
            },
          },
        }),
      address: Joi.string()
        .label('Address'),
      contactNumber: Joi.string()
        .label('Contact Number')
        .regex(/63[0-9]{10}/)
        .options({
          language: {
            string: {
              regex: { base: 'is not a valid Philippine phone number format (639xxxxxxxxx)' },
            },
          },
        })
        .required(),
      dateOfBirth: Joi.date()
        .required()
        .label('Date of Birth'),
      username: Joi.string()
        .required()
        .label('Username'),
      password: Joi.string()
        .required()
        .min(6)
        .label('Password'),
      confirmPassword: Joi.any()
        .valid(Joi.ref('password'))
        .options({
          language: { any: { allowOnly: 'must match password' } },
        })
        .label('Password Confirmation')
        .required(),
      createdAt: Joi.date(),
      updatedOn: Joi.date(),
      farms: Joi.array()
        .items(Joi.string().alphanum())
        .label('Farms'),
      isVerified: Joi.boolean().required(),
      verifyToken: Joi.string().allow(null).optional(),
      verifyShortToken: Joi.string().allow(null).optional(),
      verifyChanges: Joi.object().optional(),
      verifyExpires: Joi.number().allow(null).optional(),
      resetToken: Joi.string().allow(null).optional(),
      resetShortToken: Joi.string().allow(null).optional(),
      resetExpires: Joi.date().allow(null).optional(),
    };
  }

  get ownedFarmIds() {
    return this.ownedFarms.map(farm => farm._id.toString());
  }

  get managedFarmIds() {
    return this.managedFarms.map(farm => farm._id.toString());
  }

  get employedFarmIds() {
    return this.employedFarms.map(farm => farm._id.toString());
  }

  get hierarchy() {
    return {
      owner: this.ownedFarmIds,
      manager: this.managedFarmIds,
      employee: this.employedFarmIds,
    };
  }

  get authorizedFarms() {
    const roleNames = Object.keys(this.hierarchy);
    return this.getRelatedFarms(...roleNames);
  }

  get fullName() {
    return `${this.firstName} ${this.middleName} ${this.lastName}`;
  }

  get meta() {
    return `${this.fullName} (${this.username})`;
  }

  getRoles(farmId) {
    const { hierarchy } = this;
    const roles = Object.keys(hierarchy);
    return roles.reduce((mem, role) => {
      if (hierarchy[role].includes(farmId)) {
        return mem.concat(role);
      }
      return mem;
    }, []);
  }

  getRelatedFarms(...roleNames) {
    const { hierarchy } = this;
    return roleNames.reduce((mem, roleName) => {
      if (Object.prototype.hasOwnProperty.call(hierarchy, roleName)) {
        return mem.concat(hierarchy[roleName]);
      }
      return mem;
    }, []);
  }
}

export default User;
