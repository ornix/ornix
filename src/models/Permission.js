class Permission {
  constructor() {
    this.permissions = {
      admin: ['read', 'write', 'execute'],
      farmer: ['read'],
    };
  }

  canExecute(role, action) {
    return this.permissions[role].indexOf(action) !== -1;
  }
}

export default Permission;
