import Joi from 'joi';
import objectId from 'joi-objectid';
import Model from './Model';

Joi.objectId = objectId(Joi);

class Farm extends Model {
  static get schema() {
    return {
      name: Joi.string().regex(/[\w\s]+/).required().label('Farm Name'),
      description: Joi.string().regex(/[\w\s]+/).required().label('Description'),
      ownerId: Joi.string().label('Owner'),
      center: Joi.object()
              .keys({
                lat: Joi.number(),
                lng: Joi.number(),
              }).required().label('Center'),
      boundary: Joi.array()
              .items(Joi.object().keys({
                lat: Joi.number(),
                lng: Joi.number(),
              }))
              .label('Boundary'),
      employeeIds: Joi.array()
              .unique()
              .items(Joi.objectId())
              .label('Employees')
              .default([]),
      managerIds: Joi.array()
              .unique()
              .items(Joi.objectId())
              .label('Managers')
              .default([]),
      terminatedEmployees: Joi.array()
              .items(Joi.object().keys({
                employeeId: Joi.objectId(),
                terminationDate: Joi.date(),
              })).label('Terminated Employees'),
      treeIds: Joi.array()
              .unique()
              .items(Joi.objectId())
              .label('Trees')
              .default([]),
      areaIds: Joi.array()
              .unique()
              .items(Joi.objectId())
              .label('Areas')
              .default([]),
      createdAt: Joi.date().label('Created At'),
      updatedAt: Joi.date().label('Updated At'),
    };
  }
}

export default Farm;
