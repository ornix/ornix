import request from 'superagent';

class WeatherMap {
  constructor(lon, lat) {
    this.lon = lon;
    this.lat = lat;
    this.weatherForcasts = [];
  }

  /**
   *  GET data from Open Weather Map
   *  with specific location (longitude, latitude)
   */
  async getWeatherData(keyword) {
    const data = await request
    .get(`http://api.openweathermap.org/data/2.5/${keyword}`)
    .query({ units: 'metric', lat: this.lat, lon: this.lon, APPID: 'f03cd1eda8e6d3e2151af3e8700b5e73' }); // query string

    return JSON.parse(data.text);
  }

  getCurrentWeather() {
    return this.getWeatherData('weather');
  }

  async getWeatherForecast() {
    const forecasts = await this.getWeatherData('forecast');
    const uniqueDates = new Set();
    const objects = {};

    /**
     *  Render each date of weather forecast from the Open Weather Map API
     *  and push to the set of unique dates.
     *
     *  We use set function to make sure that each date
     *  being pushed is unique
     */
    forecasts.list.forEach((forecast) => {
      uniqueDates.add(new Date(forecast.dt_txt).toDateString());
    });

    /**
     *  Intializing an object for each unique dates
     */
    uniqueDates.forEach((date) => {
      objects[date] = [];
    });

    /**
     *  Insert each weather forecast to an object
     *  with the same date
     */
    forecasts.list.forEach((forecast) => {
      objects[new Date(forecast.dt_txt).toDateString()].push(forecast);
    });

    return objects;
  }

}

export default WeatherMap;
