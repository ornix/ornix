/* eslint-disable no-console */

import request from 'superagent';

class Sms {

  /**
   * Note: Please tell me if you are working on a feature that needs the SMS verification
   * because I need to add you to the developer sandbox of our
   * SMS gateway for the feature to work. ~Rave
   */

  constructor(recipient, content) {
    // this.requestUri = 'https://platform.clickatell.com/messages/http/send';
    // this.apiKey = 'D7EF8ubGRo2zUyZyB8oBQg==';

    this.requestUri = 'https://cdf62a876b4652e71141b106470a1146:harvestree@api.transmitsms.com/send-sms.json';
    this.recipient = recipient;
    this.content = content;
  }

  async sendSms() {
    if (this.recipient.startsWith('+')) {
      this.recipient = this.recipient.replace('+', '');
    }

    if (this.recipient.length !== 12 || !this.recipient.startsWith('63')) {
      throw new Error(`Invalid Philippine Mobile Number: ${this.recipient}`);
    }

    if (this.recipient.startsWith('09')) {
      this.recipient = this.recipient.replace('0', '63');
    }

    // const response = await request
    // .get(this.requestUri)
    // .query({
    //   apiKey: this.apiKey,
    //   to: this.recipient,
    //   content: this.content,
    // });
    // return response;

    if (process.env.NODE_ENV === 'production') {
      const response = await request
      .post(this.requestUri)
      .type('form')
      .send({
        message: this.content,
        to: this.recipient,
      });

      return response;
    }

    return { debug: true, environment: process.env.NODE_ENV };
  }

  async sendVerificationSms(name, code) {
    this.content = `Hello ${name}! Your Harvestree verification code is: ${code}`;
    const response = await this.sendSms();

    if (process.env.NODE_ENV !== 'production') {
      console.log('##################');
      console.log(code);
      console.log('##################');
    }

    return response;
  }
}

export default Sms;
