class Statistics {
  constructor(data) {
    this.data = data;
    this.statistics = {};
    this.preSeed();
    this.calculate();
  }

  calculate() {
    this.data.forEach((datum) => {
      const harvestDate = new Date(datum.date);
      const month = harvestDate.getMonth();
      this.statistics[month].profit += datum.profit;
      this.statistics[month].amountOfHarvest += datum.amountOfHarvest;
    });
  }

  getProfitArray() {
    return Object.values(this.statistics).map(data => data.profit);
  }

  getAmountOfHarvestArray() {
    return Object.values(this.statistics).map(data => data.amountOfHarvest);
  }

  preSeed() {
    for (let index = 0; index < 12; index += 1) {
      this.statistics[index] = {
        amountOfHarvest: 0,
        profit: 0,
      };
    }
  }
}

export default Statistics;
