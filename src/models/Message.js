import Joi from 'joi';
import objectId from 'joi-objectid';
import Model from './Model';

Joi.objectId = objectId(Joi);

class Message extends Model {
  static get schema() {
    return {
      userId: Joi.objectId().required(),
      farmId: Joi.objectId().required(),
      content: Joi.string().label('Message').required(),
      createdAt: Joi.date().required(),
    };
  }

}

export default Message;
