import Joi from 'joi';
import Model from './Model';

class Activity extends Model {
  static get schema() {
    return {
      type: Joi.string().allow('water', 'harvest', 'fertilizer', 'pesticide', 'report', 'weed', 'land-prep').required(),
      brand: Joi.string().when('type', {
        is: ['harvest', 'water', 'report', 'land-prep'],
        otherwise: Joi.required(),
      }).label('Brand'),
      amountOfHarvest: Joi.number().when('type', {
        is: 'harvest',
        then: Joi.required(),
      }).label('Amount of Harvest'),
      profit: Joi.number().when('type', {
        is: 'harvest',
        then: Joi.required(),
      }).label('Profit'),
      remarks: Joi.string().label('Remarks'),
      date: Joi.date().required().label('Date'),
      images: Joi.array().items(Joi.string()).label('Images'),
      createdAt: Joi.date().label('Created at'),
      updatedAt: Joi.date().label('Updated at'),
      farmId: Joi.string().optional(),
      authorId: Joi.string().optional(),
    };
  }
}

export default Activity;
