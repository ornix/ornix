import Joi from 'joi';
import Model from './Model';

class Area extends Model {
  static get schema() {
    return {
      type: Joi.string()
        .required()
        .label('Type'),
      shape: Joi.string()
        .valid(['circle', 'rectangle', 'polygon'])
        .required()
        .label('Shape'),
      radius: Joi.number()
                .when('shape', {
                  is: 'circle',
                  then: Joi.required(),
                }).label('Radius'),
      center: Joi.object()
                .keys({
                  lat: Joi.number(),
                  lng: Joi.number(),
                })
                .when('shape', {
                  is: 'circle',
                  then: Joi.required(),
                }).label('Center'),
      bounds: Joi.object()
                .keys({
                  north: Joi.number(),
                  east: Joi.number(),
                  south: Joi.number(),
                  west: Joi.number(),
                })
                .when('shape', {
                  is: 'rectangle',
                  then: Joi.required(),
                }).label('Bounds'),
      path: Joi.array()
                .items(Joi.object().keys({
                  lat: Joi.number(),
                  lng: Joi.number(),
                }))
                .when('shape', {
                  is: 'polygon',
                  then: Joi.required(),
                }).label('Paths'),
      description: Joi.string()
        .regex(/[\w\s]+/)
        .required()
        .label('description'),
      farmId: Joi.string()
        .alphanum()
        .required()
        .label('Farm Id'),
      activityIds: Joi.array()
        .items(Joi.string().alphanum()).default([]),
      createdAt: Joi.date()
        .label('Created At'),
      updatedAt: Joi.date()
        .label('Updated At'),
    };
  }
}

export default Area;
