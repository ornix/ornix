import Joi from 'joi';
import _ from 'underscore';

export default class Model {
  static get schema() {
    return {};
  }

  constructor(doc) {
    Object.assign(this, doc);
  }

/**
  *  Validates the model using Joi.
  * `abortEarly` option is set to false to show all schema errors.
  *
  * Returns a Joi validation result object.
  */
  validate() {
    return Joi.validate(this, this.constructor.schema, { abortEarly: false });
  }

/**
  * Quick-check if the document is valid.
  *
  * Returns true if document conforms to schema rules
  * Returns false if document violates schema rules
 */
  isValid() {
    return this.validate().error === null;
  }

  get doc() {
    return _(this).omit('_id');
  }

}
