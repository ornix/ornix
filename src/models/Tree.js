import Joi from 'joi';
import Model from './Model';

class Tree extends Model {
  static get schema() {
    return {
      type: Joi.string()
        .regex(/[\w\s]+/)
        .required()
        .label('Tree Type')
        .options({
          language: {
            string: {
              regex: { base: 'must be composed of letters and numbers only.' },
            },
          },
        }),
      description: Joi.string()
        .regex(/[\w\s]+/)
        .required()
        .label('Description')
        .options({
          language: {
            string: {
              regex: { base: 'must be composed of letters and numbers only.' },
            },
          },
        }),
      coordinates: Joi.object()
        .keys({
          lat: Joi.number(),
          lng: Joi.number(),
        })
        .required().label('Coordinates'),
      farmId: Joi.string()
        .alphanum()
        .label('Farm ID'),
      createdAt: Joi.date()
        .label('Created At'),
      updatedAt: Joi.date()
        .label('Updated At'),
      icon: Joi.string(),
      activityIds: Joi.array()
        .items(Joi.string().alphanum()).default([]),
    };
  }

  get markerData() {
    return {
      lastActivity: this.lastActivity,
      _id: this._id,
      position: this.coordinates,
      defaultAnimation: 2,
      key: this.createdAt,
      showInfo: false,
      type: this.type,
      description: this.description,
      icon: this.icon,
    };
  }

  get lastActivity() {
    return this.activities ? this.activities[this.activities.length - 1] : null;
  }

  get memberIds() {
    const { ownerId, employeeIds } = this.farm;
    return employeeIds.concat(ownerId);
  }
}

export default Tree;
