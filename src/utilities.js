const functions = {
  capitalize: (str) => {
    if (!str) {
      return '';
    }
    let capitalizedStr = '';
    const strSplit = str.split(' ');
    strSplit.forEach((word) => {
      capitalizedStr += `${word.charAt(0).toUpperCase() + word.substring(1)} `;
    });
    return capitalizedStr.trim();
  },
};

export default functions;
