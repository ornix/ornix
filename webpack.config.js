const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  cache: true,
  entry: {
    core: ['./src/views/index.jsx'],
  },
  output: {
    path: path.join(process.cwd(), 'public/js'),
    publicPath: './js',
    filename: 'bundle.js',
  },
  node: {
    fs: 'empty',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader' },
          ],
        }),
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin({ filename: 'app.bundle.css' }),
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      joi: 'joi-browser',
    },
  },
};
