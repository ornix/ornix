<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels id="3aCl8xSsdVcP4n7CBJVc0QhORe8=" x="75" y="975">
    <screen href="Registration.screen#/"/>
  </panels>
  <panels id="mnF-7ZDXaGLhVINlQT9DaFaGAqM=" x="975" y="675">
    <screen href="Login.screen#/"/>
  </panels>
  <panels id="Eaoj8LGpi4x_bh1IQSblp0wrMQs=" x="975" y="375">
    <screen href="Main.screen#/"/>
  </panels>
  <panels id="NEpI4kZlpfzF6jCnkxouIFTZ9WA=" x="375" y="675">
    <screen href="MainwithOptions.screen#/"/>
  </panels>
  <panels id="kzXfLqFbVj17drps_pagcew9DbA=" x="75" y="75">
    <screen href="Account.screen#/"/>
  </panels>
  <panels id="5N_YpC1c5ztZmRDMtu1fp2mn-Sw=" x="375" y="975">
    <screen href="UpdateProfile.screen#/"/>
  </panels>
  <panels id="ez-nXv1AyBlzzM7LWwDH-awvOc4=" x="975" y="75">
    <screen href="ChangePassword.screen#/"/>
  </panels>
  <panels id="_VkPPEKosZjpcoHZ7cFNkGuIdTc=" x="675" y="375">
    <screen href="SideBarEmployee.screen#/"/>
  </panels>
  <panels id="Yxm4lxfrwgoxfP04_a5vv5maWlY=" x="375" y="375">
    <screen href="Employee.screen#/"/>
  </panels>
  <panels id="NqfXCQV-_3WBPSmQSqXcLfCM-3Q=" x="675" y="75">
    <screen href="ChangeManagement.screen#/"/>
  </panels>
  <panels id="-HRcqmM2MQYBPKhAuQ5OG-7uTIU=" x="375" y="75">
    <screen href="AddEmployee.screen#/"/>
  </panels>
  <panels id="abfDCfqEi15qKC5YiLLtpj-KHcA=" x="75" y="675">
    <screen href="SideBarMap.screen#/"/>
  </panels>
  <panels id="2oy6nu6c4MJMMiHL_c1tA23ec3A=" x="675" y="675">
    <screen href="SideBarMapPreview.screen#/"/>
  </panels>
  <panels id="gIG_Ir18A1VcV_uyDyhi2iCgw14=" x="75" y="375">
    <screen href="MapPinsManagement.screen#/"/>
  </panels>
</story:Storyboard>
